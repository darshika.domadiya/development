package com.arccus.accountability.model;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by Admin on 26/12/2017.
 */

public class Contact implements Parcelable {


    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };
    private int contect_id;
    private String contactName;
    private String contactEmail;
    private String contactNumber;
    private boolean isSelected;

    protected Contact(Parcel in) {
        contect_id = in.readInt();
        contactName = in.readString();
        contactEmail = in.readString();
        contactNumber = in.readString();
        isSelected = in.readByte() != 0;
    }

    public Contact(String contactName, String contactNumber) {

        this.contactName = contactName;
        this.contactNumber = contactNumber;

    }

    public Contact() {

    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getContactNumber() {

        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactEmail() {

        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactName() {

        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(contect_id);
        dest.writeString(contactName);
        dest.writeString(contactEmail);
        dest.writeString(contactNumber);
        dest.writeByte((byte) (isSelected ? 1 : 0));
    }

}

