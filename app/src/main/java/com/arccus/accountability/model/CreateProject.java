package com.arccus.accountability.model;

/**
 * Created by Admin on 09/01/2018.
 */

public class CreateProject {

    private String project_id;
    private String user_id;
    private String project_name;
    private String project_create_date;
    private String project_create_time;
    private int deleted;
    private int mysql_insert;
    private int mysql_delete;
    private int mysql_updated;
    private int colorPosition;
    private String realtedProject;
    private int projectPositon;
    private String colorName;
    private String serverId;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProject_create_date() {
        return project_create_date;
    }

    public void setProject_create_date(String project_create_date) {
        this.project_create_date = project_create_date;
    }

    public String getProject_create_time() {
        return project_create_time;
    }

    public void setProject_create_time(String project_create_time) {
        this.project_create_time = project_create_time;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public int getProjectPositon() {
        return projectPositon;
    }

    public void setProjectPositon(int projectPositon) {
        this.projectPositon = projectPositon;
    }


    public void setRealtedProject(String realtedProject) {
        this.realtedProject = realtedProject;
    }

    public int getColorPosition() {
        return colorPosition;
    }

    public void setColorPosition(int colorPosition) {
        this.colorPosition = colorPosition;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public void setMysql_updated(int mysql_updated) {
        this.mysql_updated = mysql_updated;
    }


    public void setMysql_delete(int mysql_delete) {
        this.mysql_delete = mysql_delete;
    }



    public void setMysql_insert(int mysql_insert) {
        this.mysql_insert = mysql_insert;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }


    public String getProject_name() {

        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public String getProject_id() {

        return project_id;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }
}
