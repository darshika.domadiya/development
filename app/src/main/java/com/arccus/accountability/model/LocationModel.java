package com.arccus.accountability.model;

/**
 * Created by Admin on 16/02/2018.
 */

public class LocationModel {

    private int id;



    public void setId(int id) {
        this.id = id;
    }

    private String locationName;

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    private String locationAddress;

    public String getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    private String late;

    public String getLate() {
        return late;
    }

    public void setLate(String late) {
        this.late = late;
    }

    private String longi;

    public String getLongi() {
        return longi;
    }

    public void setLongi(String longi) {
        this.longi = longi;
    }
}
