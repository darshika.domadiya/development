package com.arccus.accountability.model;

/**
 * Created by Admin on 02/02/2018.
 */

public class Priority {


    private int priorityColor;
    private String priorityName;


    public Priority(int color, String name) {
        this.priorityColor = color;
        this.priorityName = name;

    }

    public String getPriorityName() {
        return priorityName;
    }



    public int getPriorityColor() {

        return priorityColor;
    }


}
