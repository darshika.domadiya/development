package com.arccus.accountability.model;


/**
 * Created by Admin on 25/01/2018.
 */

public class Task {


    private int taskId;
    private String taskName;
    private String taskProjectId;
    private String taskDate;
    private String taskTag;
    private String taskPrority;
    private String taskColobration;
    private String taskComment;
    private boolean isSelected;
    private boolean completed;
    private String location;
    private String type;
    private int deleted;
    private String serverId;
    private int onlyMe;

    public int getOnlyMe() {
        return onlyMe;
    }

    public void setOnlyMe(int onlyMe) {
        this.onlyMe = onlyMe;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskProjectId() {
        return taskProjectId;
    }

    public void setTaskProjectId(String taskProjectId) {
        this.taskProjectId = taskProjectId;
    }

    public String getTaskDate() {
        return taskDate;
    }

    public void setTaskDate(String taskDate) {
        this.taskDate = taskDate;
    }

    public String getTaskTag() {
        return taskTag;
    }

    public void setTaskTag(String taskTag) {
        this.taskTag = taskTag;
    }

    public String getTaskPrority() {
        return taskPrority;
    }

    public void setTaskPrority(String taskPrority) {
        this.taskPrority = taskPrority;
    }

    public String getTaskColobration() {
        return taskColobration;
    }

    public void setTaskColobration(String taskColobration) {
        this.taskColobration = taskColobration;
    }

    public String getTaskComment() {
        return taskComment;
    }

    public void setTaskComment(String taskComment) {
        this.taskComment = taskComment;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }
}
