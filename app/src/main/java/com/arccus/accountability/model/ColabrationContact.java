package com.arccus.accountability.model;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Admin on 08/01/2018.
 */

public class ColabrationContact  implements Parcelable{



    private int id;
    private int project_id;
    private String share_number;
    private String share_email;
    private String share_name;
    private int add_task;
    private int edit_task;
    private int delete_task;
    private int assign_to_other;
    private int deleted;

    public ColabrationContact()
    {

    }

    protected ColabrationContact(Parcel in) {
        id = in.readInt();
        project_id = in.readInt();
        share_number = in.readString();
        share_email = in.readString();
        share_name = in.readString();
        add_task = in.readInt();
        edit_task = in.readInt();
        delete_task = in.readInt();
        assign_to_other = in.readInt();
        deleted = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(project_id);
        dest.writeString(share_number);
        dest.writeString(share_email);
        dest.writeString(share_name);
        dest.writeInt(add_task);
        dest.writeInt(edit_task);
        dest.writeInt(delete_task);
        dest.writeInt(assign_to_other);
        dest.writeInt(deleted);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ColabrationContact> CREATOR = new Creator<ColabrationContact>() {
        @Override
        public ColabrationContact createFromParcel(Parcel in) {
            return new ColabrationContact(in);
        }

        @Override
        public ColabrationContact[] newArray(int size) {
            return new ColabrationContact[size];
        }
    };

    public void setAssign_to_other(int assign_to_other) {
        this.assign_to_other = assign_to_other;
    }

    public void setDelete_task(int delete_task) {
        this.delete_task = delete_task;
    }

    public void setEdit_task(int edit_task) {
        this.edit_task = edit_task;
    }

    public void setAdd_task(int add_task) {
        this.add_task = add_task;
    }

    public String getShare_name() {

        return share_name;
    }

    public void setShare_name(String share_name) {
        this.share_name = share_name;
    }



    public void setShare_email(String share_email) {
        this.share_email = share_email;
    }

    public String getShare_number() {

        return share_number;
    }

    public void setShare_number(String share_number) {
        this.share_number = share_number;
    }

    public void setProject_id(int project_id) {
        this.project_id = project_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProject_id() {
        return project_id;
    }

    public String getShare_email() {
        return share_email;
    }

    public int getAdd_task() {
        return add_task;
    }

    public int getEdit_task() {
        return edit_task;
    }

    public int getDelete_task() {
        return delete_task;
    }

    public int getAssign_to_other() {
        return assign_to_other;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }
}
