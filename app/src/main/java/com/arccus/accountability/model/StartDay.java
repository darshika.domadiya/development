package com.arccus.accountability.model;

/**
 * Created by Admin on 11/01/2018.
 */

public class StartDay {


    private String dayName;
    private String timezone;
    private String startdaytime;

    public String getStartPage() {
        return startPage;
    }

    public void setStartPage(String startPage) {
        this.startPage = startPage;
    }

    private String startPage;

//    public StartDay(String name, String timezone, String startdaytime) {
//
//        this.dayName = name;
//        this.timezone = timezone;
//        this.startdaytime = startdaytime;
//
//
//    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getStartdaytime() {
        return startdaytime;
    }

    public void setStartdaytime(String startdaytime) {
        this.startdaytime = startdaytime;
    }
}
