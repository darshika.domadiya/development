package com.arccus.accountability.model;

/**
 * Created by Admin on 11/01/2018.
 */

public class ProjectColor {


    private int color;
    private String colorName;

    public ProjectColor(int imageId, String name) {

        this.color = imageId;
        this.colorName = name;


    }
    public int getColor() {
        return color;
    }


    public String getColorName() {
        return colorName;
    }


}


