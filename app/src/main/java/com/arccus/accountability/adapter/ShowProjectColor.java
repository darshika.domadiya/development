package com.arccus.accountability.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.arccus.accountability.R;
import com.arccus.accountability.model.ProjectColor;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Admin on 11/01/2018.
 */

public class ShowProjectColor extends RecyclerView.Adapter<ShowProjectColor.ViewHolder> {
    private Context mContext;

    private ArrayList<ProjectColor> colorName;
    private OnItemClickListener mOnItemClickListener;

    public ShowProjectColor(Context applicationContext, ArrayList<ProjectColor> countryNames) {
        this.mContext = applicationContext;
        this.colorName = countryNames;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(mContext).inflate(R.layout.custom_spinner_items, parent, false);
        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.icon.setImageResource(colorName.get(position).getColor());
        holder.names.setText(colorName.get(position).getColorName());
    }

    @Override
    public int getItemCount() {
        return colorName.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position, ProjectColor projectColor);


    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView icon;
        private TextView names;

        public ViewHolder(View itemView) {
            super(itemView);

            icon = itemView.findViewById(R.id.imageView);
            names = itemView.findViewById(R.id.textView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null)
                mOnItemClickListener.onItemClick(getAdapterPosition(), colorName.get(getAdapterPosition()));
        }
    }
}
