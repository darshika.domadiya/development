package com.arccus.accountability.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.arccus.accountability.R;

import com.arccus.accountability.model.Task;
import com.arccus.accountability.shared_preference.SessionManager;
import com.arccus.accountability.utils.CommanFunction;
import com.arccus.accountability.utils.Constant;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Admin on 08/02/2018.
 */

public class TaskAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {


    private static final int PENDING_REMOVAL_TIMEOUT = 3000; // 3sec
    List<Task> itemsPendingRemoval;

    public static boolean lastSelected = true;
    private Context mContext;
    private ArrayList<Task> personalTask;
    private ArrayList<Task> contactArrayListCopy;
    private OnItemClickListener itemClickListener;
    public static int globalPosition = -1;
    private int currentPosition;
    private Animation bottomUp, upBottom;
    private SessionManager sessionManager;
    private int EMPTY = 0;
    private int NOT_EMPTY = 1;
    private int whereToOpen;
    private LinearLayout llFabLayout, llBottom;
    private FloatingActionButton fbtnMain;
    private ItemFilter mFilter = new ItemFilter();
    private String TAG = "TaskAdapter";
    boolean undoOn = true;

    private Handler handler = new Handler(); // hanlder for running delayed runnables
    HashMap<Task, Runnable> pendingRunnables = new HashMap<>(); // map of items to pending runnables, so we can cancel a removal if need be

    public TaskAdapter(Context context, ArrayList<Task> list, int i,
                       LinearLayout llFabLayout, LinearLayout llBottom, FloatingActionButton fbtnMain) {
        this.mContext = context;
        this.personalTask = list;
        this.contactArrayListCopy = list;
        this.whereToOpen = i;
        this.llFabLayout = llFabLayout;
        this.llBottom = llBottom;
        this.fbtnMain = fbtnMain;
        sessionManager = new SessionManager(context);

        bottomUp = AnimationUtils.loadAnimation(context, R.anim.bottom_up);

        upBottom = AnimationUtils.loadAnimation(context, R.anim.bottom_down);

        itemsPendingRemoval = new ArrayList<>();
    }

//    public TaskAdapter(Context context, ArrayList<Task> list, ArrayList<Task> overdue, int i,
//                       LinearLayout llFabLayout, LinearLayout llBottom, FloatingActionButton fbtnMain) {
//        this.mContext = context;
//        this.personalTask = list;
//        //this.contactArrayListCopy = list;
//        this.whereToOpen = i;
//        this.overduetask = overdue;
//        this.llFabLayout = llFabLayout;
//        this.llBottom = llBottom;
//        this.fbtnMain = fbtnMain;
//        sessionManager = new SessionManager(context);
//
//        bottomUp = AnimationUtils.loadAnimation(context,
//                R.anim.bottom_up);
//
//        upBottom = AnimationUtils.loadAnimation(context,
//                R.anim.bottom_down);
//    }

    public void refreshData(ArrayList<Task> list) {
        personalTask.clear();
        personalTask.addAll(list);
        notifyDataSetChanged();
    }

    public void sortByDate() {
        try {
            Collections.sort(personalTask, new Comparator<Task>() {
                @Override
                public int compare(Task r1, Task r2) {
                    if(r1.getTaskDate() == null) return 1;
                    if(r2.getTaskDate() == null) return -1;
                    return r1.getTaskDate().compareTo(r2.getTaskDate());
                }
            });
            notifyDataSetChanged();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void sortByPriority() {
        try {
            Collections.sort(personalTask, new Comparator<Task>() {
                @Override
                public int compare(Task r1, Task r2) {
                   /* int j = 0;
                    if(r1.getTaskPrority() != null) {
                        ArrayList<String> strings = new ArrayList<>();
                        strings.add("High");
                        strings.add("Medium");
                        strings.add("Low");
                        for (int i = 0; i < strings.size(); i++) {
                            j = r1.getTaskPrority().compareTo(strings.get(i));
                        }
                    }
                    return j;*/
                    String p1 = r1.getTaskPrority();
                    String p2 = r2.getTaskPrority();
                    if(p1 == null) return 1;
                    if(p2 == null) return -1;
                    if(p1.equals(p2)) return 0;
                    if(p1.equals("High") && (p2.equals("Medium") || p2.equals("Low")))
                        return -1;
                    if(p1.equals("Medium") && p2.equals("Low"))
                        return -1;
                    return 1;
                }
            });
            notifyDataSetChanged();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void sortBySharing() {
        try {
            Collections.sort(personalTask, new Comparator<Task>() {
                @Override
                public int compare(Task r1, Task r2) {
                    if(r1.getTaskColobration() == null || TextUtils.isEmpty(r1.getTaskColobration())) return 1;
                    if(r2.getTaskColobration() == null || TextUtils.isEmpty(r2.getTaskColobration())) return -1;
                    return r1.getTaskColobration().compareTo(r2.getTaskColobration());
                }
            });
            notifyDataSetChanged();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void sortByProject() {
        try {
            Collections.sort(personalTask, new Comparator<Task>() {
                @Override
                public int compare(Task r1, Task r2) {
                    if(r1.getTaskProjectId() == null || TextUtils.isEmpty(r1.getTaskProjectId()) || r1.getTaskProjectId().equals("0")) return 1;
                    if(r2.getTaskProjectId() == null || TextUtils.isEmpty(r2.getTaskProjectId()) || r2.getTaskProjectId().equals("0")) return -1;
                    return r1.getTaskProjectId().compareTo(r2.getTaskProjectId());
                }
            });
            notifyDataSetChanged();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (personalTask.size() == 0) {
            return EMPTY;
        } else {
            return NOT_EMPTY;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        if (viewType == EMPTY) {
            View viewItem = inflater.inflate(R.layout.no_task_layout, parent, false);
            viewHolder = new EmptyViewHolder(viewItem);
        } else {
            View viewLoading = inflater.inflate(R.layout.task_list, parent, false);
            viewHolder = new NotEmptyViewHolder(viewLoading);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == EMPTY) {
            EmptyViewHolder emptyViewHolder = (EmptyViewHolder) holder;
            if (whereToOpen == 0) {
                emptyViewHolder.tvNoitem.setText("No Task.");
                emptyViewHolder.tvAddItem.setText("Add Task");
            } else if (whereToOpen == 1) {
                emptyViewHolder.tvNoitem.setText("No Idea.");
                emptyViewHolder.tvAddItem.setText("Add Idea");
            } else if (whereToOpen == 2) {
                emptyViewHolder.tvNoitem.setText("No Inquiry.");
                emptyViewHolder.tvAddItem.setText("Add Inquiry");
            }
        } else if (getItemViewType(position) == NOT_EMPTY) {
            final NotEmptyViewHolder notEmptyViewHolder = (NotEmptyViewHolder) holder;
            final Task task = personalTask.get(position);
            if (itemsPendingRemoval.contains(task)) {
                // we need to show the "undo" state of the row

                notEmptyViewHolder.viewForeground.setVisibility(View.GONE);
                notEmptyViewHolder.undoButton.setVisibility(View.VISIBLE);
                notEmptyViewHolder.tvCompleted.setVisibility(View.VISIBLE);
                if (whereToOpen == 0) {
                    notEmptyViewHolder.tvCompleted.setText("Task Completed.");
                } else if (whereToOpen == 1) {
                    notEmptyViewHolder.tvCompleted.setText("Idea Completed.");
                } else if (whereToOpen == 2) {
                    notEmptyViewHolder.tvCompleted.setText("Inquiry Completed.");
                }

                holder.itemView.setBackgroundColor(Color.RED);
                notEmptyViewHolder.undoButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // user wants to undo the removal, let's cancel the pending task
                        Runnable pendingRemovalRunnable = pendingRunnables.get(task);
                        pendingRunnables.remove(task);
                        if (pendingRemovalRunnable != null)
                            handler.removeCallbacks(pendingRemovalRunnable);
                        itemsPendingRemoval.remove(task);
                        // this will rebind the row in "normal" state
                        notifyItemChanged(personalTask.indexOf(task));
                    }
                });
            } else {

                notEmptyViewHolder.itemView.setBackgroundColor(Color.WHITE);
                notEmptyViewHolder.viewForeground.setVisibility(View.VISIBLE);
                notEmptyViewHolder.undoButton.setVisibility(View.GONE);
                notEmptyViewHolder.tvCompleted.setVisibility(View.GONE);
                notEmptyViewHolder.undoButton.setOnClickListener(null);

                // Task overtask = overduetask.get(position);

                notEmptyViewHolder.tvLocation.setText(task.getLocation());
                notEmptyViewHolder.tvTaskName.setText(task.getTaskName());
                notEmptyViewHolder.tvComment.setText(task.getTaskComment());

                if(task.isCompleted())
                    notEmptyViewHolder.tvTaskName.setTextColor(Color.RED);
                else
                    notEmptyViewHolder.tvTaskName.setTextColor(Color.BLACK);

                if (task.getTaskColobration() != null && !task.getTaskColobration().isEmpty() && !task.getTaskColobration().equals(" ")) {
                    notEmptyViewHolder.tvShared.setText("Shared");
                    TextDrawable.IBuilder builder = TextDrawable.builder()
                            .beginConfig()
                            .toUpperCase()
                            .endConfig()
                            .roundRect(25);
                    char first = task.getTaskColobration().charAt(0);
                    TextDrawable ic1 = builder.build(String.valueOf(first), ContextCompat.getColor(mContext, R.color.half_black));
                    notEmptyViewHolder.ivFirstCharecter.setImageDrawable(ic1);
                } else {
                    notEmptyViewHolder.tvShared.setText("Only Me");
                    TextDrawable.IBuilder builder = TextDrawable.builder()
                            .beginConfig()
                            .toUpperCase()
                            .endConfig()
                            .roundRect(25);
                    char first = sessionManager.getUserName().charAt(0);
                    TextDrawable ic1 = builder.build(String.valueOf(first), ContextCompat.getColor(mContext, R.color.half_black));
                    notEmptyViewHolder.ivFirstCharecter.setImageDrawable(ic1);
                }

                try {
                    if (task.getTaskPrority() != null) {
                        if (task.getTaskPrority().equalsIgnoreCase("High")) {
                            notEmptyViewHolder.ivFold.setVisibility(View.VISIBLE);
                            notEmptyViewHolder.ivFold.setBackgroundResource(task.getTaskPrority().equalsIgnoreCase("High") ? R.drawable.ic_fold : Color.TRANSPARENT);
                        } else if (task.getTaskPrority().equalsIgnoreCase("Medium")) {
                            notEmptyViewHolder.ivFold.setVisibility(View.VISIBLE);
                            notEmptyViewHolder.ivFold.setBackgroundResource(task.getTaskPrority().equalsIgnoreCase("Medium") ? R.drawable.ic_orange_fold : Color.TRANSPARENT);
                        } else if (task.getTaskPrority().equalsIgnoreCase("Low")) {
                            notEmptyViewHolder.ivFold.setVisibility(View.VISIBLE);
                            notEmptyViewHolder.ivFold.setBackgroundResource(task.getTaskPrority().equalsIgnoreCase("Low") ? R.drawable.ic_green_fold : Color.TRANSPARENT);
                        }
                        else
                            notEmptyViewHolder.ivFold.setVisibility(View.GONE);
                    }
                    else
                        notEmptyViewHolder.ivFold.setVisibility(View.GONE);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

//            notEmptyViewHolder.tvOverdueTask.setText(overduetask.size());

                if (llFabLayout.getVisibility() == View.VISIBLE) {
                    if (position == globalPosition) {
                        currentPosition = position;
                        if (lastSelected) {
                            itemClickListener.OnItemClick(position, task.getTaskName(), task.getTaskId() + "", notEmptyViewHolder.cvMain);
                            notEmptyViewHolder.cvMain.setMaxCardElevation(getPixelsFromDPs(20));
                            notEmptyViewHolder.cvMain.setCardElevation(getPixelsFromDPs(10));

                            fbtnMain.hide();

                            if (llBottom.getVisibility() != View.VISIBLE) {
                                llBottom.setVisibility(View.VISIBLE);
                                llBottom.startAnimation(bottomUp);
                            }

                            lastSelected = false;
                        } else {
                            lastSelected = true;
                            notEmptyViewHolder.cvMain.setMaxCardElevation(getPixelsFromDPs(0));
                            notEmptyViewHolder.cvMain.setCardElevation(getPixelsFromDPs(0));
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    fbtnMain.show();
                                }
                            }, 500);

                            llBottom.setVisibility(View.GONE);
                            llBottom.startAnimation(upBottom);
                        }
                    } else {
                        notEmptyViewHolder.cvMain.setMaxCardElevation(getPixelsFromDPs(0));
                        notEmptyViewHolder.cvMain.setCardElevation(getPixelsFromDPs(0));
                    }
                }
            }
        }


    }

    @Override
    public int getItemCount() {
        return personalTask == null ? 0 : Math.max(1, personalTask.size());
    }

    public void setOnClickListner(OnItemClickListener onClickListner) {
        itemClickListener = onClickListner;

    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ItemFilter();
        } else {
            mFilter = new ItemFilter();
        }
        return mFilter;
    }

    private class ItemFilter extends Filter {

        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                ArrayList<Task> filterList = new ArrayList<>();
                Log.d(TAG, "getCopy size = " + contactArrayListCopy.size());
                for (int i = 0; i < contactArrayListCopy.size(); i++) {
                    if ((contactArrayListCopy.get(i).getTaskName().toLowerCase()).contains(constraint.toString().toLowerCase())) {

                        Task peopleName = contactArrayListCopy.get(i);
                        filterList.add(peopleName);
                    }
                }
                results.count = filterList.size();
                results.values = filterList;

            } else {
                results.count = contactArrayListCopy.size();
                results.values = contactArrayListCopy;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            personalTask = (ArrayList<Task>) results.values;
            notifyDataSetChanged();
        }
    }

    public interface OnItemClickListener {
        void OnItemClick(int position, String taskName, String taskId, CardView cvMain);
    }

    public class NotEmptyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTaskName, tvShared, tvComment, tvLocation, tvCompleted;
        private ImageView ivFirstCharecter, ivFold;
        private CardView cvMain;
        public View viewForeground;
        private Button undoButton;

        public NotEmptyViewHolder(View itemView) {
            super(itemView);

            viewForeground = itemView.findViewById(R.id.view_foreground);
            tvLocation = itemView.findViewById(R.id.tvLocation);
            ivFold = itemView.findViewById(R.id.ivFold);
            cvMain = itemView.findViewById(R.id.cvMain);
            tvTaskName = itemView.findViewById(R.id.tvTaskName);
            tvShared = itemView.findViewById(R.id.tvShared);
            tvComment = itemView.findViewById(R.id.tvComment);
            ivFirstCharecter = itemView.findViewById(R.id.ivFirstCharecter);
            undoButton = itemView.findViewById(R.id.undo_button);
            tvCompleted = itemView.findViewById(R.id.tvCompleted);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    globalPosition = getAdapterPosition();
                    if (currentPosition != getAdapterPosition()) {
                        lastSelected = true;
                    }
                    notifyDataSetChanged();
                }
            });
        }
    }

    public class EmptyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvNoitem, tvAddItem;

        EmptyViewHolder(View itemView) {
            super(itemView);

            tvNoitem = itemView.findViewById(R.id.tvNoitem);
            tvAddItem = itemView.findViewById(R.id.tvAddItem);
        }
    }

    protected int getPixelsFromDPs(int dps) {
        Resources r = mContext.getResources();
        int px = (int) (TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dps, r.getDisplayMetrics()));
        return px;
    }

    public void setUndoOn(boolean undoOn) {
        this.undoOn = undoOn;
    }

    public boolean isUndoOn() {
        return undoOn;
    }

    public void pendingRemoval(int position) {
        final Task item = personalTask.get(position);
        if(!item.isCompleted()) {
            if (!itemsPendingRemoval.contains(item)) {
                itemsPendingRemoval.add(item);
                // this will redraw row in "undo" state
                notifyDataSetChanged();
                // let's create, store and post a runnable to remove the item
                Runnable pendingRemovalRunnable = new Runnable() {
                    @Override
                    public void run() {
                        completeTask(personalTask.indexOf(item));
                    }
                };
                handler.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT);
                pendingRunnables.put(item, pendingRemovalRunnable);
            }
        }
        else
        {
            notifyDataSetChanged();
            Toast.makeText(mContext, item.getTaskName() + " is already Completed.", Toast.LENGTH_SHORT).show();
        }
    }

    public void completeTask(int position) {
        Task item = personalTask.get(position);
        if (itemsPendingRemoval.contains(item)) {
            itemsPendingRemoval.remove(item);
            item.setCompleted(true);
            notifyDataSetChanged();
        }
/*
        if (personalTask.contains(item)) {
                switch (item.getType()) {
                    case Constant.TASK:
                        DataBaseHelper.getInstance().completeTheTask(item.getTaskId());
                        try
                        {
                            if(CommanFunction.isNetworkAvaliable(mContext) && DataBaseHelper.getInstance().isTaskSynced(item.getTaskId()+""))
                                TaskData.completeTaskInServer(DataBaseHelper.getInstance().getServerID(Sqlite_Query.TABLE_TASK,Sqlite_Query.KEY_TASK_ID,item.getTaskId()+""),item.getTaskName(),"1");
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                        break;
                    case Constant.IDEA:
                        DataBaseHelper.getInstance().completeTheIdea(item.getTaskId());
                        try
                        {
                            if(CommanFunction.isNetworkAvaliable(mContext) && DataBaseHelper.getInstance().isIdeaSynced(item.getTaskId()+""))
                                IdeaData.completeIdeaInServer(DataBaseHelper.getInstance().getServerID(Sqlite_Query.TABLE_IDEA,Sqlite_Query.KEY_IDEA_ID,item.getTaskId()+""),item.getTaskName(),"1");
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                        break;
                    case Constant.INQUIRY:
                        DataBaseHelper.getInstance().completeTheInquiry(item.getTaskId());
                        try
                        {
                            if(CommanFunction.isNetworkAvaliable(mContext) && DataBaseHelper.getInstance().isInquirySynced(item.getTaskId()+""))
                                InquiryData.completeInquiryInServer(DataBaseHelper.getInstance().getServerID(Sqlite_Query.TABLE_INQUIRY,Sqlite_Query.KEY_INQUIRY_ID,item.getTaskId()+""),item.getTaskName(),"1");
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                        break;
                }
                Log.e("TASK TYPE = ",""+item.getType());
            item.setCompleted(true);
            notifyDataSetChanged();
            Toast.makeText(mContext, item.getTaskName() + " is Completed.", Toast.LENGTH_SHORT).show();

        }
*/

    }

    public boolean isPendingRemoval(int position) {
        Task item = personalTask.get(position);
        return itemsPendingRemoval.contains(item);
    }
}
