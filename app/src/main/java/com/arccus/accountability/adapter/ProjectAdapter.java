package com.arccus.accountability.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.arccus.accountability.R;
import com.arccus.accountability.activity.CreateTaskActivity;
import com.arccus.accountability.activity.Create_Edit_Tags;
import com.arccus.accountability.activity.EditTagActivity;
import com.arccus.accountability.model.CreateProject;
import com.arccus.accountability.utils.Constant;
import com.arccus.accountability.webservice.StaticDataApi;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Admin on 25/12/2017.
 */

public class ProjectAdapter extends RecyclerView.Adapter<ProjectAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<CreateProject> projects;

    private int type;

    public ProjectAdapter(Context context, ArrayList<CreateProject> arrayList, int types) {
        this.mContext = context;
        this.projects = arrayList;
        this.type = types;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.project_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.tvProjectName.setText(projects.get(position).getProject_name());

        holder.imageViewl.setImageResource(Constant.getProjectColor().get(projects.get(position).getColorPosition()).getColor());

//        if (type == 1) {


//            String  colorName = StaticDataApi.colorResponseArrayList.get(position).getColorName();
//                    ivColor.setImageResource(projectColor.getColor());

         /*   if (colorName.equals("Red")) {

                holder.imageViewl.setBackgroundResource(R.drawable.circle_red);
            } else if (colorName.equals("Green")) {
                holder.imageViewl.setBackgroundResource(R.drawable.circle_green);

            } else if (colorName.equals("Yellow")) {
                holder.imageViewl.setBackgroundResource(R.drawable.circle_yellow);
            }
*/

       /* } else if (type == 2) {
            holder.imageViewl.setImageResource(Constant.getProjectColor().get(projects.get(position).getColorPosition()).getColor());
        }*/
//           String colorName = StaticDataApi.colorResponseArrayList.get(position).getColorName();
//                    ivColor.setImageResource(projectColor.getColor());

           /* if (colorName.equals("Red")) {

                holder.imageViewl.setBackgroundResource(R.drawable.circle_red);
            } else if (colorName.equals("Green")) {
                holder.imageViewl.setBackgroundResource(R.drawable.circle_green);

            } else if (colorName.equals("Yellow")) {
                holder.imageViewl.setBackgroundResource(R.drawable.circle_yellow);
            }*/



    }

    @Override
    public int getItemCount() {
        return projects.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvProjectName;
        private ImageView imageViewl;

        public ViewHolder(View itemView) {
            super(itemView);
            tvProjectName = itemView.findViewById(R.id.tvProjectName);
            imageViewl = itemView.findViewById(R.id.imageView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (type == 1) {
                        Intent intent = new Intent(mContext, CreateTaskActivity.class);
                        intent.putExtra("projectId", projects.get(getAdapterPosition()).getProject_id());
                        intent.putExtra("project_name", projects.get(getAdapterPosition()).getProject_name());
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);
                    } else if (type == 2) {
                        Intent intent = new Intent(mContext, EditTagActivity.class);
                        intent.putExtra(Constant.CREATE_TAG, "Edit Tag");
                        intent.putExtra("tag_id", projects.get(getAdapterPosition()).getProject_id());
                        intent.putExtra("tag_name", projects.get(getAdapterPosition()).getProject_name());
                        intent.putExtra("tag_position", projects.get(getAdapterPosition()).getColorPosition());
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);
                    }
                }
            });
        }
    }
}
