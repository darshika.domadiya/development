package com.arccus.accountability.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.arccus.accountability.R;
import com.arccus.accountability.model.CreateProject;

import java.util.ArrayList;

/**
 * Created by Admin on 03/01/2018.
 */

public class SelectProject extends BaseAdapter {
    Context context;
    LayoutInflater inflter;
    private ArrayList<CreateProject> projectArrayList;

    public SelectProject(Context applicationContext, ArrayList<CreateProject> createProjects) {
        this.context = applicationContext;
        this.projectArrayList = createProjects;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return projectArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflter.inflate(R.layout.custom_spinner_items1, null);

        TextView names = view.findViewById(R.id.textView);
      /*  ImageView imageView = view.findViewById(R.id.imageView);*/
        setMargins(names, 0, 30, 10, 30);


        try {

            names.setText(projectArrayList.get(position).getProject_name());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        //imageView.setBackgroundResource(Constant.colorCode[projectArrayList.get(position).getColorPosition()]);
        return view;
    }

    private void setMargins(View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }
}
