package com.arccus.accountability;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;


/**
 * Created by Admin on 29/12/2017.
 */

public class MyApplicationClass extends Application {

    public static Context context;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);


    }

    @Override
    public void onCreate() {
        super.onCreate();
       /* Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)
                .build();
        Fabric.with(fabric);*/
       context = getApplicationContext();
    }

    public static Context getAppContext()
    {
        return context;
    }
}
