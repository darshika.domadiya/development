package com.arccus.accountability.utils;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arccus.accountability.R;

/**
 * Created by Admin on 22/12/2017.
 */

public class CommonDialog {
    public static String TAG = "CommonDialog";

    public static void DialogAlert(Activity activity, String message) {
        try {
            final Dialog dialog = new Dialog(activity, R.style.AppCompatAlertDialogStyle);
            dialog.requestWindowFeature(1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            View m_view = LayoutInflater.from(activity).inflate(R.layout.dlg_alert, null);
            TextView tvYes = m_view.findViewById(R.id.tvYes);

            tvYes.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            tvYes.setGravity(Gravity.CENTER);
            TextView tvNo = m_view.findViewById(R.id.tvNo);
            View vLine = m_view.findViewById(R.id.vLine);
            vLine.setVisibility(View.GONE);
            tvNo.setVisibility(View.GONE);
            ((TextView) m_view.findViewById(R.id.tvMessage)).setText(message);

            tvYes.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dialog.cancel();
                }
            });
        /*tvNo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.cancel();
            }
        });*/
            dialog.setContentView(m_view);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Dialog Alert Exception = " + e);
        }
    }


}
