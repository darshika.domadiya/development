package com.arccus.accountability.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.ContactsContract;
import android.util.Log;

import com.arccus.accountability.shared_preference.SessionManager;

import java.util.ArrayList;

/**
 * Created by Admin on 22/12/2017.
 */

public class CommanFunction {

    public static ProgressDialog progressDialog;
   // public static ArrayList<Contact> list = new ArrayList<>();

    public static boolean isNetworkAvaliable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        return (connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED)
                || (connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .getState() == NetworkInfo.State.CONNECTED);
    }

    public static ArrayList<String> getContact(Context c) {
        final ArrayList<String> numbers = new ArrayList<String>();
        Cursor phones = c.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while (phones.moveToNext()) {
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            Log.d("phonenumber", phoneNumber);
//            Log.e("phonenumber", phoneNumber);
            numbers.add(phoneNumber);
        }
        phones.close();
        return numbers;
    }

    public static String getServerId(String id)
    {
        return SessionManager.getInstance().getUserId()+id;
    }

    public static String getLocalId(String serverId)
    {
        try {
            return serverId.substring(SessionManager.getInstance().getUserId().length(), serverId.length());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return "";
        }
    }
}
