package com.arccus.accountability.utils;


import android.graphics.drawable.Drawable;
import android.net.Uri;

interface ChipInterface {


    Uri getAvatarUri();
    Drawable getAvatarDrawable();
    String getLabel();
    int getRight();
}
