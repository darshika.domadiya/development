package com.arccus.accountability.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.arccus.accountability.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by hitendra on 10/14/2016.
 */
public class Validator {

    /**
     * @param context application context
     * @param etEmail EditText object
     * @return true or false
     * @brief methods for checking email is valid or not
     */
    public static boolean isValidEmail(Context context, EditText etEmail) {
        try {
            String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            Pattern pattern = Pattern.compile(EMAIL_PATTERN);
            Matcher matcher = pattern.matcher(etEmail.getText());

            if (matcher.matches()) {
                etEmail.setError(null);
            } else {
                etEmail.setError(context.getResources().getString(R.string.invalid_email_address));
            }
            return matcher.matches();
        } catch (Exception e) {
            Log.d("JD", "e isValid Email = " + e);
            e.printStackTrace();
        }
        return false;

    }

    /**
     * @param context    application context
     * @param etPassword EditText object
     * @return true or false
     * @brief methods for checking password is valid or not
     */
    public static boolean isPasswordValid(Context context, EditText etPassword) {
        if (etPassword.getText().length() < 6) {
            etPassword.setError(context.getResources().getString(R.string.invalid_password));
            return false;
        } else {
            etPassword.setError(null);
            return true;
        }
    }

    /**
     * @param context           application context
     * @param etPassword        EditText object
     * @param etConfirmPassword EditText object retype password
     * @return true or false
     * @brief methods for checking mobile number is valid or not
     */


    /**
     * @param context application context
     * @param etField EditText object
     * @return true or false
     * @brief methods for checking input field is null or not
     */
    public static boolean isEmptyField(Context context, EditText etField) {
        try {
            if (TextUtils.isEmpty(etField.getText().toString().trim())) {
                etField.setError(context.getResources().getString(R.string.empty_filed));
                return false;
            } else {
                etField.setError(null);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("JD", "e = " + e);
        }
        return false;
    }

    public static boolean isEmptyFieldTextView(Context context, TextView etField) {
        try {
            if (TextUtils.isEmpty(etField.getText().toString().trim())) {
                etField.setError(context.getResources().getString(R.string.empty_filed));
                return false;
            } else {
                etField.setError(null);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("JD", "e = " + e);
        }
        return false;
    }

    public static boolean isValidMobileNo(Context context, EditText etEmail) {
        try {
            if (android.util.Patterns.PHONE.matcher(etEmail.getText().toString()).matches()) {
                etEmail.setError(null);
            } else {
                etEmail.setError(context.getResources().getString(R.string.invalid_email_or_phone_address));
            }
            return android.util.Patterns.PHONE.matcher(etEmail.getText().toString()).matches();
        } catch (Exception e) {
            Log.d("JD", "e isValid Email = " + e);
            e.printStackTrace();
        }
        return false;

    }
}
