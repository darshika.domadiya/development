package com.arccus.accountability.utils;


import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arccus.accountability.R;

import androidx.annotation.ColorInt;
import androidx.core.content.ContextCompat;


public class ChipView extends RelativeLayout {


    // attributes
    private static final int NONE = -1;
    // xml elements
    private LinearLayout mContentLayout;
    private ImageView mAvatarIconImageView;
    private TextView mLabelTextView;
    private ImageButton mDeleteButton;
    // context
    private Context mContext;
    private String mLabel;
    private ColorStateList mLabelColor;
    private boolean mHasAvatarIcon = false;
    private Drawable mAvatarIconDrawable;
    private Uri mAvatarIconUri;
    private boolean mDeletable = false;
    private Drawable mDeleteIcon;
    private ColorStateList mDeleteIconColor;
    private ColorStateList mBackgroundColor;

     // letter tile provider
    private LetterTileProviders mLetterTileProvider;
    // chip
    private ChipInterface mChip;

    public ChipView(Context context) {
        super(context);
        mContext = context;
        init(null);
    }

    public ChipView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init(attrs);
    }

    /**
     * Inflate the view according to attributes
     *
     * @param attrs the attributes
     */
    private void init(AttributeSet attrs) {
        // inflate layout
        View rootView = inflate(getContext(), R.layout.chip_view, this);
        // butter knife

        mContentLayout = rootView.findViewById(R.id.content);
        mAvatarIconImageView = rootView.findViewById(R.id.icon);
        mLabelTextView = rootView.findViewById(R.id.label);

        mDeleteButton = rootView.findViewById(R.id.delete_button);
        // letter tile provider
        mLetterTileProvider = new LetterTileProviders(mContext);

        // attributes
        if (attrs != null) {
            TypedArray a = mContext.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.ChipView,
                    0, 0);

            try {
                // label
                mLabel = a.getString(R.styleable.ChipView_label);
                mLabelColor = a.getColorStateList(R.styleable.ChipView_labelColor);
                // avatar icon
                mHasAvatarIcon = a.getBoolean(R.styleable.ChipView_hasAvatarIcon, false);
                int avatarIconId = a.getResourceId(R.styleable.ChipView_avatarIcon, NONE);
                if (avatarIconId != NONE)
                    mAvatarIconDrawable = ContextCompat.getDrawable(mContext, avatarIconId);
                if (mAvatarIconDrawable != null) mHasAvatarIcon = true;
                // delete icon
                mDeletable = a.getBoolean(R.styleable.ChipView_deletable, false);
                mDeleteIconColor = a.getColorStateList(R.styleable.ChipView_deleteIconColor);
                int deleteIconId = a.getResourceId(R.styleable.ChipView_deleteIcon, NONE);
                if (deleteIconId != NONE)
                    mDeleteIcon = ContextCompat.getDrawable(mContext, deleteIconId);
                // background color
                mBackgroundColor = a.getColorStateList(R.styleable.ChipView_backgroundColor);


            } finally {
                a.recycle();
            }
        }

        // inflate
        inflateWithAttributes();
    }

    /**
     * Inflate the view
     */
    private void inflateWithAttributes() {
        // label
        setLabel(mLabel);
        if (mLabelColor != null)
            setLabelColor(mLabelColor);

        // avatar
        setHasAvatarIcon(mHasAvatarIcon);

        // delete button
        setDeletable(mDeletable);

        // background color
        if (mBackgroundColor != null)
            setChipBackgroundColor(mBackgroundColor);
    }


    /**
     * Get label
     *
     * @return the label
     */
    public String getLabel() {
        return mLabel;
    }

    /**
     * Set label
     *
     * @param label the label to set
     */
    public void setLabel(String label) {
        mLabel = label;
        mLabelTextView.setText(label);
    }

    /**
     * Set label color
     *
     * @param color the color to set
     */
    private void setLabelColor(ColorStateList color) {
        mLabelColor = color;
        mLabelTextView.setTextColor(color);
    }

    /**
     * Set label color
     *
     */
    public void setLabelColor() {
        mLabelColor = ColorStateList.valueOf(android.graphics.Color.WHITE);
        mLabelTextView.setTextColor(android.graphics.Color.WHITE);
    }

    /**
     * Show or hide avatar icon
     *
     * @param hasAvatarIcon true to show, false to hide
     */
    private void setHasAvatarIcon(boolean hasAvatarIcon) {
        mHasAvatarIcon = hasAvatarIcon;

        if (!mHasAvatarIcon) {
            // hide icon
            mAvatarIconImageView.setVisibility(GONE);
            // adjust padding
            if (mDeleteButton.getVisibility() == VISIBLE)
                mLabelTextView.setPadding(ViewUtil.dpToPx(12), 0, 0, 0);
            else
                mLabelTextView.setPadding(ViewUtil.dpToPx(12), 0, ViewUtil.dpToPx(12), 0);

        } else {
            // show icon
            mAvatarIconImageView.setVisibility(VISIBLE);
            // adjust padding
            if (mDeleteButton.getVisibility() == VISIBLE)
                mLabelTextView.setPadding(ViewUtil.dpToPx(8), 0, 0, 0);
            else
                mLabelTextView.setPadding(ViewUtil.dpToPx(8), 0, ViewUtil.dpToPx(12), 0);

            // set icon
            if (mAvatarIconUri != null)
                mAvatarIconImageView.setImageURI(mAvatarIconUri);
            else if (mAvatarIconDrawable != null)
                mAvatarIconImageView.setImageDrawable(mAvatarIconDrawable);
            else
                mAvatarIconImageView.setImageBitmap(mLetterTileProvider.getLetterTile(getLabel()));
        }
    }

    /**
     * Set avatar icon
     *
     * @param avatarIcon the icon to set
     */
    public void setAvatarIcon(Drawable avatarIcon) {
        mAvatarIconDrawable = avatarIcon;
        mHasAvatarIcon = true;
        inflateWithAttributes();
    }

    /**
     * Set avatar icon
     *
     * @param avatarUri the uri of the icon to set
     */

    /**
     * Show or hide delte button
     *
     * @param deletable true to show, false to hide
     */
    public void setDeletable(boolean deletable) {
        mDeletable = deletable;
        if (!mDeletable) {
            // hide delete icon
            mDeleteButton.setVisibility(GONE);
            // adjust padding
            if (mAvatarIconImageView.getVisibility() == VISIBLE)
                mLabelTextView.setPadding(ViewUtil.dpToPx(8), 0, ViewUtil.dpToPx(12), 0);
            else
                mLabelTextView.setPadding(ViewUtil.dpToPx(12), 0, ViewUtil.dpToPx(12), 0);
        } else {
            // show icon
            mDeleteButton.setVisibility(VISIBLE);
            // adjust padding
            if (mAvatarIconImageView.getVisibility() == VISIBLE)
                mLabelTextView.setPadding(ViewUtil.dpToPx(8), 0, 0, 0);
            else
                mLabelTextView.setPadding(ViewUtil.dpToPx(12), 0, 0, 0);

            // set icon
            if (mDeleteIcon != null)
                mDeleteButton.setImageDrawable(mDeleteIcon);
            if (mDeleteIconColor != null)
                mDeleteButton.getDrawable().mutate().setColorFilter(mDeleteIconColor.getDefaultColor(), PorterDuff.Mode.SRC_ATOP);
        }
    }

    /**
     * Set delete icon color
     *
     * @param color the color to set
     */




    /**
     * Set delete icon
     *
     * @param deleteIcon the icon to set
     */


    /**
     * Set background color
     *
     * @param color the color to set
     */
    private void setChipBackgroundColor(ColorStateList color) {
        mBackgroundColor = color;
        setChipBackgroundColor(color.getDefaultColor());
    }

    /**
     * Set background color
     *
     * @param color the color to set
     */
    public void setChipBackgroundColor(@ColorInt int color) {
        mBackgroundColor = ColorStateList.valueOf(color);
        mContentLayout.getBackground().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
    }

    /**
     * Set the chip object
     *
     * @param chip the chip
     */

    /**
     * Set OnClickListener on the delete button
     *
     * @param onClickListener the OnClickListener
     */
    public void setOnDeleteClicked(OnClickListener onClickListener) {
        mDeleteButton.setOnClickListener(onClickListener);
    }





    public static class Builder {

        private boolean hasAvatarIcon = false;
        private boolean deletable = false;
    }
}
