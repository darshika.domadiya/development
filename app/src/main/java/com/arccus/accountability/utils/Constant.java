package com.arccus.accountability.utils;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

import com.arccus.accountability.R;
import com.arccus.accountability.model.Priority;
import com.arccus.accountability.model.ProjectColor;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Admin on 23/12/2017.
 */

public class Constant {


    public static final String TAG = "ToDoCute";
    public static final String APP_LOGIN = "1";


    public static final String CREATE_PROJECTS = "create_project";
    public static final String CREATE_TAG = "create_tag";

    public static final String PUTEXTRA = "putextra";
    public static final String TASK = "Task";
    public static final String IDEA = "Idea";
    public static final String INQUIRY = "Inquiry";
    public static final String FILTER = "Filter";

    public static final String WHICHTASK = "whichtask";

    public static final String PERSONAL = "personal";
    public static final String NEXT7DAY = "next7day";
    public static final String TODAY = "today";
    public static final String PROJECT = "project";
    public static final String ALLOCATE_ME = "project_allocate_me";
    public static final String ALLOCATE_OTHERS = "project_allocate_others";
    public static final String HIGH_PRIORITY = "High";
    public static final String MEDIUM_PRIORITY = "Medium";
    public static final String LOW_PRIORITY = "Low";
    public static final String VIEW_ALL = "All";
    public static final String NO_DUE_DATE = "NoDueDate";


    public static final String CURRENT_DATE = DateFormat.getDateInstance().format(new Date());
    public static final String CURRENT_TIME = DateFormat.getTimeInstance().format(new Date());
    //Constant Arrayt List


    public static final ArrayList<ProjectColor> getProjectColor() {

        ArrayList<ProjectColor> data1 = new ArrayList<>();
        data1.add(new ProjectColor(R.drawable.circle_red, "Red"));
        data1.add(new ProjectColor(R.drawable.circle_green, "Green"));
//        data1.add(new ProjectColor(R.drawable.circle_blue, "Blue"));
//        data1.add(new ProjectColor(R.drawable.circle_purple, "Purple"));
        data1.add(new ProjectColor(R.drawable.circle_yellow, "Yellow"));
//        data1.add(new ProjectColor(R.drawable.circle_orange, "Orange"));
//        data1.add(new ProjectColor(R.drawable.circle_pink, "Pink"));
        return data1;
    }

    public static final ArrayList<ProjectColor> getTagColor() {
        ArrayList<ProjectColor> projectColors = new ArrayList<>();
        projectColors.add(new ProjectColor(R.drawable.tag_red, "Red"));
        projectColors.add(new ProjectColor(R.drawable.tag_green, "Green"));
        projectColors.add(new ProjectColor(R.drawable.tag_blue, "Blue"));
        projectColors.add(new ProjectColor(R.drawable.tag_purple, "Purple"));
        projectColors.add(new ProjectColor(R.drawable.tag_yellow, "Yellow"));
        projectColors.add(new ProjectColor(R.drawable.tag_orange, "Orange"));
        projectColors.add(new ProjectColor(R.drawable.tag_pink, "Pink"));
        return projectColors;
    }

    public static final ArrayList<Priority> getPriority() {

        ArrayList<Priority> priority = new ArrayList<>();
        priority.add(new Priority(R.drawable.circle_red, "High"));
        priority.add(new Priority(R.drawable.circle_orange, "Medium"));
        priority.add(new Priority(R.drawable.circle_green, "Low"));
        return priority;
    }


    public static void closeKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        assert inputManager != null;
        inputManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

    }



    public static void openKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
        }
    }
}
