/*
package com.arccus.accountability.utils;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arccus.accountability.R;
import com.arccus.accountability.adapter.AdapterList;
import com.arccus.accountability.adapter.LocationAdapter;
import com.arccus.accountability.adapter.TaskAdapter;
import com.arccus.accountability.fontcontrol.EditTextControl;
import com.arccus.accountability.fragment.IdeaFragment;
import com.arccus.accountability.fragment.InquiryFragment;
import com.arccus.accountability.fragment.Next7DayFragment.Next7DayFragment;
import com.arccus.accountability.fragment.PersonalFragment.PersonalFragment;
import com.arccus.accountability.fragment.TaskFragment;
import com.arccus.accountability.helper.DataBaseHelper;
import com.arccus.accountability.helper.Sqlite_Query;
import com.arccus.accountability.model.LocationModel;
import com.arccus.accountability.model.Task;
import com.arccus.accountability.slidedatetimepicker.DateTimePicker;
import com.arccus.accountability.slidedatetimepicker.SimpleDateTimePicker;
import com.arccus.accountability.webservices.IdeaData;
import com.arccus.accountability.webservices.InquiryData;
import com.arccus.accountability.webservices.TaskData;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import org.apmem.tools.layouts.FlowLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import static com.arccus.accountability.utils.Constant.IDEA;
import static com.arccus.accountability.utils.Constant.INQUIRY;
import static com.arccus.accountability.utils.Constant.TASK;

*/
/**
 * Created by JD on 27-02-2018.
 *//*


public class OpenCommanDialog {
    private Activity activity;
    private DataBaseHelper dataBaseHelper;
    private ChipView cvProject, cvCalander, cvContact, cvTag, cvPriority, cvComment;
    private String selectProjectId = " " , selectTagName = " ", selectPriority = " ", selectContact = " ", selectComment = " ";
    private boolean calanderClick = false;
    private String selectDate;
    private String currentDate;
    private FlowLayout llChipView;
    private EditText etvAddTask;

    private ImageView ivTag, ivAssign, ivProject, ivAddCalander, ivPriority, ivCommant, ivCalander, ivAlarm;
    private RecyclerView rvProjectlist, rvContactList, rvTag, rvPriority;
    private AdapterList adapterList1, adapterList2, adapterList3, adapterList4;
    private FrameLayout fm;
    private LinearLayout llBottom, llFabLayout, flAddTask, flAddIdea, flAddInquiry;
    private ViewPager viewPager;
    private boolean isFABOpen = false;
    private RelativeLayout rlMain;
    private FloatingActionButton fbtnMain;
    private String whereToCome;

    private String categoryName;

    private SimpleDateFormat mFormatter = new SimpleDateFormat("yyyy-MM-dd");
    private String currentDateFormat = new String("yyyy-MM-dd");

    public OpenCommanDialog(String whereToCome, ViewPager viewPager) {
        this.whereToCome = whereToCome;
        this.viewPager = viewPager;
    }


    public OpenCommanDialog(String whereToCome, final Activity act, FlowLayout flowLayout, EditText etvAddTask,
                            final ImageView ivTag, final ImageView ivAssign, final ImageView ivProject,
                            final ImageView ivAddCalander, final ImageView ivPriority, final ImageView ivCommant,
                            RecyclerView rvProjectlist, RecyclerView rvContactList, RecyclerView rvTag, RecyclerView rvPriority,
                            FrameLayout fm, LinearLayout llBottom, LinearLayout llFabLayout, ViewPager viewPager,
                            ImageView ivCalander, ImageView ivAlarm, RelativeLayout rlMain,
                            LinearLayout flAddTask, LinearLayout flAddIdea, LinearLayout flAddInquiry,
                            FloatingActionButton fbtnMain, String projectId) {
        this.activity = act;
        this.whereToCome = whereToCome;
        this.llChipView = flowLayout;
        this.etvAddTask = etvAddTask;
        dataBaseHelper = new DataBaseHelper(activity);
        this.ivTag = ivTag;
        this.ivAssign = ivAssign;
        this.ivProject = ivProject;
        this.ivAddCalander = ivAddCalander;
        this.ivPriority = ivPriority;
        this.ivCommant = ivCommant;
        this.rvProjectlist = rvProjectlist;
        this.rvContactList = rvContactList;
        this.rvTag = rvTag;
        this.rvPriority = rvPriority;
        this.fm = fm;
        this.llBottom = llBottom;
        this.llFabLayout = llFabLayout;
        this.viewPager = viewPager;
        this.ivCalander = ivCalander;
        this.ivAlarm = ivAlarm;
        this.rlMain = rlMain;
        this.flAddTask = flAddTask;
        this.flAddIdea = flAddIdea;
        this.flAddInquiry = flAddInquiry;
        this.fbtnMain = fbtnMain;
        this.selectProjectId = projectId;
        setAdapter();

        cvProject = new ChipView(activity);
        cvCalander = new ChipView(activity);
        cvContact = new ChipView(activity);
        cvTag = new ChipView(activity);
        cvPriority = new ChipView(activity);
        cvComment = new ChipView(activity);
        cvTag.setOnDeleteClicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llChipView.removeView(cvTag);
                cvTag.setLabel("");
                DrawableCompat.setTint(ivTag.getDrawable(), ContextCompat.getColor(activity, R.color.button_background));
                selectTagName = " ";
            }
        });
        cvContact.setOnDeleteClicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llChipView.removeView(cvContact);
                cvContact.setLabel("");
                DrawableCompat.setTint(ivAssign.getDrawable(), ContextCompat.getColor(activity, R.color.button_background));
                selectContact = " ";
            }
        });

        cvProject.setOnDeleteClicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectProjectId = " ";
                llChipView.removeView(cvProject);
                cvProject.setLabel("");
                DrawableCompat.setTint(ivProject.getDrawable(), ContextCompat.getColor(activity, R.color.button_background));
            }
        });


        cvCalander.setOnDeleteClicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //selectDate = " ";
                llChipView.removeView(cvCalander);
                cvCalander.setLabel("");
                DrawableCompat.setTint(ivAddCalander.getDrawable(), ContextCompat.getColor(activity, R.color.button_background));
            }
        });

        cvPriority.setOnDeleteClicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPriority = " ";
                llChipView.removeView(cvPriority);
                cvPriority.setLabel("");
                DrawableCompat.setTint(ivPriority.getDrawable(), ContextCompat.getColor(activity, R.color.button_background));
            }
        });
        cvComment.setOnDeleteClicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectComment = " ";
                llChipView.removeView(cvComment);
                cvComment.setLabel("");
                DrawableCompat.setTint(ivCommant.getDrawable(), ContextCompat.getColor(activity, R.color.button_background));
            }
        });

        fbtnMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isFABOpen) {
                    showFABMenu();
                } else {
                    closeFABMenu();
                }

            }
        });

    }

    private void setAdapter() {
        adapterList1 = new AdapterList(activity, 1, dataBaseHelper, selectProjectId);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
        rvProjectlist.setLayoutManager(layoutManager);
        DividerItemDecoration divider = new DividerItemDecoration(rvProjectlist.getContext(), DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(activity, R.drawable.custom_divider));
        rvProjectlist.addItemDecoration(divider);
        rvProjectlist.setAdapter(adapterList1);


        adapterList2 = new AdapterList(activity, 2, dataBaseHelper, selectProjectId);
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(activity);
        rvContactList.setLayoutManager(layoutManager1);
        divider = new DividerItemDecoration(rvContactList.getContext(), DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(activity, R.drawable.custom_divider));
        rvContactList.addItemDecoration(divider);
        rvContactList.setAdapter(adapterList2);


        adapterList3 = new AdapterList(activity, 3, dataBaseHelper, selectProjectId);
        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(activity);
        rvTag.setLayoutManager(layoutManager2);
        divider.setDrawable(ContextCompat.getDrawable(activity, R.drawable.custom_divider));
        rvTag.addItemDecoration(divider);
        rvTag.setAdapter(adapterList3);


        adapterList4 = new AdapterList(activity, 4, dataBaseHelper, selectProjectId);
        RecyclerView.LayoutManager layoutManager3 = new LinearLayoutManager(activity);
        rvPriority.setLayoutManager(layoutManager3);
        divider.setDrawable(ContextCompat.getDrawable(activity, R.drawable.custom_divider));
        rvPriority.addItemDecoration(divider);
        rvPriority.setAdapter(adapterList4);

        etvAddTask.setKeyImeChangeListener(new EditTextControl.KeyImeChange() {
            @Override
            public void onKeyIme(int keyCode) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    fm.setVisibility(View.GONE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (llBottom.getVisibility() == View.VISIBLE) {
                                llFabLayout.setVisibility(View.GONE);
                            } else {
                                llFabLayout.setVisibility(View.VISIBLE);
                            }
                        }
                    }, 200);
                    closeFABMenu();
                }
            }
        });

        etvAddTask.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (etvAddTask.getText().toString().trim().length() != 0) {

                        if (whereToCome.equalsIgnoreCase(Constant.PERSONAL)) {
                            if (PersonalFragment.categoryName.equalsIgnoreCase(TASK)) {
                                addTaskDatabase(TASK);
                                setTaskFragmnet(TASK, personalTaskFragment, ivCalander, ivAlarm);
                            } else if (PersonalFragment.categoryName.equalsIgnoreCase(Constant.IDEA)) {
                                addTaskDatabase(Constant.IDEA);
                                setTaskFragmnet(Constant.IDEA, personalIdeaFragment, ivCalander, ivAlarm);
                            } else if (PersonalFragment.categoryName.equalsIgnoreCase(Constant.INQUIRY)) {
                                addTaskDatabase(Constant.INQUIRY);
                                setTaskFragmnet(Constant.INQUIRY, personalInquiryFragment, ivCalander, ivAlarm);
                            }
                        } else if (whereToCome.equalsIgnoreCase(Constant.NEXT7DAY)) {
                            if (Next7DayFragment.categoryName.equalsIgnoreCase(TASK)) {
                                addTaskDatabase(TASK);
                                setTaskFragmnet(TASK, personalTaskFragment, ivCalander, ivAlarm);
                            } else if (Next7DayFragment.categoryName.equalsIgnoreCase(Constant.INQUIRY)) {
                                addTaskDatabase(Constant.INQUIRY);
                                setTaskFragmnet(Constant.INQUIRY, personalInquiryFragment, ivCalander, ivAlarm);
                            }
                        }

                        resetUI();
                        Constant.closeKeyboard(activity);
                        fm.setVisibility(View.GONE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                llFabLayout.setVisibility(View.VISIBLE);
                            }
                        }, 200);
                        closeFABMenu();
                    } else {
                        Toast.makeText(activity, "Please Enter Task Name", Toast.LENGTH_SHORT).show();
                        return true;
                    }
                }
                return false;
            }
        });

        etvAddTask.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @SuppressLint("WrongConstant")
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (rvProjectlist.getVisibility() == View.VISIBLE ||
                        rvContactList.getVisibility() == View.VISIBLE ||
                        rvTag.getVisibility() == View.VISIBLE ||
                        rvPriority.getVisibility() == View.VISIBLE) {
                    rvProjectlist.setVisibility(View.GONE);
                    rvContactList.setVisibility(View.GONE);
                    rvTag.setVisibility(View.GONE);
                    rvPriority.setVisibility(View.GONE);
                }
                if (count == 0) {
                    rvProjectlist.setVisibility(View.GONE);
                    rvContactList.setVisibility(View.GONE);
                    rvTag.setVisibility(View.GONE);
                    rvPriority.setVisibility(View.GONE);
                } else {
                    try {
                        char currentChar = 0,lastChar = 0;
                        try {
                            currentChar = s.charAt(start);
                            lastChar = s.charAt(before);
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }


                        if (String.valueOf(currentChar).equalsIgnoreCase("@") || String.valueOf(lastChar).equalsIgnoreCase("@")) {
                            rvProjectlist.setVisibility(View.VISIBLE);
                        } else if (String.valueOf(currentChar).equalsIgnoreCase("#") || String.valueOf(lastChar).equalsIgnoreCase("#")) {
                            rvContactList.setVisibility(View.VISIBLE);
                        } else if (String.valueOf(currentChar).equalsIgnoreCase("!") || String.valueOf(lastChar).equalsIgnoreCase("!")) {
                            rvPriority.setVisibility(View.VISIBLE);
                        } else if (String.valueOf(currentChar).equalsIgnoreCase("*") || String.valueOf(lastChar).equalsIgnoreCase("*")) {
                            rvTag.setVisibility(View.VISIBLE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

*/
/*
        adapterList1.setOnItemClickListner(new AdapterList.OnItemClickListener() {
            @Override
            public void onIemClick(String id) {
                selectProjectId = id;
                String name = dataBaseHelper.getProjectName(id);
                if (!TextUtils.isEmpty(cvProject.getLabel())) {
                    setChiupLable(cvProject, name, etvAddTask);
                } else {
                    setChipView(cvProject, R.drawable.ic_project_white, R.color.signup_button_background, name);
                }
                DrawableCompat.setTint(ivProject.getDrawable(), ContextCompat.getColor(activity, R.color.signup_button_background));
                etvAddTask.setRawInputType(InputType.TYPE_CLASS_TEXT);
                etvAddTask.setTextIsSelectable(true);
                rvProjectlist.setVisibility(View.GONE);

            }
        });
*//*


*/
/*
        adapterList2.setOnItemClickListner(new AdapterList.OnItemClickListener() {
            @Override
            public void onIemClick(String name) {
                selectContact = name;
                if (!TextUtils.isEmpty(cvContact.getLabel())) {
                    setChiupLable(cvContact, name, etvAddTask);
                } else {
                    setChipView(cvContact, R.drawable.ic_project_white, R.color.signup_button_background, name);
                }
                DrawableCompat.setTint(ivAssign.getDrawable(), ContextCompat.getColor(activity, R.color.signup_button_background));
                etvAddTask.setRawInputType(InputType.TYPE_CLASS_TEXT);
                etvAddTask.setTextIsSelectable(true);
                rvContactList.setVisibility(View.GONE);
            }
        });
*//*


*/
/*
        adapterList3.setOnItemClickListner(
                new AdapterList.OnItemClickListener() {
                    @Override
                    public void onIemClick(String name) {
                        selectTagName = name;
                        if (!TextUtils.isEmpty(cvTag.getLabel())) {
                            setChiupLable(cvTag, name, etvAddTask);
                        } else {
                            setChipView(cvTag, R.drawable.ic_project_white, R.color.signup_button_background, name);
                        }
                        DrawableCompat.setTint(ivTag.getDrawable(), ContextCompat.getColor(activity, R.color.signup_button_background));
                        etvAddTask.setRawInputType(InputType.TYPE_CLASS_TEXT);
                        etvAddTask.setTextIsSelectable(true);
                        rvTag.setVisibility(View.GONE);
                    }
                });
*//*


*/
/*
        adapterList4.setOnItemClickListner(new AdapterList.OnItemClickListener() {
            @Override
            public void onIemClick(String name) {
                selectPriority = name;
                if (!TextUtils.isEmpty(cvPriority.getLabel())) {
                    setChiupLable(cvPriority, name, etvAddTask);
                } else {
                    setChipView(cvPriority, R.drawable.ic_project_white, R.color.signup_button_background, name);
                }
                DrawableCompat.setTint(ivPriority.getDrawable(), ContextCompat.getColor(activity, R.color.signup_button_background));
                etvAddTask.setRawInputType(InputType.TYPE_CLASS_TEXT);
                etvAddTask.setTextIsSelectable(true);
                rvPriority.setVisibility(View.GONE);
            }
        });
*//*

    }


    public void showFABMenu() {
        rlMain.setBackgroundColor(ContextCompat.getColor(activity, R.color.black_semi_transparent));

        isFABOpen = true;
        flAddTask.setVisibility(View.VISIBLE);
        flAddIdea.setVisibility(View.VISIBLE);
        flAddInquiry.setVisibility(View.VISIBLE);

        fbtnMain.animate().rotationBy(135);
        flAddTask.animate().translationY(-activity.getResources().getDimension(R.dimen._10sdp));
        flAddIdea.animate().translationY(-activity.getResources().getDimension(R.dimen._20sdp));
        flAddInquiry.animate().translationY(-activity.getResources().getDimension(R.dimen._30sdp));
    }

    public void closeFABMenu() {

        isFABOpen = false;
        flAddTask.animate().translationY(activity.getResources().getDimension(R.dimen._10sdp));
        flAddIdea.animate().translationY(activity.getResources().getDimension(R.dimen._20sdp));
        flAddInquiry.animate().translationY(activity.getResources().getDimension(R.dimen._30sdp)).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (!isFABOpen) {
                    rlMain.setBackgroundColor(Color.TRANSPARENT);
                    flAddTask.setVisibility(View.GONE);
                    flAddIdea.setVisibility(View.GONE);
                    flAddInquiry.setVisibility(View.GONE);
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        fbtnMain.animate().rotationBy(-135);
    }

*/
/*
    public void addTaskDatabase(String name) {
        try {
            String serverProjectId = "";
            if(!TextUtils.isEmpty(selectProjectId()))
                serverProjectId = DataBaseHelper.getInstance().getServerID(Sqlite_Query.TABLE_PROJECT, Sqlite_Query.KEY_PROJECT_ID, selectProjectId());
            switch (name) {
                case TASK:
                    long taskId = dataBaseHelper.addTask(etvAddTask.getText().toString().trim(), selectProjectId(), selectDate(),
                            selectTagName(), selectPriority(),
                            selectContact(), selectComment());
                    dataBaseHelper.setServerID(Sqlite_Query.TABLE_TASK, CommanFunction.getServerId(taskId + ""), Sqlite_Query.KEY_TASK_ID, taskId + "");
                    TaskData.addTaskInServer(CommanFunction.getServerId(taskId + ""), serverProjectId,
                            etvAddTask.getText().toString().trim(), selectTagName(), selectPriority(), selectContact());
                    break;
                case IDEA:
                    taskId = dataBaseHelper.addIdea(etvAddTask.getText().toString().trim(), selectProjectId(), selectDate(),
                            selectTagName(), selectPriority(),
                            selectContact(), selectComment());
                    dataBaseHelper.setServerID(Sqlite_Query.TABLE_IDEA, CommanFunction.getServerId(taskId + ""), Sqlite_Query.KEY_IDEA_ID, taskId + "");
                    IdeaData.addIdeaInServer(CommanFunction.getServerId(taskId + "") + "", serverProjectId,
                            etvAddTask.getText().toString().trim(), "", selectTagName(),
                            selectPriority(), selectContact());
                    break;
                case INQUIRY:
                    taskId = dataBaseHelper.addInquiry(etvAddTask.getText().toString().trim(), selectProjectId(), selectDate(),
                            selectTagName(), selectPriority(),
                            selectContact(), selectComment());
                    dataBaseHelper.setServerID(Sqlite_Query.TABLE_INQUIRY, CommanFunction.getServerId(taskId + ""), Sqlite_Query.KEY_INQUIRY_ID, taskId + "");
                    InquiryData.addInquiryInServer(CommanFunction.getServerId(taskId + ""), serverProjectId,
                            etvAddTask.getText().toString().trim(), "", selectTagName(),
                            selectPriority(), selectContact());
                    break;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
*//*


*/
/*
    public void addProjectTask(String name, String projectId) {
        String serverProjectId = DataBaseHelper.getInstance().getServerID(Sqlite_Query.TABLE_PROJECT, Sqlite_Query.KEY_PROJECT_ID,projectId);
        switch (name) {
            case TASK:
                long taskId = dataBaseHelper.addTask(etvAddTask.getText().toString().trim(), projectId, selectDate(),
                        selectTagName(), selectPriority(),
                        selectContact(), selectComment());
                dataBaseHelper.setServerID(Sqlite_Query.TABLE_TASK, CommanFunction.getServerId(taskId+""), Sqlite_Query.KEY_TASK_ID,taskId+"");
                TaskData.addTaskInServer(CommanFunction.getServerId(taskId + ""), serverProjectId + "",
                    etvAddTask.getText().toString().trim(), selectTagName(), selectPriority(), selectContact());
                break;
            case IDEA:
                taskId = dataBaseHelper.addIdea(etvAddTask.getText().toString().trim(), projectId, selectDate(),
                        selectTagName(), selectPriority(),
                        selectContact(), selectComment());
                dataBaseHelper.setServerID(Sqlite_Query.TABLE_IDEA, CommanFunction.getServerId(taskId+""), Sqlite_Query.KEY_IDEA_ID,taskId+"");
                IdeaData.addIdeaInServer(CommanFunction.getServerId(taskId + ""),serverProjectId,
                        etvAddTask.getText().toString().trim(), "", selectTagName(),
                        selectPriority(), selectContact());
                break;
            case INQUIRY:
                taskId = dataBaseHelper.addInquiry(etvAddTask.getText().toString().trim(), projectId, selectDate(),
                        selectTagName(), selectPriority(),
                        selectContact(), selectComment());
                dataBaseHelper.setServerID(Sqlite_Query.TABLE_INQUIRY, CommanFunction.getServerId(taskId+""), Sqlite_Query.KEY_INQUIRY_ID,taskId+"");
                InquiryData.addInquiryInServer(CommanFunction.getServerId(taskId + ""),serverProjectId,
                        etvAddTask.getText().toString().trim(), "", selectTagName(),
                        selectPriority(), selectContact());
                break;
        }
    }
*//*


    public String selectProjectId() {
        return selectProjectId;
    }

    public String selectTagName() {
        return selectTagName;
    }

    public String selectPriority() {
        return selectPriority;
    }

    public String selectContact() {
        return selectContact;
    }


*/
/*
    public void openLocationDialog(final String taskName) {
        try {
            final Dialog dialog = new Dialog(activity, R.style.AppCompatAlertDialogStyle);
            dialog.requestWindowFeature(1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            View m_view = LayoutInflater.from(activity).inflate(R.layout.dlg_show_project, null);

            ImageView ivCloseDialog = m_view.findViewById(R.id.ivCloseDialog);
            ivCloseDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });

            RecyclerView rvProjectList = m_view.findViewById(R.id.rvProjectList);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
            rvProjectList.setLayoutManager(layoutManager);

            LocationAdapter showProjectColor = new LocationAdapter(activity, dataBaseHelper.getLocationList(), 1);
            rvProjectList.setAdapter(showProjectColor);
            showProjectColor.setOnItemClickListener(new LocationAdapter.OnItemClickListener() {
                @Override
                public void OnItemClick(LocationModel locationName) {
                    dataBaseHelper.updateTaskLocation(taskName, locationName.getLocationName());
                    Log.e("location", locationName.getLocationName());

                    refreshTab(Constant.PERSONAL);
                    dialog.cancel();
                }
            });

            dialog.setContentView(m_view);
            dialog.setCancelable(true);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(Constant.TAG, "Dialog Alert Exception = " + e);
        }
    }
*//*


    public void resetUI() {
        etvAddTask.setText("");

        llChipView.removeView(cvTag);
        cvTag.setLabel("");
        DrawableCompat.setTint(ivTag.getDrawable(), ContextCompat.getColor(activity, R.color.button_background));
        selectTagName = " ";

        llChipView.removeView(cvContact);
        cvContact.setLabel("");
        DrawableCompat.setTint(ivAssign.getDrawable(), ContextCompat.getColor(activity, R.color.button_background));
        selectContact = " ";

        llChipView.removeView(cvProject);
        cvProject.setLabel("");
        DrawableCompat.setTint(ivProject.getDrawable(), ContextCompat.getColor(activity, R.color.button_background));
        selectProjectId = " ";

        llChipView.removeView(cvCalander);
        cvCalander.setLabel("");
        DrawableCompat.setTint(ivAddCalander.getDrawable(), ContextCompat.getColor(activity, R.color.button_background));
        //selectDate = " ";

        llChipView.removeView(cvComment);
        cvComment.setLabel("");
        DrawableCompat.setTint(ivCommant.getDrawable(), ContextCompat.getColor(activity, R.color.button_background));
        selectComment = " ";

        llChipView.removeView(cvPriority);
        cvPriority.setLabel("");
        DrawableCompat.setTint(ivPriority.getDrawable(), ContextCompat.getColor(activity, R.color.button_background));
        selectPriority = " ";
    }

    public void setChiupLable(ChipView chipView, String lable, EditTextControl etvAddTask) {
        try {
            chipView.setLabel(lable);
//            String text = etvAddTask.getText().toString().trim();
//            etvAddTask.setText(text.substring(0, text.length() - 1));
            etvAddTask.setSelection(etvAddTask.getText().toString().trim().length());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setChipView(ChipView chipView, int chipViewIcon, int chipViewBackColor, String name) {
        try {
            chipView.setLabel(name);
            chipView.setPadding(2, 2, 2, 2);
            chipView.setAvatarIcon(ContextCompat.getDrawable(activity, chipViewIcon));
            chipView.setChipBackgroundColor(ContextCompat.getColor(activity, chipViewBackColor));
            chipView.setLabelColor();
            chipView.setDeletable(true);
            chipView.setGravity(Gravity.CENTER);
            llChipView.addView(chipView);
            if (!TextUtils.isEmpty(etvAddTask.getText().toString())) {
                String text = etvAddTask.getText().toString().trim();
//                etvAddTask.setText(text.substring(0, text.length() - 1));
                etvAddTask.setText(text);
            } else {
                etvAddTask.setText("");
                etvAddTask.setCursorVisible(true);
            }
            etvAddTask.setSelection(etvAddTask.getText().toString().trim().length());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openCalanderDialog(final ImageView ivAddCalander, final FragmentManager fragManager) {
        try {
            final Dialog dialog = new Dialog(activity, R.style.AppCompatPickDateStyle);
            dialog.requestWindowFeature(1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            View rootView = LayoutInflater.from(activity).inflate(R.layout.pick_date_time_dialog, null);

            LinearLayout llPickDateTime = rootView.findViewById(R.id.llPickDateTime);
            llPickDateTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    calanderClick = true;
                    dialog.cancel();
                    SimpleDateTimePicker simpleDateTimePicker = SimpleDateTimePicker.make(
                            "Select Date and Time",
                            new Date(),
                            new DateTimePicker.OnDateTimeSetListener() {
                                @Override
                                public void DateTimeSet(Date date, int buttonPositive) {
                                    if (DialogInterface.BUTTON_POSITIVE == buttonPositive) {
                                        selectDate = mFormatter.format(date);
                                        setChipView(cvCalander, R.drawable.ic_project_white, R.color.signup_button_background, selectDate);
                                        DrawableCompat.setTint(ivAddCalander.getDrawable(), ContextCompat.getColor(activity, R.color.signup_button_background));
                                        etvAddTask.requestFocus();
                                        Constant.openKeyboard(activity);
                                    } else {
                                        etvAddTask.requestFocus();
                                        Constant.openKeyboard(activity);
                                    }
                                }
                            },
                            fragManager
                    );
                    simpleDateTimePicker.show();
                }
            });


            LinearLayout llToday = rootView.findViewById(R.id.llToday);
            llToday.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    calanderClick = true;
                    dialog.cancel();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = new Date();
                    currentDate = dateFormat.format(date);
                    selectDate = currentDate;
                    setChipView(cvCalander, R.drawable.ic_project_white, R.color.signup_button_background, currentDate);
                    etvAddTask.requestFocus();
                    Constant.openKeyboard(activity);
                    DrawableCompat.setTint(ivAddCalander.getDrawable(), ContextCompat.getColor(activity, R.color.signup_button_background));
                }
            });


            LinearLayout llTomorrow = rootView.findViewById(R.id.llTomorrow);
            llTomorrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    calanderClick = true;
                    dialog.cancel();
//                    Calendar c = Calendar.getInstance();
//                    c.add(Calendar.DATE, 1);
//                    currentDate = currentDateFormat.format(c.getTime());
//                    selectDate = currentDate;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = new Date();
                    currentDate = dateFormat.format(date);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    calendar.add(Calendar.DATE, +1);
                    Date newDate = calendar.getTime();
                    currentDate = dateFormat.format(newDate);
                    selectDate = currentDate;
                    setChipView(cvCalander, R.drawable.ic_project_white, R.color.signup_button_background, currentDate);
                    etvAddTask.requestFocus();
                    Constant.openKeyboard(activity);
                    DrawableCompat.setTint(ivAddCalander.getDrawable(), ContextCompat.getColor(activity, R.color.signup_button_background));
                }
            });

            LinearLayout llNextWeek = rootView.findViewById(R.id.llNextWeek);
            llNextWeek.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    calanderClick = true;
//                    Calendar c = Calendar.getInstance();
//                    c.add(Calendar.DATE, 6);
//                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//                    Date date = new Date();
//                    currentDate = dateFormat.format(date);
//                    selectDate = currentDate;
                    //  currentDate = currentDateFormat.format(c.getTime());
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = new Date();
                    currentDate = dateFormat.format(date);
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    calendar.add(Calendar.DATE, 6);
                    Date newDate = calendar.getTime();
                    currentDate = dateFormat.format(newDate);
                    selectDate = currentDate;
                    setChipView(cvCalander, R.drawable.ic_project_white, R.color.signup_button_background, currentDate);
                    dialog.cancel();
                    etvAddTask.requestFocus();
                    Constant.openKeyboard(activity);
                    DrawableCompat.setTint(ivAddCalander.getDrawable(), ContextCompat.getColor(activity, R.color.signup_button_background));
                }
            });

            dialog.setContentView(rootView);
            dialog.setCancelable(true);
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    if (!calanderClick) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Constant.openKeyboard(activity);
                            }
                        }, 300);
                    } else {
                        calanderClick = false;
                    }
                }
            });
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("All Project Task dialog", "Dialog Alert Exception = " + e);
        }
    }

    public String selectDate() {
        return selectDate;
    }

    public void openCommentDialog(final ImageView ivCommant) {
        try {
            final Dialog dialog = new Dialog(activity, R.style.AppCompatAlertDialogStyle);
            dialog.requestWindowFeature(1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            View rootView = LayoutInflater.from(activity).inflate(R.layout.dlg_comment, null);

            final TextInputEditText etvComment = rootView.findViewById(R.id.etvComment);
            etvComment.requestFocus();
            TextView tvYes = rootView.findViewById(R.id.tvYes);
            tvYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                    selectComment = etvComment.getText().toString().trim();
                    setChipView(cvComment, R.drawable.ic_project_white, R.color.signup_button_background, selectComment);
                    DrawableCompat.setTint(ivCommant.getDrawable(), ContextCompat.getColor(activity, R.color.signup_button_background));
                }
            });

            dialog.setContentView(rootView);
            dialog.setCancelable(false);
            dialog.show();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String selectComment() {
        return selectComment;
    }

    public void setTaskFragmnet(String name, Fragment fragment, final ImageView ivCalander, final ImageView ivAlarm) {
        ArrayList<Task> todayTaskList = new ArrayList<>();
        switch (name) {
            case Constant.TASK:
                todayTaskList = dataBaseHelper.getPersonalTask();
                break;
            case Constant.IDEA:
                todayTaskList = dataBaseHelper.getPersonalIdea();
                break;
            case Constant.INQUIRY:
                todayTaskList = dataBaseHelper.getPersonalInquiry();
                break;
        }

        if (fragment instanceof TaskFragment) {
            personalTaskFragment = (TaskFragment) fragment;

            if (personalTaskFragment.todayTaskAdapter == null) {
                personalTaskFragment.todayTaskAdapter = new TaskAdapter(activity, todayTaskList, 0, llFabLayout, llBottom, fbtnMain);
                RecyclerView.LayoutManager layoutManage = new LinearLayoutManager(activity);

                personalTaskFragment.rvTodayTask.setLayoutManager(layoutManage);

                DividerItemDecoration dividerTask = new DividerItemDecoration(personalTaskFragment.rvTodayTask.getContext(), DividerItemDecoration.VERTICAL);
                dividerTask.setDrawable(ContextCompat.getDrawable(activity, R.drawable.custom_divider));
                personalTaskFragment.rvTodayTask.addItemDecoration(dividerTask);
                personalTaskFragment.rvTodayTask.setAdapter(personalTaskFragment.todayTaskAdapter);
                personalTaskFragment.todayTaskAdapter.setOnClickListner(new TaskAdapter.OnItemClickListener() {
                    @Override
                    public void OnItemClick(int position, String taskNames,String taskId, CardView cvMain) {
                        ivCalander.setVisibility(View.VISIBLE);
                        ivAlarm.setVisibility(View.VISIBLE);
                        personalTaskFragment.taskNames = taskNames;
                        personalTaskFragment.taskId = taskId;
                    }
                });
            } else {

                refreshTab(TASK);

            }


        } else if (fragment instanceof IdeaFragment) {
            personalIdeaFragment = (IdeaFragment) fragment;

            if (personalIdeaFragment.todayTaskList == null) {
                personalTaskFragment.todayTaskAdapter = new TaskAdapter(activity, todayTaskList, 1, llFabLayout, llBottom, fbtnMain);
                RecyclerView.LayoutManager layoutManage = new LinearLayoutManager(activity);
                personalIdeaFragment.rvTodayTask.setLayoutManager(layoutManage);

                DividerItemDecoration dividerTask = new DividerItemDecoration(personalIdeaFragment.rvTodayTask.getContext(), DividerItemDecoration.VERTICAL);
                dividerTask.setDrawable(ContextCompat.getDrawable(activity, R.drawable.custom_divider));
                personalIdeaFragment.rvTodayTask.addItemDecoration(dividerTask);
                personalIdeaFragment.rvTodayTask.setAdapter(personalIdeaFragment.todayTaskAdapter);
                personalIdeaFragment.todayTaskAdapter.setOnClickListner(new TaskAdapter.OnItemClickListener() {
                    @Override
                    public void OnItemClick(int position, String taskNames,String taskId, CardView cvMain) {
                        ivCalander.setVisibility(View.VISIBLE);
                        ivAlarm.setVisibility(View.VISIBLE);
                        personalIdeaFragment.taskNames = taskNames;
                        personalIdeaFragment.taskId = taskId;
                    }
                });
            } else {
                refreshTab(Constant.IDEA);
            }

        } else if (fragment instanceof InquiryFragment) {
            personalInquiryFragment = (InquiryFragment) fragment;

            if (personalInquiryFragment.todayTaskAdapter == null) {
                personalTaskFragment.todayTaskAdapter = new TaskAdapter(activity, todayTaskList, 2, llFabLayout, llBottom, fbtnMain);
                RecyclerView.LayoutManager layoutManage = new LinearLayoutManager(activity);
                personalInquiryFragment.rvTodayTask.setLayoutManager(layoutManage);

                DividerItemDecoration dividerTask = new DividerItemDecoration(personalInquiryFragment.rvTodayTask.getContext(), DividerItemDecoration.VERTICAL);
                dividerTask.setDrawable(ContextCompat.getDrawable(activity, R.drawable.custom_divider));
                personalInquiryFragment.rvTodayTask.addItemDecoration(dividerTask);
                personalInquiryFragment.rvTodayTask.setAdapter(personalInquiryFragment.todayTaskAdapter);
                personalInquiryFragment.todayTaskAdapter.setOnClickListner(new TaskAdapter.OnItemClickListener() {
                    @Override
                    public void OnItemClick(int position, String taskNames,String taskId, CardView cvMain) {
                        ivCalander.setVisibility(View.VISIBLE);
                        ivAlarm.setVisibility(View.VISIBLE);
                        personalInquiryFragment.taskNames = taskNames;
                        personalInquiryFragment.taskId = taskId;
                    }
                });
            } else {
                refreshTab(Constant.INQUIRY);
            }
        }
    }

    public void refreshTab(String where) {
        if (whereToCome.equalsIgnoreCase(Constant.PERSONAL)) {
            if (where.equalsIgnoreCase(TASK)) {
                TaskFragment comP1 = (TaskFragment) viewPager.getAdapter().instantiateItem(viewPager, 0);
                comP1.refreshpage(Constant.PERSONAL);
            } else if (where.equalsIgnoreCase(Constant.IDEA)) {
                IdeaFragment comP1 = (IdeaFragment) viewPager.getAdapter().instantiateItem(viewPager, 1);
                comP1.refreshpage(Constant.PERSONAL);
            } else if (where.equalsIgnoreCase(Constant.INQUIRY)) {
                InquiryFragment comP1 = (InquiryFragment) viewPager.getAdapter().instantiateItem(viewPager, 2);
                comP1.refreshpage(Constant.PERSONAL);
            }
        } else if (whereToCome.equalsIgnoreCase(Constant.NEXT7DAY)) {
            if (where.equalsIgnoreCase(TASK)) {
                TaskFragment comP1 = (TaskFragment) viewPager.getAdapter().instantiateItem(viewPager, 0);
                comP1.refreshpage(Constant.NEXT7DAY);
            } else if (where.equalsIgnoreCase(Constant.INQUIRY)) {
                InquiryFragment comP1 = (InquiryFragment) viewPager.getAdapter().instantiateItem(viewPager, 1);
                comP1.refreshpage(Constant.NEXT7DAY);
            }
        } else if (whereToCome.equalsIgnoreCase(Constant.TODAY)) {
            if (where.equalsIgnoreCase(TASK)) {
                OpenCommanDialog openCommanDialog = new OpenCommanDialog(whereToCome, PersonalFragment.vpToday);
                TaskFragment comP1 = (TaskFragment) viewPager.getAdapter().instantiateItem(openCommanDialog.viewPager, 0);
                comP1.refreshpage(Constant.TODAY);
            } else if (where.equalsIgnoreCase(Constant.INQUIRY)) {
                InquiryFragment comP1 = (InquiryFragment) viewPager.getAdapter().instantiateItem(viewPager, 1);
                comP1.refreshpage(Constant.TODAY);
            }
        }
        if (whereToCome.equalsIgnoreCase(Constant.PROJECT)) {
            if (where.equalsIgnoreCase(TASK)) {
                TaskFragment comP1 = (TaskFragment) viewPager.getAdapter().instantiateItem(viewPager, 0);
                comP1.refreshpage(Constant.PROJECT);
            } else if (where.equalsIgnoreCase(Constant.IDEA)) {
                IdeaFragment comP1 = (IdeaFragment) viewPager.getAdapter().instantiateItem(viewPager, 1);
                comP1.refreshpage(Constant.PROJECT);
            } else if (where.equalsIgnoreCase(Constant.INQUIRY)) {
                InquiryFragment comP1 = (InquiryFragment) viewPager.getAdapter().instantiateItem(viewPager, 2);
                comP1.refreshpage(Constant.PROJECT);
            }
        }
    }

    public void showPriority() {
        rvPriority.setVisibility(View.VISIBLE);
    }

    public void showProject() {
        rvProjectlist.setVisibility(View.VISIBLE);
    }

    public void showAssign() {
        rvContactList.setVisibility(View.VISIBLE);
    }

    public void showTag() {
        rvTag.setVisibility(View.VISIBLE);
    }
}

*/
