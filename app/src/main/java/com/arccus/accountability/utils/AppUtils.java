package com.arccus.accountability.utils;

/**
 * Created by Admin on 09/10/2017.
 */

public class AppUtils {

//    public static String GETCONTACTURI = "http://todocute.com/todolistapi/project_all_data/project_get_number.php";
    public static String GETCONTACTURI = "http://todocute.com/test_api/todolistapi/project_all_data/project_get_number.php";

//    private static String BASE_URL = "http://todocute.com/todolistapi/";
    private static String BASE_URL = "http://laxraj.com/taskmanagementnew/apis/";
    private static String BASE_URL1 = "http://laxraj.com/taskmanagementnew/apis/";
    public static String SIGNUP_URL = BASE_URL1 + "user_register_api";
    public static String LOGIN_URL = BASE_URL1 + "user_login_api";
    public static String FORGOT_PASSWORD_URL = BASE_URL + "user_forgetpassword_api";
    public static String VERIFY_USER_URL = BASE_URL1 + "user_verify_api";

    private static String PROJECT_SERVER = "http://todocute.com/test_api/todolistapi/project/";
//    private static String PROJECT_SERVER = "http://todocute.com/todolistapi/project/";
    //	PROJECT
    public static final String ADD_PROJECT = BASE_URL + "add_project_api";
    public static final String DELETE_PROJECT = BASE_URL1	+ "delete_project_api";
    public static final String ADD_SHARED_PROJECT = PROJECT_SERVER + "share_the_project.php";
    public static final String EDIT_PROJECT = BASE_URL1 + "edit_project_api";



    public static final String EDIT_TAG = BASE_URL1 + "edit_tag_api";
    public static final String ADD_TAG = BASE_URL1 + "add_tag_api";
    public static final String DELETE_TAG = BASE_URL1 + "delete_tag_api";

    private static final String TASK_SERVER = "http://todocute.com/test_api/todolistapi/task/";
    //	TASK
    public static final String ADD_TASK = TASK_SERVER + "create_the_task.php";
    public static final String DELETE_TASK = TASK_SERVER + "delete_the_task.php";
    public static final String COMPLATE_TASK = TASK_SERVER + "edit_the_task.php";
    public static final String EDIT_TASK = TASK_SERVER + "edit_the_task.php";
    public static final String SHARE_THE_TASK = TASK_SERVER + "share_the_task.php";



    public static final String GET_COLORS = BASE_URL + "get_colors_api";








    public static final String DASHBOARD =  BASE_URL + "user/dashboard.php";

}
