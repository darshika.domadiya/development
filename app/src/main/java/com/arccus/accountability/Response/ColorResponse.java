package com.arccus.accountability.Response;

import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Expose;


public class ColorResponse {

    @SerializedName("colorID")
    @Expose
    private String colorID;
    @SerializedName("color_name")
    @Expose
    private String colorName;
    @SerializedName("color_code")
    @Expose
    private String colorCode;
    @SerializedName("createDate")
    @Expose
    private String createDate;

    public String getColorID() {
        return colorID;
    }

    public void setColorID(String colorID) {
        this.colorID = colorID;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

}

