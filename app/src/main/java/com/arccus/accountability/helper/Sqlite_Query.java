package com.arccus.accountability.helper;

/**
 * Created by Admin on 09/01/2018.
 */

public class Sqlite_Query {

    public static String SYNCED_WITH_SERVER = "1";
    public static String NOT_SYNCED_WITH_SERVER = "0";

    public static String SERVER_ID = "server_id";

    // TBAL_PROJECT

    public static String TABLE_PROJECT = "tbl_project";
    public static String KEY_PROJECT_ID = "project_id";
    public static String KEY_USER_ID = "user_id";
    public static String KEY_PROJECT_NAME = "project_name";
    public static String KEY_PROJECT_CREATE_DATE = "project_create_date";
    public static String KEY_PROJECT_CREATE_TIME = "project_create_time";
    public static String KEY_DELETED = "deleted";
    public static String KEY_MYSQL_INSERT = "mysql_insert";
    public static String KEY_MYSQL_DELETE = "mysql_delete";
    public static String KEY_COLOR_NAME = "color_name";
    public static String KEY_COLOR_POSITION = "color_position";
    public static String KEY_MYSQL_UPDATE = "mysql_update";
    public static String KEY_RELATED_PROJECT = "related_project";
    public static String KEY_RELATED_PROJECT_POSITION = "project_postion";
    public static String KEY_FLAG = "flag";


    static String CREATE_TABLE_TBL_TBL_PROJECT = "CREATE TABLE " + TABLE_PROJECT
            + " (" + " " + KEY_PROJECT_ID + " TEXT," + "	"
            + KEY_USER_ID + "	TEXT," + "	" + KEY_PROJECT_NAME + "	TEXT," + "	"
            + KEY_PROJECT_CREATE_DATE + "	TEXT," + " " + KEY_PROJECT_CREATE_TIME + "	TEXT,"
            + "	" + KEY_DELETED + "	INTEGER DEFAULT 0," + KEY_MYSQL_INSERT + " INTEGER DEFAULT 0, "
            + KEY_MYSQL_DELETE + " INTEGER DEFAULT 0, " + KEY_COLOR_NAME + " TEXT, " + KEY_RELATED_PROJECT + " TEXT , " + "  " + KEY_COLOR_POSITION + " INTEGER , "
            + KEY_RELATED_PROJECT_POSITION + " INTEGER, " + " "
            + KEY_MYSQL_UPDATE + " INTEGER DEFAULT 0, " + " "
            + KEY_FLAG + " TEXT, "
            + SERVER_ID + " TEXT )";


    //TABLE CONTACT

    static String TABLE_CONTACT = "tbl_contact";
    static String KEY_CONTACT_ID = "contact_id";
    static String KEY_CONTACT_NAME = "contact_name";
    static String KEY_CONTACT_NUMBER = "contact_number";
    static String KEY_CONTACT_EMAIL = "contact_email";

    static String CREATE_TABLE_CONTACT = "CREATE TABLE " + TABLE_CONTACT
            + " (" + " " + KEY_CONTACT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + "	"
            + KEY_CONTACT_NAME + "	TEXT," + "	" + KEY_CONTACT_NUMBER + "	TEXT," + "	"
            + KEY_CONTACT_EMAIL + "	TEXT)";


    //TABLE CONTACT COLABRATION

    static String TABLE_CONTACT_COLABRATION = "tbl_contact_cola";
    public static String KEY_CONTACT_COLA_ID = "contact_id_cola";
    public static String KEY_PROJECT_COLA_ID = "project_id_cola";
    static String KEY_CONTACT_COLA_NAME = "contact_name_cola";
    static String KEY_CONTACT_COLA_NUMBER = "contact_number_cola";
    public static String KEY_CONTACT_COLA_EMAIL = "contact_email_cola";
    public static String KEY_ADD_TASK = "add_task";
    public static String KEY_EDIT_TASK = "edit_task";
    public static String KEY_DELETE_TASK = "delete_task";
    public static String KEY_ASSIGN_TO_OTHER = "assign_to_other";

    static String CREATE_TABLE_CONTACT_COLABRATION = "CREATE TABLE " + TABLE_CONTACT_COLABRATION
            + " (" + " " + KEY_CONTACT_COLA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + "	"
            + KEY_CONTACT_COLA_NAME + "	TEXT," + "	" + KEY_CONTACT_COLA_NUMBER + "	TEXT," + "	" + KEY_PROJECT_COLA_ID + " INTEGER," + " "
            + KEY_CONTACT_COLA_EMAIL + "	TEXT," + " " + KEY_ADD_TASK + " INTEGER," + " " + KEY_EDIT_TASK + " INTEGER," + " "
            + KEY_DELETE_TASK + " INTEGER," + " " + KEY_ASSIGN_TO_OTHER + " INTEGER, "
            + KEY_DELETED + " INTEGER DEFAULT 0, "
            + KEY_MYSQL_INSERT + " INTEGER DEFAULT 0, "
            + KEY_MYSQL_UPDATE + " INTEGER DEFAULT 0, "
            + KEY_MYSQL_DELETE + " INTEGER DEFAULT 0, "
            + KEY_FLAG + " TEXT )";

    static String TABLE_TAG = "tbl_tag";
    static String KEY_TAG_ID = "tag_id";
    static String KEY_TAG_NAME = "tag_name";
    static String KEY_TAG_COLOR_POSITION = "color_position";


    static String CREATE_TABLE_TAG = "CREATE TABLE " + TABLE_TAG
            + "(" + " " + KEY_TAG_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + " "
            + KEY_TAG_NAME + " TEXT, " + " "
            + KEY_TAG_COLOR_POSITION + " INTEGER )";


    public static String TABLE_TASK = "tbl_task";
    public static String KEY_TASK_ID = "task_id";
    public static String KEY_TASK_NAME = "task_name";
    public static String KEY_TASK_PROJECT_ID = "project_id";
    static String KEY_TASK_DATE = "date";
    public static String KEY_TASK_TAG = "tag";
    public static String KEY_TASK_PRIORITY = "priority";
    public static String KEY_TASK_COLOBRATION = "colobration";
    static String KEY_TASK_COMMENT = "comment";
    static String KEY_TASK_LOCATION = "task_location";
    public static String KEY_TASK_COMPLETED = "task_Completed";
    public static String KEY_ONLY_ME = "Only_Me";

    public static String CREATE_TABLE_TASK = "CREATE TABLE " + TABLE_TASK
            + "(" + " " + KEY_TASK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + " "
            + KEY_TASK_NAME + " TEXT, " + " "
            + KEY_TASK_PROJECT_ID + " TEXT, "
            + KEY_TASK_DATE + " DATETIME, " + " "
            + KEY_TASK_TAG + " TEXT, " + " "
            + KEY_TASK_PRIORITY + " TEXT, " + " "
            + KEY_TASK_COLOBRATION + " TEXT, " + " "
            + KEY_TASK_LOCATION + " TEXT, " + " "
            + KEY_TASK_COMMENT + " TEXT, "
            + KEY_DELETED + " INTEGER DEFAULT 0,"
            + KEY_MYSQL_INSERT + " INTEGER DEFAULT 0, "
            + KEY_MYSQL_UPDATE + " INTEGER DEFAULT 0, "
            + KEY_MYSQL_DELETE + " INTEGER DEFAULT 0, "
            + KEY_FLAG + " TEXT, "
            + KEY_TASK_COMPLETED + " INTEGER DEFAULT 0, "
            + SERVER_ID + " TEXT, "
            + KEY_ONLY_ME + " INTEGER DEFAULT 0 )";

    // TABLE IDEA
    public static String TABLE_IDEA = "tbl_idea";
    public static String KEY_IDEA_ID = "idea_id";
    public static String KEY_IDEA_NAME = "idea_name";
    public static String KEY_IDEA_PROJECT_Id = "idea_project_id";
    static String KEY_IDEA_DATE = "date";
    public static String KEY_IDEA_TAG = "tag";
    public static String KEY_IDEA_PRIORITY = "priority";
    public static String KEY_IDEA_COLOBRATION = "colobration";
    static String KEY_IDEA_COMMENT = "comment";
    static String KEY_IDEA_LOCATION = "idea_location";
    public static String KEY_IDEA_COMPLETED = "idea_completed";

    public static String CREATE_TABLE_IDEA = "CREATE TABLE " + TABLE_IDEA
            + "(" + " " + KEY_IDEA_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + " "
            + KEY_IDEA_NAME + " TEXT, " + " "
            + KEY_IDEA_PROJECT_Id + " TEXT, " + " "
            + KEY_IDEA_DATE + " DATETIME, " + " "
            + KEY_IDEA_TAG + " TEXT, " + " "
            + KEY_IDEA_PRIORITY + " TEXT, " + " "
            + KEY_IDEA_COLOBRATION + " TEXT, " + " "
            + KEY_IDEA_LOCATION + " TEXT, " + " "
            + KEY_IDEA_COMMENT + " TEXT, "
            + KEY_DELETED + " INTEGER DEFAULT 0,"
            + KEY_MYSQL_INSERT + " INTEGER DEFAULT 0, "
            + KEY_MYSQL_UPDATE + " INTEGER DEFAULT 0, "
            + KEY_MYSQL_DELETE + " INTEGER DEFAULT 0, "
            + KEY_FLAG + " TEXT , "
            + KEY_IDEA_COMPLETED + " INTEGER DEFAULT 0, "
            + SERVER_ID + " TEXT , "
            + KEY_ONLY_ME + " INTEGER DEFAULT 0 )";

    // TABLE INQUIRY
    public static String TABLE_INQUIRY = "tbl_inquiry";
    public static String KEY_INQUIRY_ID = "inquiry_id";
    public static String KEY_INQUIRY_NAME = "inquiry_name";
    public static String KEY_INQUIRY_PROJECT_Id = "inquiry_project_id";
    static String KEY_INQUIRY_DATE = "date";
    public static String KEY_INQUIRY_TAG = "tag";
    public static String KEY_INQUIRY_PRIORITY = "priority";
    public static String KEY_INQUIRY_COLOBRATION = "colobration";
    static String KEY_INQUIRY_COMMENT = "comment";
    static String KEY_INQUIRY_LOCATION = "inquiry_location";
    public static String KEY_INQUIRY_COMPLETED = "inquiry_completed";

    public static String CREATE_TABLE_INQUIRY = "CREATE TABLE " + TABLE_INQUIRY
            + "(" + " " + KEY_INQUIRY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + " "
            + KEY_INQUIRY_NAME + " TEXT, " + " "
            + KEY_INQUIRY_PROJECT_Id + " TEXT, " + " "
            + KEY_INQUIRY_DATE + " DATETIME, " + " "
            + KEY_INQUIRY_TAG + " TEXT, " + " "
            + KEY_INQUIRY_PRIORITY + " TEXT, " + " "
            + KEY_INQUIRY_COLOBRATION + " TEXT, " + " "
            + KEY_INQUIRY_LOCATION + " TEXT, " + " "
            + KEY_INQUIRY_COMMENT + " TEXT, "
            + KEY_DELETED + " INTEGER DEFAULT 0,"
            + KEY_MYSQL_INSERT + " INTEGER DEFAULT 0, "
            + KEY_MYSQL_UPDATE + " INTEGER DEFAULT 0, "
            + KEY_MYSQL_DELETE + " INTEGER DEFAULT 0, "
            + KEY_FLAG + " TEXT , "
            + KEY_INQUIRY_COMPLETED + " INTEGER DEFAULT 0, "
            + SERVER_ID + " TEXT , "
            + KEY_ONLY_ME + " INTEGER DEFAULT 0 )";

    //Table Location
    static String TABLE_LOCATION = "tbl_location";
    static String KEY_LOCATION_ID = "location_id";
    static String KEY_LOCATION_NAME = "location_name";
    static String KEY_LOCATION_ADDRESS = "location_address";
    static String KEY_LATITUDE = "latitude";
    static String KEY_LONGITUDE = "longitude";

    static String CREATE_TABLE_LOCATION = "CREATE TABLE " + TABLE_LOCATION
            + "(" + " " + KEY_LOCATION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + " "
            + KEY_LOCATION_NAME + " TEXT, " + " "
            + KEY_LOCATION_ADDRESS + " TEXT, " + " "
            + KEY_LATITUDE + " TEXT, " + " "
            + KEY_LONGITUDE + " TEXT )";


    //Table CommentActivity

    public static String TABLE_COMMENT = "tbl_comment";
    public static String KEY_COMMENT_ID = "comment_id";
    public static String KEY_COMMENT_TASK_ID = "comment_task_id";
    public static String KEY_COMMENT_NAME = "comment_name";
    static String KEY_COMMENT_USER_NAME = "comment_user_name";
    public static String KEY_CURRENT_DATE_TIME = "comment_date_time";
    public static String KEY_COMMENT_TASK_TYPE = "comment_task_type";


    static String CREATE_TABLE_COMMENT = "CREATE TABLE " + TABLE_COMMENT
            + "(" + " " + KEY_COMMENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + " "
            + KEY_COMMENT_TASK_ID + " TEXT, " + " "
            + KEY_COMMENT_NAME + " TEXT, " + " "
            + KEY_COMMENT_USER_NAME + " TEXT, " + " "
            + KEY_CURRENT_DATE_TIME + " TEXT, "
            + KEY_COMMENT_TASK_TYPE + " TEXT, "
            + KEY_FLAG + " TEXT, "
            + KEY_DELETED + " INTEGER DEFAULT 0, "
            + KEY_MYSQL_INSERT + " INTEGER DEFAULT 0, "
            + KEY_MYSQL_UPDATE + " INTEGER DEFAULT 0, "
            + KEY_MYSQL_DELETE + " INTEGER DEFAULT 0, "
            + SERVER_ID + " TEXT )";

    //table general

    //table for general settings
    public static String TABLE_GENERAL = "general";
    public static String KEY_ID = "id";

    public static String KEY_TIMEZONE = "timezone";
    public static String KEY_STARTPAGE = "startpage";
    public static String KEY_START_DAY = "start_day";
    public static String KEY_DAYTIME = "start_day_time";

//    static String CREATE_TABLE_GENERAL = "CREATE TABLE " + TABLE_GENERAL
//            + "(" + " " + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + " "
//            + KEY_TIMEZONE + " TEXT, " + " "
//            + KEY_START_DAY + " TEXT, " + " "
//            + KEY_STARTPAGE + " TEXT, " + " "
//            + KEY_DAYTIME + " TEXT)";

}
