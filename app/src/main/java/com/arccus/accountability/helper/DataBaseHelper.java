package com.arccus.accountability.helper;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import com.arccus.accountability.MyApplicationClass;
import com.arccus.accountability.model.ColabrationContact;
import com.arccus.accountability.model.Comment;
import com.arccus.accountability.model.Contact;
import com.arccus.accountability.model.CreateProject;
import com.arccus.accountability.model.LocationModel;
import com.arccus.accountability.model.StartDay;
import com.arccus.accountability.model.Task;
import com.arccus.accountability.shared_preference.SessionManager;
import com.arccus.accountability.utils.Constant;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import static com.arccus.accountability.helper.Sqlite_Query.CREATE_TABLE_COMMENT;
import static com.arccus.accountability.helper.Sqlite_Query.CREATE_TABLE_CONTACT;
import static com.arccus.accountability.helper.Sqlite_Query.CREATE_TABLE_CONTACT_COLABRATION;
import static com.arccus.accountability.helper.Sqlite_Query.CREATE_TABLE_IDEA;
import static com.arccus.accountability.helper.Sqlite_Query.CREATE_TABLE_INQUIRY;
import static com.arccus.accountability.helper.Sqlite_Query.CREATE_TABLE_LOCATION;
import static com.arccus.accountability.helper.Sqlite_Query.CREATE_TABLE_TAG;
import static com.arccus.accountability.helper.Sqlite_Query.CREATE_TABLE_TASK;
import static com.arccus.accountability.helper.Sqlite_Query.CREATE_TABLE_TBL_TBL_PROJECT;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_ADD_TASK;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_ASSIGN_TO_OTHER;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_COLOR_NAME;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_COLOR_POSITION;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_COMMENT_ID;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_COMMENT_NAME;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_COMMENT_TASK_ID;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_COMMENT_TASK_TYPE;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_COMMENT_USER_NAME;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_CONTACT_COLA_EMAIL;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_CONTACT_COLA_ID;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_CONTACT_COLA_NAME;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_CONTACT_COLA_NUMBER;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_CONTACT_EMAIL;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_CONTACT_NAME;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_CONTACT_NUMBER;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_CURRENT_DATE_TIME;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_DAYTIME;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_DELETED;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_DELETE_TASK;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_EDIT_TASK;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_FLAG;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_IDEA_ID;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_IDEA_PROJECT_Id;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_INQUIRY_ID;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_INQUIRY_PROJECT_Id;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_LATITUDE;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_LOCATION_ADDRESS;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_LOCATION_ID;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_LOCATION_NAME;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_LONGITUDE;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_MYSQL_DELETE;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_MYSQL_INSERT;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_MYSQL_UPDATE;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_ONLY_ME;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_PROJECT_COLA_ID;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_PROJECT_CREATE_DATE;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_PROJECT_CREATE_TIME;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_PROJECT_ID;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_PROJECT_NAME;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_RELATED_PROJECT;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_RELATED_PROJECT_POSITION;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_STARTPAGE;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_START_DAY;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_TAG_COLOR_POSITION;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_TAG_ID;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_TAG_NAME;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_TASK_COLOBRATION;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_TASK_COMMENT;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_TASK_DATE;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_TASK_ID;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_TASK_LOCATION;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_TASK_NAME;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_TASK_PRIORITY;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_TASK_PROJECT_ID;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_TASK_TAG;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_TIMEZONE;
import static com.arccus.accountability.helper.Sqlite_Query.KEY_USER_ID;
import static com.arccus.accountability.helper.Sqlite_Query.SERVER_ID;
import static com.arccus.accountability.helper.Sqlite_Query.TABLE_COMMENT;
import static com.arccus.accountability.helper.Sqlite_Query.TABLE_CONTACT;
import static com.arccus.accountability.helper.Sqlite_Query.TABLE_CONTACT_COLABRATION;
import static com.arccus.accountability.helper.Sqlite_Query.TABLE_GENERAL;
import static com.arccus.accountability.helper.Sqlite_Query.TABLE_IDEA;
import static com.arccus.accountability.helper.Sqlite_Query.TABLE_INQUIRY;
import static com.arccus.accountability.helper.Sqlite_Query.TABLE_LOCATION;
import static com.arccus.accountability.helper.Sqlite_Query.TABLE_PROJECT;
import static com.arccus.accountability.helper.Sqlite_Query.TABLE_TAG;
import static com.arccus.accountability.helper.Sqlite_Query.TABLE_TASK;

/**
 * Created by Admin on 09/01/2018.
 */

public class DataBaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "todocute.db";
    private static final int DATABASE_VERSION = 1;
    public static DataBaseHelper instance;

    public static DataBaseHelper getInstance()
    {
        if(instance != null)
            return instance;
        else
        {
            instance = new DataBaseHelper(MyApplicationClass.getAppContext());
            return instance;
        }
    }

    public DataBaseHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }

    public long addProject(String project_id, String user_id, String projectName, String colorName, String colorPosition, int deleted, int insert, int updated, int delete, String selectProjectName, int projectPosition, String flag) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PROJECT_ID, project_id);
        values.put(KEY_USER_ID, user_id);
        values.put(KEY_PROJECT_NAME, projectName);
        values.put(KEY_PROJECT_CREATE_DATE, Constant.CURRENT_DATE);
        values.put(KEY_PROJECT_CREATE_TIME, Constant.CURRENT_TIME);
        values.put(KEY_COLOR_POSITION, colorPosition);
        values.put(KEY_COLOR_NAME,colorName);
        values.put(KEY_DELETED, deleted);
        values.put(KEY_MYSQL_INSERT, insert);
        values.put(KEY_MYSQL_DELETE, updated);
        values.put(KEY_MYSQL_UPDATE, delete);
        values.put(KEY_RELATED_PROJECT, selectProjectName);
        values.put(KEY_RELATED_PROJECT_POSITION, projectPosition);
        values.put(KEY_FLAG, flag);
        //db.close();

        Log.e("Project_added",""+"Project_added");
        return db.insert(TABLE_PROJECT, null, values);

    }

    public void setServerID(String table,String serverId,String localKey,String localId)
    {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(Sqlite_Query.SERVER_ID,serverId);
            db.update(table, values,localKey + " = " + localId,null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getServerID(String table,String localKey,String localId)
    {
        String serverId = "";
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String query = "SELECT "+ Sqlite_Query.SERVER_ID + " FROM "+ table + " WHERE " + localKey + " = " + localId;
            Cursor cursor = db.rawQuery(query,null);
            if(cursor != null && cursor.moveToNext())
            {
                serverId = cursor.getString(0);
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return serverId;
    }

    public String getLocalID(String table ,String serverId,String localIdKey)
    {
        String localId = "";
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String query = "SELECT "+ localIdKey + " FROM "+ table + " WHERE " + Sqlite_Query.SERVER_ID + " = '" + serverId + "'";
            Cursor cursor = db.rawQuery(query,null);
            if(cursor != null && cursor.moveToNext())
            {
                localId = cursor.getString(0);
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return localId;
    }

    public long addProject(CreateProject createProject) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = projectContentValueSetter(createProject);
            //db.close();
            return db.insert(TABLE_PROJECT, null, values);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return  0;
    }

    private ContentValues projectContentValueSetter(CreateProject createProject) {
        ContentValues values = new ContentValues();
        if(!TextUtils.isEmpty(createProject.getProject_id()))
            values.put(Sqlite_Query.KEY_PROJECT_ID,createProject.getProject_id());
        values.put(Sqlite_Query.KEY_USER_ID, createProject.getUser_id());
        values.put(Sqlite_Query.KEY_PROJECT_NAME, createProject.getProject_name());
        values.put(Sqlite_Query.KEY_PROJECT_CREATE_DATE, Constant.CURRENT_DATE);
        values.put(Sqlite_Query.KEY_PROJECT_CREATE_TIME, Constant.CURRENT_TIME);
        values.put(Sqlite_Query.KEY_COLOR_POSITION, createProject.getColorPosition());
        values.put(Sqlite_Query.KEY_COLOR_NAME,createProject.getColorName());
        values.put(Sqlite_Query.KEY_DELETED, createProject.getDeleted());
        values.put(Sqlite_Query.KEY_MYSQL_INSERT, 1);
        values.put(Sqlite_Query.KEY_MYSQL_DELETE, createProject.getDeleted());
        values.put(Sqlite_Query.KEY_FLAG, "I");
        values.put(Sqlite_Query.SERVER_ID,createProject.getServerId());
        values.put(Sqlite_Query.KEY_PROJECT_CREATE_DATE,createProject.getProject_create_date());
        values.put(Sqlite_Query.KEY_PROJECT_CREATE_TIME,createProject.getProject_create_time());
        return values;
    }

    public String getProjectName(String projectId)
    {
        String query = "SELECT " + Sqlite_Query.KEY_PROJECT_NAME + " FROM " + Sqlite_Query.TABLE_PROJECT + " WHERE " + Sqlite_Query.KEY_PROJECT_ID + " = "+ projectId;
        Cursor cursor = getReadableDatabase().rawQuery(query,null);
        if(cursor != null && cursor.moveToNext())
        {
            return cursor.getString(0);
        }
        return "";
    }

    public void addContact(String contactName, String contactNumber, String contactEmail) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CONTACT_NAME, contactName);
        values.put(KEY_CONTACT_NUMBER, contactNumber);
        if (contactEmail != null) {
            values.put(KEY_CONTACT_EMAIL, contactEmail);
        }
        db.insert(TABLE_CONTACT, null, values);
    }

    public void addContact(String contactName, String contactNumber) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_CONTACT_NAME, contactName);
        values.put(KEY_CONTACT_NUMBER, contactNumber);
        db.insert(TABLE_CONTACT, null, values);
    }

    public void addTag(String tagid,String tabName, String tagColorPosition) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_TAG_ID, tagid);
        values.put(KEY_TAG_NAME, tabName);
        values.put(KEY_TAG_COLOR_POSITION, tagColorPosition);
        sqLiteDatabase.insert(TABLE_TAG, null, values);

    }

    public void addContactColabration(String project_id , String contactName, String contactNumber, String contactEmail) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PROJECT_COLA_ID, project_id);
        values.put(KEY_CONTACT_COLA_NAME, contactName);
        values.put(KEY_CONTACT_COLA_NUMBER, contactNumber);
        if (contactEmail != null) {
            values.put(KEY_CONTACT_COLA_EMAIL, contactEmail);
        }
        values.put(KEY_ADD_TASK, 0);
        values.put(KEY_EDIT_TASK, 0);
        values.put(KEY_DELETE_TASK, 0);
        values.put(KEY_ASSIGN_TO_OTHER, 0);

        values.put(KEY_FLAG,"I");

        db.insert(TABLE_CONTACT_COLABRATION, null, values);
    }

    public void addContactColabration(ColabrationContact colabrationContact, int mysql_insert) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PROJECT_COLA_ID, colabrationContact.getProject_id());
        values.put(KEY_CONTACT_COLA_EMAIL, colabrationContact.getShare_email());
        values.put(Sqlite_Query.KEY_CONTACT_COLA_NAME, colabrationContact.getShare_name());
        values.put(Sqlite_Query.KEY_CONTACT_COLA_NUMBER, colabrationContact.getShare_number());
        values.put(KEY_ADD_TASK, colabrationContact.getAdd_task());
        values.put(KEY_EDIT_TASK, colabrationContact.getEdit_task());
        values.put(KEY_DELETE_TASK, colabrationContact.getDelete_task());
        values.put(KEY_ASSIGN_TO_OTHER, colabrationContact.getAssign_to_other());

        values.put(Sqlite_Query.KEY_DELETED,colabrationContact.getDeleted());
        values.put(Sqlite_Query.KEY_MYSQL_DELETE,colabrationContact.getDeleted());
        values.put(Sqlite_Query.KEY_MYSQL_INSERT,mysql_insert);
        values.put(KEY_FLAG,"I");

        db.insert(TABLE_CONTACT_COLABRATION, null, values);
    }

    public long addTask(String taskName, String taskProjectID, String taskDate, String taskTag, String taskPriority, String taskColobration, String taskComment) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_TASK_NAME, taskName);
            values.put(KEY_TASK_PROJECT_ID, taskProjectID);
            values.put(KEY_TASK_DATE, taskDate);
            values.put(KEY_TASK_TAG, taskTag);
            values.put(KEY_TASK_PRIORITY, taskPriority);
            values.put(KEY_TASK_COLOBRATION, taskColobration);
            values.put(KEY_TASK_COMMENT, taskComment);
            values.put(KEY_FLAG,"I");
            return db.insert(TABLE_TASK, null, values);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public long addTask(Task task) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = taskContentSetter(task);
            return db.insert(TABLE_TASK, null, values);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    private ContentValues taskContentSetter(Task task) {
        ContentValues values = new ContentValues();
        if(task.getTaskId() != 0)
            values.put(Sqlite_Query.KEY_TASK_ID,task.getTaskId());
        values.put(Sqlite_Query.KEY_TASK_NAME, task.getTaskName());
        values.put(Sqlite_Query.KEY_TASK_PROJECT_ID, task.getTaskProjectId());
        values.put(Sqlite_Query.KEY_FLAG,"I");
        values.put(Sqlite_Query.KEY_DELETED,task.getDeleted());
        values.put(Sqlite_Query.KEY_MYSQL_DELETE,task.getDeleted());
        values.put(Sqlite_Query.KEY_TASK_COMPLETED,task.isCompleted() ? "1" : "0");
        values.put(Sqlite_Query.KEY_MYSQL_INSERT,1);
        values.put(Sqlite_Query.SERVER_ID,task.getServerId());
        if(task.getTaskColobration() != null)
            values.put(Sqlite_Query.KEY_INQUIRY_COLOBRATION,task.getTaskColobration());
        return values;
    }

    public long addInquiry(String taskName, String taskProjectId, String taskDate, String taskTag, String taskPriority, String taskColobration, String taskComment) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(Sqlite_Query.KEY_INQUIRY_NAME, taskName);
            values.put(Sqlite_Query.KEY_INQUIRY_PROJECT_Id, taskProjectId);
            values.put(Sqlite_Query.KEY_INQUIRY_DATE, taskDate);
            values.put(Sqlite_Query.KEY_INQUIRY_TAG, taskTag);
            values.put(Sqlite_Query.KEY_INQUIRY_PRIORITY, taskPriority);
            values.put(Sqlite_Query.KEY_INQUIRY_COLOBRATION, taskColobration);
            values.put(Sqlite_Query.KEY_INQUIRY_COMMENT, taskComment);
            values.put(Sqlite_Query.KEY_FLAG,"I");
            return db.insert(Sqlite_Query.TABLE_INQUIRY, null, values);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public long addIdea(String taskName, String taskProjectId, String taskDate, String taskTag, String taskPriority, String taskColobration, String taskComment) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(Sqlite_Query.KEY_IDEA_NAME, taskName);
            values.put(Sqlite_Query.KEY_IDEA_PROJECT_Id, taskProjectId);
            values.put(Sqlite_Query.KEY_IDEA_DATE, taskDate);
            values.put(Sqlite_Query.KEY_IDEA_TAG, taskTag);
            values.put(Sqlite_Query.KEY_IDEA_PRIORITY, taskPriority);
            values.put(Sqlite_Query.KEY_IDEA_COLOBRATION, taskColobration);
            values.put(Sqlite_Query.KEY_IDEA_COMMENT, taskComment);
            values.put(Sqlite_Query.KEY_FLAG,"I");
            return db.insert(TABLE_IDEA, null, values);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void addLocation(String locationName, String locationAddress, String late, String longi) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_LOCATION_NAME, locationName);
            values.put(KEY_LOCATION_ADDRESS, locationAddress);
            values.put(KEY_LATITUDE, late);
            values.put(KEY_LONGITUDE, longi);
            db.insert(TABLE_LOCATION, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long addComment(String taskId,String taskType, String userName, String comment, String currentDateTime) {
        try {
            SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_COMMENT_TASK_ID, taskId);
            values.put(KEY_COMMENT_TASK_TYPE, taskType);
            values.put(KEY_COMMENT_USER_NAME, userName);
            values.put(KEY_COMMENT_NAME, comment);
            values.put(KEY_CURRENT_DATE_TIME, currentDateTime);
            values.put(KEY_FLAG,"I");
            return sqLiteDatabase.insert(TABLE_COMMENT, null, values);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public ArrayList<Comment> getCommnetList(String taskId, String taskType) {

        ArrayList<Comment> commentArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            @SuppressLint("Recycle") Cursor cursor = db.rawQuery("select * from tbl_comment where "+KEY_COMMENT_TASK_ID+" ='" + taskId + "' AND "+KEY_COMMENT_TASK_TYPE+ " = '"+ taskType+"' AND "+KEY_DELETED+" = 0 ", null);
            while (cursor.moveToNext()) {
                Comment comment = new Comment();
                comment.setId(cursor.getInt(cursor.getColumnIndex(KEY_COMMENT_ID)));
                comment.setTaskId(cursor.getString(cursor.getColumnIndex(KEY_COMMENT_TASK_ID)));
                comment.setTaskType(cursor.getString(cursor.getColumnIndex(KEY_COMMENT_TASK_TYPE)));
                comment.setComment(cursor.getString(cursor.getColumnIndex(KEY_COMMENT_NAME)));
                comment.setUserName(cursor.getString(cursor.getColumnIndex(KEY_COMMENT_USER_NAME)));
                comment.setCurrentDateTime(cursor.getString(cursor.getColumnIndex(KEY_CURRENT_DATE_TIME)));
                commentArrayList.add(comment);
            }
            Collections.reverse(commentArrayList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return commentArrayList;
    }

    public void addIdeaFromServer(Task task) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = ideaContentSetter(task);
            db.insert(TABLE_IDEA, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ContentValues ideaContentSetter(Task task) {
        ContentValues values = new ContentValues();
        if(task.getTaskId() != 0)
            values.put(Sqlite_Query.KEY_IDEA_ID,task.getTaskId());
        values.put(Sqlite_Query.KEY_IDEA_NAME, task.getTaskName());
        values.put(Sqlite_Query.KEY_IDEA_PROJECT_Id, task.getTaskProjectId());
        values.put(Sqlite_Query.KEY_FLAG,"I");
        values.put(Sqlite_Query.KEY_DELETED,task.getDeleted());
        values.put(Sqlite_Query.KEY_MYSQL_DELETE,task.getDeleted());
        values.put(Sqlite_Query.KEY_IDEA_COMPLETED,task.isCompleted() ? "1" : "0");
        values.put(Sqlite_Query.KEY_MYSQL_INSERT,1);
        values.put(Sqlite_Query.SERVER_ID,task.getServerId());
        if(task.getTaskColobration() != null)
            values.put(Sqlite_Query.KEY_IDEA_COLOBRATION,task.getTaskColobration());
        return values;
    }

    public void addInquiryFromServer(Task task) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = inquiryContentSetter(task);
            db.insert(TABLE_INQUIRY, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ContentValues inquiryContentSetter(Task task) {
        ContentValues values = new ContentValues();
        if(task.getTaskId() != 0)
            values.put(Sqlite_Query.KEY_INQUIRY_ID,task.getTaskId());
        values.put(Sqlite_Query.KEY_INQUIRY_NAME, task.getTaskName());
        values.put(Sqlite_Query.KEY_INQUIRY_PROJECT_Id, task.getTaskProjectId());
        values.put(Sqlite_Query.KEY_FLAG,"I");
        values.put(Sqlite_Query.KEY_DELETED,task.getDeleted());
        values.put(Sqlite_Query.KEY_MYSQL_DELETE,task.getDeleted());
        values.put(Sqlite_Query.KEY_INQUIRY_COMPLETED,task.isCompleted() ? "1" : "0");
        values.put(Sqlite_Query.KEY_MYSQL_INSERT,1);
        values.put(Sqlite_Query.SERVER_ID,task.getServerId());
        if(task.getTaskColobration() != null)
            values.put(Sqlite_Query.KEY_INQUIRY_COLOBRATION,task.getTaskColobration());
        return values;
    }

    public void addCommentFromServer(Comment comment) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = commentContentSetter(comment);
            db.insert(TABLE_COMMENT, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ContentValues commentContentSetter(Comment comment) {
        ContentValues values = new ContentValues();
        if(comment.getId() != 0)
            values.put(Sqlite_Query.KEY_COMMENT_ID,comment.getId());
        values.put(Sqlite_Query.KEY_COMMENT_NAME, comment.getComment());
        values.put(Sqlite_Query.KEY_COMMENT_TASK_ID, comment.getTaskId());
        values.put(Sqlite_Query.KEY_COMMENT_TASK_TYPE,comment.getTaskType());
        values.put(Sqlite_Query.KEY_DELETED,comment.getDeleted());
        values.put(Sqlite_Query.KEY_MYSQL_DELETE,comment.getDeleted());
        values.put(Sqlite_Query.KEY_MYSQL_INSERT,1);
        values.put(Sqlite_Query.KEY_CURRENT_DATE_TIME,comment.getDate()+" "+comment.getTime());
        values.put(Sqlite_Query.KEY_FLAG,"I");
        values.put(Sqlite_Query.KEY_COMMENT_USER_NAME, SessionManager.getInstance().getUserName());
        values.put(Sqlite_Query.SERVER_ID,comment.getServerId());
        return values;
    }

    public void editComment(String commentName, String commentId) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_COMMENT_NAME, commentName);
        values.put(KEY_FLAG,"U");
        sqLiteDatabase.update(TABLE_COMMENT, values, KEY_COMMENT_ID + " = ?", new String[]{commentId});
    }

    public void deleteComment(String commentId) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete(TABLE_COMMENT, Sqlite_Query.SERVER_ID+" = '"+commentId+"' AND ("+KEY_MYSQL_INSERT+" = 0 OR "+KEY_MYSQL_DELETE+" = 1)", null);
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_DELETED,"1");
            db.update(TABLE_COMMENT,contentValues, Sqlite_Query.SERVER_ID+" = ? ", new String[]{commentId});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateLocation(String locationName, String locationAddress, String late, String longi, String oldLocationName) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_LOCATION_NAME, locationName);
        values.put(KEY_LOCATION_ADDRESS, locationAddress);
        values.put(KEY_LATITUDE, late);
        values.put(KEY_LONGITUDE, longi);
        db.update(TABLE_LOCATION, values, KEY_LOCATION_NAME + " = ?", new String[]{oldLocationName});
    }

    public void updateTaskLocation(String taskName, String locationName) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_TASK_LOCATION, locationName);
            db.update(TABLE_TASK, values, KEY_TASK_NAME + " = ?", new String[]{taskName});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deletLocation(String locationName) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete(TABLE_LOCATION, "location_name = ?", new String[]{locationName});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<LocationModel> getLocationList() {
        ArrayList<LocationModel> locationModels = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from tbl_location", null);
            while (cursor.moveToNext()) {
                LocationModel locationModel = new LocationModel();
                locationModel.setId(cursor.getInt(cursor.getColumnIndex(KEY_LOCATION_ID)));
                locationModel.setLocationName(cursor.getString(cursor.getColumnIndexOrThrow(KEY_LOCATION_NAME)));
                locationModel.setLocationAddress(cursor.getString(cursor.getColumnIndex(KEY_LOCATION_ADDRESS)));
                locationModel.setLate(cursor.getString(cursor.getColumnIndexOrThrow(KEY_LATITUDE)));
                locationModel.setLongi(cursor.getString(cursor.getColumnIndexOrThrow(KEY_LONGITUDE)));
                locationModels.add(locationModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return locationModels;
    }

    public void updateTask(String taskID, String taskName, String taskProjectId, String taskDate, String taskTag, String taskPriority, String taskColobration, String taskComment,int onlyMe) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TASK_NAME, taskName);
        values.put(KEY_TASK_PROJECT_ID, taskProjectId);
        values.put(KEY_TASK_DATE, taskDate);
        values.put(KEY_TASK_TAG, taskTag);
        values.put(KEY_TASK_PRIORITY, taskPriority);
        values.put(KEY_TASK_COLOBRATION, taskColobration);
        values.put(KEY_TASK_COMMENT, taskComment);
        values.put(KEY_FLAG,"U");
        values.put(KEY_ONLY_ME,onlyMe);
        db.update(TABLE_TASK, values, KEY_TASK_ID + " = ?", new String[]{taskID});
    }

    public void updateTask(Task task) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TASK_NAME, task.getTaskName());
        values.put(KEY_TASK_PROJECT_ID, task.getTaskProjectId());
        values.put(KEY_TASK_COLOBRATION, task.getTaskColobration());
        values.put(KEY_FLAG,"U");
        values.put(KEY_ONLY_ME,task.getOnlyMe());
        values.put(Sqlite_Query.KEY_MYSQL_UPDATE,1);
        values.put(Sqlite_Query.KEY_MYSQL_INSERT,1);

        db.update(TABLE_TASK, values, Sqlite_Query.SERVER_ID + " = ?", new String[]{task.getServerId()});
    }

    public void updateIdea(Task task) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Sqlite_Query.KEY_IDEA_NAME, task.getTaskName());
        values.put(Sqlite_Query.KEY_IDEA_PROJECT_Id, task.getTaskProjectId());
        values.put(Sqlite_Query.KEY_IDEA_COLOBRATION, task.getTaskColobration());
        values.put(Sqlite_Query.KEY_FLAG,"U");
        values.put(Sqlite_Query.KEY_ONLY_ME,task.getOnlyMe());
        values.put(Sqlite_Query.KEY_MYSQL_UPDATE,1);
        values.put(Sqlite_Query.KEY_MYSQL_INSERT,1);

        db.update(TABLE_IDEA, values, Sqlite_Query.SERVER_ID + " = ?", new String[]{task.getServerId()});
    }

    public void updateInquiry(Task task) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Sqlite_Query.KEY_INQUIRY_NAME, task.getTaskName());
        values.put(Sqlite_Query.KEY_INQUIRY_PROJECT_Id, task.getTaskProjectId());
        values.put(Sqlite_Query.KEY_INQUIRY_COLOBRATION, task.getTaskColobration());
        values.put(Sqlite_Query.KEY_FLAG,"U");
        values.put(Sqlite_Query.KEY_ONLY_ME,task.getOnlyMe());
        values.put(Sqlite_Query.KEY_MYSQL_UPDATE,1);
        values.put(Sqlite_Query.KEY_MYSQL_INSERT,1);

        db.update(TABLE_INQUIRY, values, Sqlite_Query.SERVER_ID + " = ?", new String[]{task.getServerId()});
    }

    public void updateIdea(String taskID, String taskName, String taskProjectId, String taskDate, String taskTag, String taskPriority, String taskColobration, String taskComment,int only_me) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Sqlite_Query.KEY_IDEA_NAME, taskName);
        values.put(Sqlite_Query.KEY_IDEA_PROJECT_Id, taskProjectId);
        values.put(Sqlite_Query.KEY_IDEA_DATE, taskDate);
        values.put(Sqlite_Query.KEY_IDEA_TAG, taskTag);
        values.put(Sqlite_Query.KEY_IDEA_PRIORITY, taskPriority);
        values.put(Sqlite_Query.KEY_IDEA_COLOBRATION, taskColobration);
        values.put(Sqlite_Query.KEY_IDEA_COMMENT, taskComment);
        values.put(Sqlite_Query.KEY_FLAG,"U");
        values.put(Sqlite_Query.KEY_ONLY_ME,only_me);

        db.update(Sqlite_Query.TABLE_IDEA, values, Sqlite_Query.KEY_IDEA_ID + " = ?", new String[]{taskID});
    }

    public void updateInquiry(String taskID, String taskName, String taskProjectId, String taskDate, String taskTag, String taskPriority, String taskColobration, String taskComment, int only_me) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Sqlite_Query.KEY_INQUIRY_NAME, taskName);
        values.put(Sqlite_Query.KEY_INQUIRY_PROJECT_Id, taskProjectId);
        values.put(Sqlite_Query.KEY_INQUIRY_DATE, taskDate);
        values.put(Sqlite_Query.KEY_INQUIRY_TAG, taskTag);
        values.put(Sqlite_Query.KEY_INQUIRY_PRIORITY, taskPriority);
        values.put(Sqlite_Query.KEY_INQUIRY_COLOBRATION, taskColobration);
        values.put(Sqlite_Query.KEY_INQUIRY_COMMENT, taskComment);
        values.put(Sqlite_Query.KEY_FLAG,"U");
        values.put(Sqlite_Query.KEY_ONLY_ME,only_me);

        db.update(Sqlite_Query.TABLE_INQUIRY, values, Sqlite_Query.KEY_INQUIRY_ID + " = ?", new String[]{taskID});
    }

    public Task getTaskDetail(String taskId) {
        Task task = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from tbl_task where " + KEY_TASK_ID + " = " + taskId , null);
            while (cursor.moveToNext()) {
                task = getTaskFromCursor(cursor);
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return task;
    }

    public Task getIdeaDetail(String taskId) {
        Task task = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from "+ Sqlite_Query.TABLE_IDEA+" where " + Sqlite_Query.KEY_IDEA_ID + " = " + taskId , null);
            if(cursor != null) {
                while (cursor.moveToNext()) {
                    task = new Task();
                    task.setTaskId(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_IDEA_ID)));
                    task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_NAME)));
                    task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_PROJECT_Id)));
                    task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_DATE)));
                    task.setLocation(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_IDEA_LOCATION)));
                    task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_TAG)));
                    task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_PRIORITY)));
                    task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_COLOBRATION)));
                    task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_COMMENT)));
                    task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_IDEA_COMPLETED)) == 1);
                task.setType(Constant.IDEA);
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return task;
    }

    public Task getInquiryDetail(String taskId) {
        Task task = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from "+ Sqlite_Query.TABLE_INQUIRY+" where " + Sqlite_Query.KEY_INQUIRY_ID + " = " + taskId , null);
            if(cursor != null) {
                while (cursor.moveToNext()) {
                    task = new Task();
                    task.setTaskId(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_ID)));
                    task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_NAME)));
                    task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PROJECT_Id)));
                    task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_DATE)));
                    task.setLocation(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_LOCATION)));
                    task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_TAG)));
                    task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PRIORITY)));
                    task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COLOBRATION)));
                    task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COMMENT)));
                    task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_COMPLETED)) == 1);
                    task.setType(Constant.INQUIRY);
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return task;
    }

    public StartDay getgeneralList() {
        StartDay generaldata = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from general", null);
            while (cursor.moveToNext()) {
                StartDay data = new StartDay();
                data.setDayName(cursor.getString(cursor.getColumnIndex(KEY_START_DAY)));
                data.setTimezone(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TIMEZONE)));
                data.setStartPage(cursor.getString(cursor.getColumnIndexOrThrow(KEY_STARTPAGE)));
                data.setStartdaytime(cursor.getString(cursor.getColumnIndexOrThrow(KEY_DAYTIME)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return generaldata;
    }


/*
    public ArrayList<Task> getProjectWiseTask(String projectName, String categoryName) {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from tbl_task where " + KEY_TASK_PROJECT_ID + "='" + projectName + "'" + " AND " + KEY_CATEGORY_NAME + "='" + categoryName + "'", null);
            Log.e( "getProjectWiseTask: ", cursor.toString() );
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(KEY_TASK_ID)));
                task.setTaskId(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_NAME)));
                task.setCategoryName(cursor.getString(cursor.getColumnIndex(KEY_CATEGORY_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_PROJECT_ID)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_DATE)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_TAG)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(KEY_TASK_LOCATION)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_COMMENT)));
                taskArrayList.add(task);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }
*/

    public ArrayList<Task> getProjectWiseTask(String projectId) {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            //String query = "select * from tbl_task where " + KEY_TASK_DATE + " < '" + currentdate+"'";
//        String query = "SELECT  * FROM " + TABLE_TASK + " WHERE category_name='" + categoryName + "' AND date(" + KEY_TASK_DATE + ") <= date('" + next7day + "') AND date(" + KEY_TASK_DATE + ") >= date('" + tomorrowDate + "')";
            String query = "select  * from tbl_task where "+ Sqlite_Query.KEY_TASK_PROJECT_ID+"='" + projectId + "' AND "+KEY_DELETED+" = 0";

            Log.e("Overdesk Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = getTaskFromCursor(cursor);
                taskArrayList.add(task);
                Log.e("date", "" + task.getTaskDate());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> getProjectWiseIdea(String projectName) {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String query = "select * from "+TABLE_IDEA+" where "+ KEY_IDEA_PROJECT_Id +"='" + projectName + "' AND "+KEY_DELETED+" = 0";

            Log.e("Overdesk Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_IDEA_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_PROJECT_Id)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_DATE)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_IDEA_LOCATION)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_TAG)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_COMMENT)));
                task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_IDEA_COMPLETED)) == 1);
                task.setType(Constant.IDEA);
                taskArrayList.add(task);
                Log.e("date", "" + task.getTaskDate());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> getProjectWiseInquiry(String projectId) {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String query = "select * from "+TABLE_INQUIRY+" where "+ KEY_INQUIRY_PROJECT_Id +"='" + projectId + "' AND "+KEY_DELETED+" = 0";

            Log.e("Overdesk Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PROJECT_Id)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_DATE)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_LOCATION)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_TAG)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COMMENT)));
                task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_COMPLETED)) == 1);
                task.setType(Constant.INQUIRY);
                taskArrayList.add(task);
                Log.e("date", "" + task.getTaskDate());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> getTodayTask(String todayDate) {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            //Cursor cursor = db.rawQuery("select * from tbl_task where " + KEY_TASK_DATE + " LIKE '" + date + "%" + "'" + " AND " + KEY_CATEGORY_NAME + "= '" + categoryName + "'", null);

            //Cursor cursor = db.rawQuery("select * from tbl_task where " + KEY_CATEGORY_NAME + "= '" + categoryName + "'" + " AND " + KEY_TASK_DATE + "= '" + todayDate + "'", null);
            Cursor cursor = db.rawQuery("select * from tbl_task where date = '" + todayDate + "' AND "+KEY_DELETED+" = 0", null);
            while (cursor.moveToNext()) {
                Task task = getTaskFromCursor(cursor);
                taskArrayList.add(task);
                Log.e("today get data ", "select * from tbl_task where " + KEY_TASK_DATE + "= '" + todayDate + "'", null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return taskArrayList;
    }

    public ArrayList<Task> getTodayIdea(String todayDate) {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from "+ Sqlite_Query.TABLE_IDEA+" where date = '" + todayDate + "' AND "+KEY_DELETED+" = 0", null);
            if(cursor != null) {
                while (cursor.moveToNext()) {
                    Task task = new Task();
                    task.setTaskId(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_IDEA_ID)));
                    task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_NAME)));
                    task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_PROJECT_Id)));
                    task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_DATE)));
                    task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_TAG)));
                    task.setLocation(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_IDEA_LOCATION)));
                    task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_PRIORITY)));
                    task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_COLOBRATION)));
                    task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_COMMENT)));
                    task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_IDEA_COMPLETED)) == 1);
                task.setType(Constant.IDEA);
                    taskArrayList.add(task);
                    Log.e("today get data ", "select * from tbl_task where " + Sqlite_Query.KEY_IDEA_DATE + "= '" + todayDate + "'", null);
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return taskArrayList;
    }

    public ArrayList<Task> getTodayInquiry(String todayDate) {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            Log.e("getTodayTask: ", todayDate);
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from "+ Sqlite_Query.TABLE_INQUIRY+" where date = '" + todayDate + "' AND "+KEY_DELETED+" = 0", null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PROJECT_Id)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_DATE)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_TAG)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_LOCATION)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COMMENT)));
                task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_COMPLETED)) == 1);
                task.setType(Constant.INQUIRY);
                taskArrayList.add(task);
                Log.e("today get data ", "select * from "+TABLE_INQUIRY+" where " + Sqlite_Query.KEY_INQUIRY_DATE + "= '" + todayDate + "'", null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return taskArrayList;
    }

    public ArrayList<Task> Next7DayTask() {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Calendar c = Calendar.getInstance();
            SimpleDateFormat currentDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            c.add(Calendar.DATE, 1);
            String tomorrowDate = currentDateFormat.format(c.getTime());
            c.add(Calendar.DATE, 6);
            String next7day = currentDateFormat.format(c.getTime());
            //String query = "select * from tbl_task where " + KEY_TASK_DATE + " < '" + currentdate+"'";
            String query = "SELECT  * FROM " + TABLE_TASK + " WHERE  date(" + KEY_TASK_DATE + ") <= date('" + next7day + "') AND date(" + KEY_TASK_DATE + ") >= date('" + tomorrowDate + "') AND "+KEY_DELETED+" = 0";

            Log.e("Overdesk Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = getTaskFromCursor(cursor);
                taskArrayList.add(task);
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> Next7DayIdea() {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Calendar c = Calendar.getInstance();
            SimpleDateFormat currentDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            c.add(Calendar.DATE, 1);
            String tomorrowDate = currentDateFormat.format(c.getTime());
            c.add(Calendar.DATE, 6);
            String next7day = currentDateFormat.format(c.getTime());

            String query = "SELECT  * FROM " + Sqlite_Query.TABLE_IDEA + " WHERE  date(" + Sqlite_Query.KEY_IDEA_DATE + ") <= date('" + next7day + "') AND date(" + Sqlite_Query.KEY_IDEA_DATE + ") >= date('" + tomorrowDate + "') AND "+ Sqlite_Query.KEY_DELETED+" = 0";

            Log.e("Overdesk Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_IDEA_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_PROJECT_Id)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_DATE)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_IDEA_LOCATION)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_TAG)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_COMMENT)));
                task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_IDEA_COMPLETED)) == 1);
                task.setType(Constant.IDEA);
                taskArrayList.add(task);
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> Next7DayInquiry() {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Calendar c = Calendar.getInstance();
            SimpleDateFormat currentDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            c.add(Calendar.DATE, 1);
            String tomorrowDate = currentDateFormat.format(c.getTime());
            c.add(Calendar.DATE, 6);
            String next7day = currentDateFormat.format(c.getTime());
            //String query = "select * from tbl_task where " + KEY_TASK_DATE + " < '" + currentdate+"'";
            String query = "SELECT  * FROM " + Sqlite_Query.TABLE_INQUIRY + " WHERE  date(" + Sqlite_Query.KEY_INQUIRY_DATE + ") <= date('" + next7day + "') AND date(" + Sqlite_Query.KEY_INQUIRY_DATE + ") >= date('" + tomorrowDate + "') AND "+ Sqlite_Query.KEY_DELETED+" = 0";

            Log.e("Overdesk Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PROJECT_Id)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_DATE)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_LOCATION)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_TAG)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COMMENT)));
                task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_COMPLETED)) == 1);
                task.setType(Constant.INQUIRY);
                taskArrayList.add(task);
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> getOverdueTask(String categoryName) {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 0);
            SimpleDateFormat currentDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String currentdate = currentDateFormat.format(c.getTime());

            //String query = "select * from tbl_task where " + KEY_TASK_DATE + " < '" + currentdate+"'";
            String query = "select * from tbl_task where category_name='" + categoryName + "' and date < '" + currentdate + "'";

            Log.e("Overdesk Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = getTaskFromCursor(cursor);
                taskArrayList.add(task);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    private Task getTaskFromCursor(Cursor cursor) {
        Task task = new Task();
        task.setTaskId(cursor.getInt(cursor.getColumnIndex(KEY_TASK_ID)));
        task.setTaskName(cursor.getString(cursor.getColumnIndex(KEY_TASK_NAME)));
        task.setTaskProjectId(cursor.getString(cursor.getColumnIndex(KEY_TASK_PROJECT_ID)));
        task.setTaskDate(cursor.getString(cursor.getColumnIndex(KEY_TASK_DATE)));
        task.setLocation(cursor.getString(cursor.getColumnIndex(KEY_TASK_LOCATION)));
        task.setTaskTag(cursor.getString(cursor.getColumnIndex(KEY_TASK_TAG)));
        task.setTaskPrority(cursor.getString(cursor.getColumnIndex(KEY_TASK_PRIORITY)));
        task.setTaskColobration(cursor.getString(cursor.getColumnIndex(KEY_TASK_COLOBRATION)));
        task.setTaskComment(cursor.getString(cursor.getColumnIndex(KEY_TASK_COMMENT)));
        task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_TASK_COMPLETED)) == 1);
        task.setServerId(cursor.getString(cursor.getColumnIndex(Sqlite_Query.SERVER_ID)));
        task.setOnlyMe(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_ONLY_ME)));
        task.setType(Constant.TASK);
        return task;
    }

    public ArrayList<Task> getOverdueTask1() {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 0);
            SimpleDateFormat currentDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String currentdate = currentDateFormat.format(c.getTime());

            //String query = "select * from tbl_task where " + KEY_TASK_DATE + " < '" + currentdate+"'";
            String query = "select * from tbl_task where date < '" + currentdate + "' AND "+KEY_DELETED+" = 0";

            Log.e("Overdesk Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(KEY_TASK_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_PROJECT_ID)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_DATE)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(KEY_TASK_LOCATION)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_TAG)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_COMMENT)));
                task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_TASK_COMPLETED)) == 1);
                task.setType(Constant.TASK);
                taskArrayList.add(task);
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> getOverdueIdea() {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 0);
            SimpleDateFormat currentDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String currentdate = currentDateFormat.format(c.getTime());

            //String query = "select * from tbl_task where " + KEY_TASK_DATE + " < '" + currentdate+"'";
            String query = "select * from "+ Sqlite_Query.TABLE_IDEA+" where date < '" + currentdate + "' AND "+ Sqlite_Query.KEY_DELETED+" = 0";

            Log.e("Overdesk Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_IDEA_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_PROJECT_Id)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_DATE)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_IDEA_LOCATION)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_TAG)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_COMMENT)));
                task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_IDEA_COMPLETED)) == 1);
                task.setType(Constant.IDEA);
                taskArrayList.add(task);
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> getOverdueInquiry() {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 0);
            SimpleDateFormat currentDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String currentdate = currentDateFormat.format(c.getTime());

            //String query = "select * from tbl_task where " + KEY_TASK_DATE + " < '" + currentdate+"'";
            String query = "select * from "+ Sqlite_Query.TABLE_INQUIRY+" where date < '" + currentdate + "' AND "+KEY_DELETED+" = 0";

            Log.e("Overdesk Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PROJECT_Id)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_DATE)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_LOCATION)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_TAG)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COMMENT)));
                task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_COMPLETED)) == 1);
                task.setType(Constant.INQUIRY);
                taskArrayList.add(task);
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> getPersonalTask() {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            //String query = "select * from tbl_task where " + KEY_TASK_DATE + " < '" + currentdate+"'";
            String query = "select * from tbl_task where ("+KEY_TASK_PROJECT_ID+" = '0' or "+KEY_TASK_PROJECT_ID+" = ' ') AND "+KEY_DELETED+" = 0";

            Log.e("Personal Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(KEY_TASK_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_PROJECT_ID)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_DATE)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(KEY_TASK_LOCATION)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_TAG)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_COMMENT)));
                task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_TASK_COMPLETED)) == 1);
                task.setType(Constant.TASK);
                task.setOnlyMe(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_ONLY_ME)));
                taskArrayList.add(task);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> getPersonalIdea() {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            String query = "select * from "+ Sqlite_Query.TABLE_IDEA+" where "+ KEY_IDEA_PROJECT_Id +"='0' AND "+ Sqlite_Query.KEY_DELETED+" = 0";

            Log.e("Personal Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_IDEA_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_PROJECT_Id)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_DATE)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_IDEA_LOCATION)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_TAG)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_IDEA_COMMENT)));
                task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_IDEA_COMPLETED)) == 1);
                task.setType(Constant.IDEA);
                taskArrayList.add(task);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> getPersonalInquiry() {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            //String query = "select * from tbl_task where " + KEY_TASK_DATE + " < '" + currentdate+"'";
            String query = "select * from "+ Sqlite_Query.TABLE_INQUIRY+" where "+ KEY_INQUIRY_PROJECT_Id +"='0' AND "+KEY_DELETED+" = 0";

            Log.e("Personal Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PROJECT_Id)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_DATE)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_LOCATION)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_TAG)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COMMENT)));
                task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_COMPLETED)) == 1);
                task.setType(Constant.INQUIRY);
                taskArrayList.add(task);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> getAllocateMe() {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 0);
            SimpleDateFormat currentDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String currentdate = currentDateFormat.format(c.getTime());

            //String query = "select * from tbl_task where " + KEY_TASK_DATE + " < '" + currentdate+"'";
//            **String query = "SELECT * FROM tbl_task INNER JOIN tbl_project ON tbl_task.project_id=tbl_project.project_id AND tbl_project.user_id='"+SessionManager.getInstance().getUserId()+"'";
            String query = "SELECT * FROM tbl_task WHERE "+ Sqlite_Query.KEY_TASK_COLOBRATION+" ='"+ SessionManager.getInstance().getUserId()+"' OR "+ Sqlite_Query.KEY_ONLY_ME+" =1";

            Log.e("Personal Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(KEY_TASK_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_PROJECT_ID)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_DATE)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(KEY_TASK_LOCATION)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_TAG)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_COMMENT)));
                task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_TASK_COMPLETED)) == 1);
                task.setType(Constant.TASK);
                task.setOnlyMe(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_ONLY_ME)));
                taskArrayList.add(task);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> getAllocateOthers() {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            String query = "SELECT * FROM tbl_task WHERE " + Sqlite_Query.KEY_TASK_COLOBRATION + " !='" + SessionManager.getInstance().getUserId() +"' " +
                    "AND "+ Sqlite_Query.KEY_TASK_COLOBRATION + " != ''";

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(KEY_TASK_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_PROJECT_ID)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_DATE)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(KEY_TASK_LOCATION)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_TAG)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_COMMENT)));
                task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_TASK_COMPLETED)) == 1);
                task.setType(Constant.TASK);
                task.setOnlyMe(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_ONLY_ME)));
                taskArrayList.add(task);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<ColabrationContact> getContactColabration(String projectId) {
        ArrayList<ColabrationContact> colabrationContacts = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from " + TABLE_CONTACT_COLABRATION + " where " + Sqlite_Query.KEY_PROJECT_COLA_ID + " = '" + projectId + "' AND "+KEY_DELETED +" = 0 ", null);
            if(cursor != null) {
                while (cursor.moveToNext()) {
                    ColabrationContact colabrationContact = new ColabrationContact();
                    colabrationContact.setId(cursor.getInt(cursor.getColumnIndex(KEY_CONTACT_COLA_ID)));
                    colabrationContact.setProject_id(cursor.getInt(cursor.getColumnIndex(KEY_PROJECT_COLA_ID)));
                    colabrationContact.setShare_name(cursor.getString(cursor.getColumnIndex(KEY_CONTACT_COLA_NAME)));
                    colabrationContact.setShare_number(cursor.getString(cursor.getColumnIndex(KEY_CONTACT_COLA_NUMBER)));
                    colabrationContact.setShare_email(cursor.getString(cursor.getColumnIndex(KEY_CONTACT_COLA_EMAIL)));
                    colabrationContact.setAdd_task(cursor.getInt(cursor.getColumnIndex(KEY_ADD_TASK)));
                    colabrationContact.setEdit_task(cursor.getInt(cursor.getColumnIndex(KEY_EDIT_TASK)));
                    colabrationContact.setDelete_task(cursor.getInt(cursor.getColumnIndex(KEY_DELETE_TASK)));
                    colabrationContact.setAssign_to_other(cursor.getInt(cursor.getColumnIndex(KEY_ASSIGN_TO_OTHER)));

                    colabrationContacts.add(colabrationContact);
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("CONTACT SIZE = ",""+colabrationContacts.size());
        return colabrationContacts;
    }

    public ColabrationContact getContactColabration(String projectId, String emailID) {
        ColabrationContact colabrationContact = new ColabrationContact();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from " + TABLE_CONTACT_COLABRATION + " where " + Sqlite_Query.KEY_PROJECT_COLA_ID + " = " + projectId + " AND "+ Sqlite_Query.KEY_CONTACT_COLA_EMAIL +" = '"+emailID+"'  AND "+KEY_DELETED +" = 0 ", null);
            if(cursor != null) {
                while (cursor.moveToNext()) {
                    colabrationContact.setId(cursor.getInt(cursor.getColumnIndex(KEY_CONTACT_COLA_ID)));
                    colabrationContact.setProject_id(cursor.getInt(cursor.getColumnIndex(KEY_PROJECT_COLA_ID)));
                    colabrationContact.setShare_name(cursor.getString(cursor.getColumnIndex(KEY_CONTACT_COLA_NAME)));
                    colabrationContact.setShare_number(cursor.getString(cursor.getColumnIndex(KEY_CONTACT_COLA_NUMBER)));
                    colabrationContact.setShare_email(cursor.getString(cursor.getColumnIndex(KEY_CONTACT_COLA_EMAIL)));
                    colabrationContact.setAdd_task(cursor.getInt(cursor.getColumnIndex(KEY_ADD_TASK)));
                    colabrationContact.setEdit_task(cursor.getInt(cursor.getColumnIndex(KEY_EDIT_TASK)));
                    colabrationContact.setDelete_task(cursor.getInt(cursor.getColumnIndex(KEY_DELETE_TASK)));
                    colabrationContact.setAssign_to_other(cursor.getInt(cursor.getColumnIndex(KEY_ASSIGN_TO_OTHER)));

                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return colabrationContact;
    }

    public ArrayList<Contact> getAllContactList() {
        ArrayList<Contact> contactArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from " + TABLE_CONTACT, null);

            while (cursor.moveToNext()) {
                Contact contact = new Contact();
                contact.setContactName(cursor.getString(cursor.getColumnIndex(KEY_CONTACT_NAME)));
                contact.setContactNumber(cursor.getString(cursor.getColumnIndex(KEY_CONTACT_NUMBER)));
                contact.setContactEmail(cursor.getString(cursor.getColumnIndex(KEY_CONTACT_EMAIL)));

                contactArrayList.add(contact);
            }

        } catch (Exception e) {
            Log.d("DB", e.getMessage());
        }
        return contactArrayList;
    }

    public String chekRealtedProject(String project_name) {
        SQLiteDatabase db = this.getReadableDatabase();
        String name;
        String query = "select related_project from " + TABLE_PROJECT + " where project_name ='" + project_name + "'";

        Log.d(Constant.TAG, "query = " + query);
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        name = cursor.getString(0);
        cursor.close();

        return name;
    }

    public void updateStatus(String contactId, int status, int position) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (position == 1) {
            values.put(KEY_ADD_TASK, status);
        } else if (position == 2) {
            values.put(KEY_EDIT_TASK, status);
        } else if (position == 3) {
            values.put(KEY_DELETE_TASK, status);
        } else if (position == 4) {
            values.put(KEY_ASSIGN_TO_OTHER, status);
        }

        values.put(KEY_FLAG,"U");
        sqLiteDatabase.update(TABLE_CONTACT_COLABRATION, values, Sqlite_Query.KEY_CONTACT_COLA_ID + "=?",
                new String[]{contactId});
    }

    public boolean CheckIsDataAlreadyInDBorNot(String projectName) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select project_name from " + TABLE_PROJECT + " where project_name ='" + projectName + "' AND "+KEY_DELETED+" = 0";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public boolean checkNumberExites(String projectId, String contactNumber) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select * from " + TABLE_CONTACT_COLABRATION + " where contact_number_cola ='" + contactNumber + "'" + " AND " + Sqlite_Query.KEY_PROJECT_COLA_ID+" = " + projectId + " AND "+KEY_DELETED+" = 0";
        Log.d("JD", "getQurery = " + query);
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public void exportDB(Context mContext) {
        try {
            File sd = mContext.getExternalFilesDir("/database/");
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
                String currentDBPath = "//data//" + mContext.getPackageName() + "//databases//" + DB_NAME;
                String backupDBPath = DB_NAME;
                File currentDB = new File(data, currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public CreateProject getProjectDetails(String projectId) {

        String selectQuery = "SELECT * FROM " + TABLE_PROJECT + " WHERE "+ Sqlite_Query.KEY_PROJECT_ID+" = " + projectId ;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        CreateProject createProject = new CreateProject();

        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            createProject.setProject_id(cursor.getString(cursor.getColumnIndex(KEY_PROJECT_ID)));
            createProject.setProject_name(cursor.getString(cursor.getColumnIndex(KEY_PROJECT_NAME)));
            createProject.setColorName(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_COLOR_NAME)));
            createProject.setColorPosition(cursor.getInt(cursor.getColumnIndex(KEY_COLOR_POSITION)));
            createProject.setDeleted(cursor.getInt(cursor.getColumnIndex(KEY_DELETED)));
            createProject.setMysql_insert(cursor.getInt(cursor.getColumnIndex(KEY_MYSQL_INSERT)));
            createProject.setMysql_updated(cursor.getInt(cursor.getColumnIndex(KEY_MYSQL_UPDATE)));
            createProject.setMysql_delete(cursor.getInt(cursor.getColumnIndex(KEY_MYSQL_DELETE)));
            createProject.setRealtedProject(cursor.getString(cursor.getColumnIndex(KEY_RELATED_PROJECT)));
            createProject.setProjectPositon(cursor.getInt(cursor.getColumnIndex(KEY_RELATED_PROJECT_POSITION)));
            createProject.setServerId(cursor.getString(cursor.getColumnIndex(Sqlite_Query.SERVER_ID)));
            createProject.setUser_id(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_USER_ID)));
            cursor.close();
        } else {
            createProject = null;
        }

        return createProject;
    }

    public void updateProject(String project_id, String projectName, String colorName, int colorPosition, String oldProjectName, String parentProject, int projectPositon, String flag) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PROJECT_ID, project_id);
        values.put(KEY_PROJECT_NAME, projectName);
        values.put(KEY_PROJECT_CREATE_DATE, Constant.CURRENT_DATE);
        values.put(KEY_PROJECT_CREATE_TIME, Constant.CURRENT_TIME);
        values.put(KEY_COLOR_POSITION, colorPosition);
        values.put(KEY_COLOR_NAME,colorName);
        values.put(KEY_RELATED_PROJECT, parentProject);
        values.put(KEY_RELATED_PROJECT_POSITION, projectPositon);
        values.put(KEY_FLAG, flag);
        db.update(TABLE_PROJECT, values, KEY_PROJECT_NAME + " = ?", new String[]{oldProjectName});
        Log.e("ProjectUpdate","Project_update");
    }

    public void updateProject(CreateProject createProject) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_PROJECT_NAME, createProject.getProject_name());
        values.put(KEY_PROJECT_CREATE_DATE, createProject.getProject_create_date());
        values.put(KEY_PROJECT_CREATE_TIME, createProject.getProject_create_time());
        values.put(KEY_COLOR_POSITION, createProject.getColorPosition());
        values.put(KEY_COLOR_NAME,createProject.getColorName());
        values.put(Sqlite_Query.KEY_MYSQL_DELETE,createProject.getDeleted());
        values.put(Sqlite_Query.KEY_DELETED,createProject.getDeleted());
        values.put(Sqlite_Query.KEY_MYSQL_INSERT,1);

        db.update(TABLE_PROJECT, values, Sqlite_Query.SERVER_ID + " = ?", new String[]{createProject.getServerId()});
    }

    public void updateTag( String tagName, String tagPosition, String oldTagName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

//        values.put(KEY_TAG_ID, tag_id);
        values.put(KEY_TAG_NAME, tagName);
        values.put(KEY_TAG_COLOR_POSITION, tagPosition);


        db.update(TABLE_TAG, values, KEY_TAG_NAME + " = ?", new String[]{oldTagName});
    }


    public void deleteProject(String projectServerId) {
        SQLiteDatabase db = this.getWritableDatabase();
      //  Log.e("procjectserver_id",projectServerId);
        try {
            db.delete(TABLE_PROJECT, KEY_PROJECT_ID+" = '"+projectServerId+"' AND ("+KEY_MYSQL_INSERT+" = 0 OR "+KEY_MYSQL_DELETE+" = 1)", null);
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_DELETED,"1");
            db.update(TABLE_PROJECT,contentValues, Sqlite_Query.KEY_PROJECT_ID+" = ? ", new String[]{projectServerId});
        } catch (Exception e) {
            e.printStackTrace();

            Log.e("delet_pro",e.toString());
        }

        Log.e("delet_pro","sucess");
    }

    public void deleteTaskFromServerID(String taskId) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete(TABLE_TASK, Sqlite_Query.SERVER_ID+" = '"+taskId+"' AND ("+KEY_MYSQL_INSERT+" = 0 OR "+KEY_MYSQL_DELETE+" = 1)", null);
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_DELETED,"1");
            db.update(TABLE_TASK,contentValues, Sqlite_Query.SERVER_ID+" = ? ", new String[]{taskId});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteTask(String taskId) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete(TABLE_TASK, Sqlite_Query.KEY_TASK_ID+" = "+taskId+" AND ("+KEY_MYSQL_INSERT+" = 0 OR "+KEY_MYSQL_DELETE+" = 1)", null);
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_DELETED,"1");
            db.update(TABLE_TASK,contentValues, Sqlite_Query.KEY_TASK_ID+" = ? ", new String[]{taskId});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void deleteTag(String tagName) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete(TABLE_TAG, "tag_name = ?", new String[]{tagName});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteContact(String contactId) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete(TABLE_CONTACT_COLABRATION, KEY_CONTACT_COLA_ID + " = "+contactId+" AND ("+KEY_MYSQL_INSERT+" = 0 OR "+KEY_MYSQL_DELETE+" = 1)" , null);
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_DELETED,"1");
            db.update(TABLE_CONTACT_COLABRATION,contentValues,KEY_CONTACT_COLA_ID+" = ? ", new String[]{contactId});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getStatus(String contactNumber, int position) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = null;
        if (position == 1) {
            query = "select add_task from " + TABLE_CONTACT_COLABRATION + " where contact_number_cola ='" + contactNumber + "' AND "+KEY_DELETED+ " = 0";
        } else if (position == 2) {
            query = "select edit_task from " + TABLE_CONTACT_COLABRATION + " where contact_number_cola ='" + contactNumber + "' AND "+KEY_DELETED+ " = 0";
        } else if (position == 3) {
            query = "select delete_task from " + TABLE_CONTACT_COLABRATION + " where contact_number_cola ='" + contactNumber + "' AND "+KEY_DELETED+ " = 0";
        } else if (position == 4) {
            query = "select assign_to_other from " + TABLE_CONTACT_COLABRATION + " where contact_number_cola ='" + contactNumber + "' AND "+KEY_DELETED+ " = 0";
        }
        int status = 0;
        Log.d(Constant.TAG, "query = " + query);
        Cursor cursor = db.rawQuery(query, null);
        if(cursor != null && cursor.moveToFirst()) {
             status = cursor.getInt(0);
            cursor.close();
        }
        return status;
    }

    public ArrayList<CreateProject> getProjectList() {
        ArrayList<CreateProject> createProjects = new ArrayList<>();

        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from " + TABLE_PROJECT +" WHERE "+KEY_DELETED +" = 0", null);

            while (cursor.moveToNext()) {
                CreateProject createProject = new CreateProject();
                createProject.setProject_id(cursor.getString(cursor.getColumnIndex(KEY_PROJECT_ID)));
                createProject.setProject_name(cursor.getString(cursor.getColumnIndex(KEY_PROJECT_NAME)));
                createProject.setColorName(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_COLOR_NAME)));
                createProject.setColorPosition(cursor.getInt(cursor.getColumnIndex(KEY_COLOR_POSITION)));
                createProject.setDeleted(cursor.getInt(cursor.getColumnIndex(KEY_DELETED)));
                createProject.setRealtedProject(cursor.getString(cursor.getColumnIndex(KEY_RELATED_PROJECT)));
                createProject.setMysql_insert(cursor.getInt(cursor.getColumnIndex(KEY_MYSQL_INSERT)));
                createProject.setMysql_updated(cursor.getInt(cursor.getColumnIndex(KEY_MYSQL_UPDATE)));
                createProject.setMysql_delete(cursor.getInt(cursor.getColumnIndex(KEY_MYSQL_DELETE)));
                createProjects.add(createProject);
            }

        } catch (Exception e) {
            Log.d("DB", e.getMessage());
        }
        return createProjects;
    }

    public ArrayList<CreateProject> getTagList() {
        ArrayList<CreateProject> createProjects = new ArrayList<>();


        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from " + TABLE_TAG, null);

            while (cursor.moveToNext()) {
                CreateProject createProject = new CreateProject();
                createProject.setProject_id(cursor.getString(cursor.getColumnIndex(KEY_TAG_ID)));
                createProject.setProject_name(cursor.getString(cursor.getColumnIndex(KEY_TAG_NAME)));
                createProject.setColorPosition(cursor.getInt(cursor.getColumnIndex(KEY_TAG_COLOR_POSITION)));
                createProjects.add(createProject);
            }

        } catch (Exception e) {
            Log.d("DB", e.getMessage());
        }
        return createProjects;
    }

    //add general data
    public void addgeneraldata(String timezone, String startday, String startpage, String startdaytime) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_TIMEZONE, timezone);
            values.put(KEY_START_DAY, startday);
            values.put(KEY_STARTPAGE, startpage);
            values.put(KEY_DAYTIME, startdaytime);
            db.insert(TABLE_GENERAL, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Task> getHighPriorityTask() {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 0);

            //String query = "select * from tbl_task where " + KEY_TASK_DATE + " < '" + currentdate+"'";
            String query = "select * from tbl_task where priority='High' AND "+KEY_DELETED+ " = 0";

            Log.e("Personal Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(KEY_TASK_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_PROJECT_ID)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_DATE)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(KEY_TASK_LOCATION)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_TAG)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_COMMENT)));
                task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_TASK_COMPLETED)) == 1);
                task.setType(Constant.TASK);
                task.setOnlyMe(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_ONLY_ME)));
                taskArrayList.add(task);
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> getHighPriorityInquiry() {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 0);

            //String query = "select * from tbl_task where " + KEY_TASK_DATE + " < '" + currentdate+"'";
            String query = "select * from "+TABLE_INQUIRY+" where priority='High' AND "+KEY_DELETED+ " = 0";

            Log.e("Personal Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PROJECT_Id)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_DATE)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_LOCATION)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_TAG)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COMMENT)));
                taskArrayList.add(task);
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> getMediumPriorityInquiry() {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 0);

            //String query = "select * from tbl_task where " + KEY_TASK_DATE + " < '" + currentdate+"'";
            String query = "select * from "+ Sqlite_Query.TABLE_INQUIRY+" WHERE priority='Medium' AND "+KEY_DELETED+ " = 0";

            Log.e("Personal Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PROJECT_Id)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_DATE)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_LOCATION)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_TAG)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COMMENT)));
                taskArrayList.add(task);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> getMediumPriorityTask() {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 0);

            //String query = "select * from tbl_task where " + KEY_TASK_DATE + " < '" + currentdate+"'";
            String query = "select * from tbl_task WHERE priority='Medium' AND "+KEY_DELETED+ " = 0";

            Log.e("Personal Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(KEY_TASK_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_PROJECT_ID)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_DATE)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(KEY_TASK_LOCATION)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_TAG)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_COMMENT)));
                task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_TASK_COMPLETED)) == 1);
                task.setType(Constant.TASK);
                task.setOnlyMe(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_ONLY_ME)));
                taskArrayList.add(task);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> getLowPriorityInquiry() {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 0);

            //String query = "select * from tbl_task where " + KEY_TASK_DATE + " < '" + currentdate+"'";
            String query = "select * from "+ Sqlite_Query.TABLE_INQUIRY+" where priority='Low' AND "+KEY_DELETED+ " = 0";

            Log.e("Personal Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PROJECT_Id)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_DATE)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_LOCATION)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_TAG)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COMMENT)));
                taskArrayList.add(task);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> getLowPriorityTask() {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 0);

            //String query = "select * from tbl_task where " + KEY_TASK_DATE + " < '" + currentdate+"'";
            String query = "select * from tbl_task where priority='Low' AND "+KEY_DELETED+ " = 0";

            Log.e("Personal Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(KEY_TASK_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_PROJECT_ID)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_DATE)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(KEY_TASK_LOCATION)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_TAG)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(KEY_TASK_COMMENT)));
                task.setCompleted(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_TASK_COMPLETED)) == 1);
                task.setType(Constant.TASK);
                task.setOnlyMe(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_ONLY_ME)));
                taskArrayList.add(task);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> getAllDataTask() {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 0);

            //String query = "select * from tbl_task where " + KEY_TASK_DATE + " < '" + currentdate+"'";
            String query = "select * from tbl_task where "+KEY_DELETED +" = 0";

            Log.e("Personal Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = getTaskFromCursor(cursor);
                taskArrayList.add(task);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<Task> getAllDataInquiry() {
        ArrayList<Task> taskArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, 0);

            //String query = "select * from tbl_task where " + KEY_TASK_DATE + " < '" + currentdate+"'";
            String query = "select * from "+ Sqlite_Query.TABLE_INQUIRY+" where "+KEY_DELETED +" = 0";

            Log.e("Personal Query", query);

            Cursor cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                Task task = new Task();
                task.setTaskId(cursor.getInt(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_NAME)));
                task.setTaskProjectId(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PROJECT_Id)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_DATE)));
                task.setLocation(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_INQUIRY_LOCATION)));
                task.setTaskTag(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_TAG)));
                task.setTaskPrority(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_PRIORITY)));
                task.setTaskColobration(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COLOBRATION)));
                task.setTaskComment(cursor.getString(cursor.getColumnIndexOrThrow(Sqlite_Query.KEY_INQUIRY_COMMENT)));
                taskArrayList.add(task);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskArrayList;
    }

    public ArrayList<CreateProject> getProjectListIs(String currentProject) {
        ArrayList<CreateProject> createProjects = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            // Cursor cursor = db.rawQuery("select * from " + Sqlite_Query.TABLE_PROJECT, null);
            Cursor cursor = db.rawQuery("select * from " + TABLE_PROJECT + " where " + KEY_PROJECT_NAME + " != '" + currentProject + "' AND "+KEY_DELETED+" = 0", null);

            while (cursor.moveToNext()) {
                CreateProject createProject = new CreateProject();
                createProject.setProject_name(cursor.getString(cursor.getColumnIndex(KEY_PROJECT_NAME)));
                createProject.setColorName(cursor.getString(cursor.getColumnIndex(Sqlite_Query.KEY_COLOR_NAME)));
                createProject.setColorPosition(cursor.getInt(cursor.getColumnIndex(KEY_COLOR_POSITION)));
                createProject.setDeleted(cursor.getInt(cursor.getColumnIndex(KEY_DELETED)));
                createProject.setMysql_insert(cursor.getInt(cursor.getColumnIndex(KEY_MYSQL_INSERT)));
                createProject.setMysql_updated(cursor.getInt(cursor.getColumnIndex(KEY_MYSQL_UPDATE)));
                createProject.setMysql_delete(cursor.getInt(cursor.getColumnIndex(KEY_MYSQL_DELETE)));
                createProjects.add(createProject);
            }

        } catch (Exception e) {
            Log.d("DB", e.getMessage());
        }
        return createProjects;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_TBL_TBL_PROJECT);
        db.execSQL(CREATE_TABLE_CONTACT);
        db.execSQL(CREATE_TABLE_CONTACT_COLABRATION);
        db.execSQL(CREATE_TABLE_TAG);
        db.execSQL(CREATE_TABLE_TASK);
        db.execSQL(CREATE_TABLE_IDEA);
        db.execSQL(CREATE_TABLE_INQUIRY);
        db.execSQL(CREATE_TABLE_LOCATION);
        db.execSQL(CREATE_TABLE_COMMENT);
        //db.execSQL(CREATE_TABLE_GENERAL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROJECT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACT_COLABRATION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TAG);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TASK);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IDEA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INQUIRY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMMENT);

//        if (newVersion > oldVersion)
//
//            db.execSQL("ALTER TABLE general ADD COLUMN '(timezone TEXT,"
//                    + " stary_day TEXT,"
//                    + " startpage TEXT,"
//                    + " start_day_time TEXT)'");

        onCreate(db);

    }

    /*
     * this method is for getting all the unsynced project need to insert
     * so that we can sync it with web database
     * */
    public Cursor getUnsyncedInsertedProject() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_PROJECT + " WHERE (" + KEY_MYSQL_INSERT + " = 0 AND " + KEY_FLAG + " = 'I') OR (" + KEY_MYSQL_INSERT + " = 0 AND " + KEY_FLAG + " = 'U')";
        Log.e("INSERT PROJECT SIZE = ",db.rawQuery(sql, null).getCount()+"");
        return db.rawQuery(sql, null);
    }

    public void updateStatusInProject(String projectId, String columnName, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(columnName, status);
        db.update(TABLE_PROJECT, values, Sqlite_Query.SERVER_ID + " = ?", new String[]{projectId});
    }

    public Cursor getUnsyncedEditedProject() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_PROJECT + " WHERE " + KEY_MYSQL_INSERT + " = 1 AND "+ KEY_MYSQL_UPDATE + " = 0 AND " + KEY_FLAG + " = 'U'";
        Log.e("EDIT PROJECT SIZE = ",db.rawQuery(sql, null).getCount()+"");
        return db.rawQuery(sql, null);
    }

    public Cursor getUnsyncedDeletedProject() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_PROJECT + " WHERE " + KEY_MYSQL_DELETE + " = 0 AND " + KEY_DELETED + " = 1";
        Log.e("DELETE PROJECT SIZE = ",db.rawQuery(sql, null).getCount()+"");
        return db.rawQuery(sql, null);
    }

    public void updateStatusInTask(String taskId, String columnName, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(columnName, status);
        try {
            db.update(TABLE_TASK, values, SERVER_ID + " = '" + taskId +"'" , null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void updateStatusInComment(String commentId, String columnName, String status) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(columnName, status);
            db.update(TABLE_COMMENT, values, Sqlite_Query.SERVER_ID + " = '" + commentId + "'", null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public String getTaskNameForId(String taskId) {
        String taskName = "";
        String query = "SELECT "+KEY_TASK_NAME+ " FROM "+TABLE_TASK+" WHERE "+KEY_TASK_ID+ " = "+taskId;
        Cursor cursor = getReadableDatabase().rawQuery(query,null);
        if(cursor != null)
        {
            if(cursor.moveToNext())
                taskName = cursor.getString(cursor.getColumnIndex(KEY_TASK_NAME));
            cursor.close();
        }
        return taskName;
    }

    public Cursor getUnsyncedInsertedComment() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_COMMENT + " WHERE (" + KEY_MYSQL_INSERT + " = 0 AND " + KEY_FLAG + " = 'I') OR (" + KEY_MYSQL_INSERT + " = 0 AND " + KEY_FLAG + " = 'U')";
        Log.e("INSERT COMMENT SIZE = ",db.rawQuery(sql, null).getCount()+"");
        return db.rawQuery(sql, null);
    }

    public Cursor getUnsyncedEditedComment() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_COMMENT + " WHERE " + KEY_MYSQL_INSERT + " = 1 AND "+ KEY_MYSQL_UPDATE + " = 0 AND " + KEY_FLAG + " = 'U'";
        Log.e("EDIT COMMENT SIZE = ",db.rawQuery(sql, null).getCount()+"");
        return db.rawQuery(sql, null);
    }

    public Cursor getUnsyncedDeletedComment() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_COMMENT + " WHERE " + KEY_MYSQL_DELETE + " = 0 AND " + KEY_DELETED + " = 1";
        Log.e("DELETE Comment SIZE = ",db.rawQuery(sql, null).getCount()+"");
        return db.rawQuery(sql, null);
    }

    public Cursor getUnsyncedAddSharedProject() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_CONTACT_COLABRATION + " WHERE (" + KEY_MYSQL_INSERT + " = 0 AND " + KEY_FLAG + " = 'I') OR (" + KEY_MYSQL_INSERT + " = 0 AND " + KEY_FLAG + " = 'U')";
        Log.e("Add Shared Pro SIZE = ",db.rawQuery(sql, null).getCount()+"");
        return db.rawQuery(sql, null);
    }

    public void updateStatusInContactCollabration(String cont_cola_id, String columnName, String status) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(columnName, status);
            db.update(TABLE_CONTACT_COLABRATION, values, KEY_CONTACT_COLA_ID + " = " + cont_cola_id, null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public Cursor getUnsyncedEditSharedProject() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_CONTACT_COLABRATION + " WHERE " + KEY_MYSQL_INSERT + " = 1 AND "+ KEY_MYSQL_UPDATE + " = 0 AND " + KEY_FLAG + " = 'U'";
        Log.e("Edit shared Pro per SIZ",db.rawQuery(sql, null).getCount()+"");
        return db.rawQuery(sql, null);
    }

    public Cursor getUnsyncedDeleteUserSharedProject() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_CONTACT_COLABRATION + " WHERE " + KEY_MYSQL_DELETE + " = 0 AND " + KEY_DELETED + " = 1";
        Log.e("delete shared Pro SIZ",db.rawQuery(sql, null).getCount()+"");
        return db.rawQuery(sql, null);
    }

    public Cursor getUnsyncedInsertedTask() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_TASK + " WHERE (" + KEY_MYSQL_INSERT + " = 0 AND " + KEY_FLAG + " = 'I') OR (" + KEY_MYSQL_INSERT + " = 0 AND " + KEY_FLAG + " = 'U')";
        Log.e("Add Task SIZE = ",db.rawQuery(sql, null).getCount()+"");
        return db.rawQuery(sql, null);
    }

    public Cursor getUnsyncedEditedTask() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_TASK + " WHERE " + KEY_MYSQL_INSERT + " = 1 AND "+ KEY_MYSQL_UPDATE + " = 0 AND " + KEY_FLAG + " = 'U'";
        Log.e("Edit Task SIZE",db.rawQuery(sql, null).getCount()+"");
        return db.rawQuery(sql, null);
    }

    public Cursor getUnsyncedDeletedTask() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_TASK + " WHERE " + KEY_MYSQL_DELETE + " = 0 AND " + KEY_DELETED + " = 1";
        Log.e("delete task SIZE",db.rawQuery(sql, null).getCount()+"");
        return db.rawQuery(sql, null);
    }

    public Cursor getUnsyncedInsertedIdea() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_IDEA + " WHERE (" + KEY_MYSQL_INSERT + " = 0 AND " + KEY_FLAG + " = 'I') OR (" + KEY_MYSQL_INSERT + " = 0 AND " + KEY_FLAG + " = 'U')";
        Log.e("Add IDEA SIZE = ",db.rawQuery(sql, null).getCount()+"");
        return db.rawQuery(sql, null);
    }

    public Cursor getUnsyncedDeletedIdea() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_IDEA + " WHERE " + KEY_MYSQL_DELETE + " = 0 AND " + KEY_DELETED + " = 1";
        Log.e("delete IDEA SIZE",db.rawQuery(sql, null).getCount()+"");
        return db.rawQuery(sql, null);
    }

    public Cursor getUnsyncedInsertedInquiry() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_INQUIRY + " WHERE (" + KEY_MYSQL_INSERT + " = 0 AND " + KEY_FLAG + " = 'I') OR (" + KEY_MYSQL_INSERT + " = 0 AND " + KEY_FLAG + " = 'U')";
        Log.e("Add INQUIRY SIZE = ",db.rawQuery(sql, null).getCount()+"");
        return db.rawQuery(sql, null);
    }

    public Cursor getUnsyncedDeletedInquiry() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_INQUIRY + " WHERE "+ KEY_MYSQL_DELETE + " = 0 AND " + KEY_DELETED + " = 1";
        Log.e("delete INQUIRY SIZE",db.rawQuery(sql, null).getCount()+"");
        return db.rawQuery(sql, null);
    }

    public void deleteProjectFromName(String projectName) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete(TABLE_PROJECT, KEY_PROJECT_NAME+" = "+projectName+" AND ("+KEY_MYSQL_INSERT+" = 0 OR "+KEY_MYSQL_DELETE+" = 1)", null);
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_DELETED,"1");
            db.update(TABLE_PROJECT,contentValues,KEY_PROJECT_NAME+" = ? ", new String[]{projectName});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateProjectId(String projectId, String projectName) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_PROJECT_ID,projectId);
            db.update(TABLE_PROJECT,contentValues,KEY_PROJECT_NAME+" = ? ", new String[]{projectName});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isProjectSyncedWithWeb(String projectId) {
        try {
            String query = "SELECT * FROM " + TABLE_PROJECT + " WHERE " + KEY_PROJECT_ID + " = " + projectId +" AND "+KEY_MYSQL_INSERT +" = 1";
            Cursor cursor = getReadableDatabase().rawQuery(query, null);
            return cursor != null && cursor.moveToNext();
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public boolean isProjectShareSyncedWithWeb(String projectShareId) {
        try {
            String query = "SELECT * FROM " + TABLE_CONTACT_COLABRATION + " WHERE " + KEY_CONTACT_COLA_ID + " = " + projectShareId +" AND "+KEY_MYSQL_INSERT +" = 1";
            Cursor cursor = getReadableDatabase().rawQuery(query, null);
            return cursor != null && cursor.moveToNext();
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public boolean CheckIfOtherProjectWithSameName(String projectName, String project_id) {
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String query = "select project_name from " + TABLE_PROJECT + " where project_name ='" + projectName + "' AND " + KEY_PROJECT_ID + " != " + project_id + " AND " + KEY_DELETED + " = 0";
            Cursor cursor = db.rawQuery(query, null);
            if(cursor != null && cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            else
                return false;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }


    public boolean isTaskSynced(String taskId) {
        try {
            String query = "SELECT * FROM " + TABLE_TASK + " WHERE " + KEY_TASK_ID + " = " + taskId +" AND "+KEY_MYSQL_INSERT +" = 1";
            Cursor cursor = getReadableDatabase().rawQuery(query, null);
            return cursor != null && cursor.moveToNext();
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public boolean isIdeaSynced(String taskId) {
        try {
            String query = "SELECT * FROM " + TABLE_IDEA + " WHERE " + KEY_IDEA_ID + " = " + taskId +" AND "+KEY_MYSQL_INSERT +" = 1";
            Cursor cursor = getReadableDatabase().rawQuery(query, null);
            return cursor != null && cursor.moveToNext();
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public boolean isInquirySynced(String taskId) {
        try {
            String query = "SELECT * FROM " + TABLE_INQUIRY + " WHERE " + KEY_INQUIRY_ID + " = " + taskId +" AND "+KEY_MYSQL_INSERT +" = 1";
            Cursor cursor = getReadableDatabase().rawQuery(query, null);
            return cursor != null && cursor.moveToNext();
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public void updateStatusInIdea(String taskId, String columnName, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(columnName, status);
        try {
            db.update(TABLE_IDEA, values, Sqlite_Query.SERVER_ID + " = '" + taskId +"'" , null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void deleteIdea(String taskId) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete(TABLE_IDEA, KEY_IDEA_ID+" = "+taskId+" AND ("+KEY_MYSQL_INSERT+" = 0 OR "+KEY_MYSQL_DELETE+" = 1)", null);
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_DELETED,"1");
            db.update(TABLE_IDEA,contentValues,KEY_IDEA_ID+" = ? ", new String[]{taskId});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteIdeaFromServerId(String taskId) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete(TABLE_IDEA, Sqlite_Query.SERVER_ID+" = '"+taskId+"' AND ("+KEY_MYSQL_INSERT+" = 0 OR "+KEY_MYSQL_DELETE+" = 1)", null);
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_DELETED,"1");
            db.update(TABLE_IDEA,contentValues, Sqlite_Query.SERVER_ID+" = ? ", new String[]{taskId});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteInquiry(String taskId) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete(TABLE_INQUIRY, KEY_INQUIRY_ID+" = "+taskId+" AND ("+KEY_MYSQL_INSERT+" = 0 OR "+KEY_MYSQL_DELETE+" = 1)", null);
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_DELETED,"1");
            db.update(TABLE_INQUIRY,contentValues,KEY_INQUIRY_ID+" = ? ", new String[]{taskId});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteInquiryFromServerId(String taskId) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete(TABLE_INQUIRY, Sqlite_Query.SERVER_ID+" = '"+taskId+"' AND ("+KEY_MYSQL_INSERT+" = 0 OR "+KEY_MYSQL_DELETE+" = 1)", null);
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_DELETED,"1");
            db.update(TABLE_INQUIRY,contentValues, Sqlite_Query.SERVER_ID+" = ? ", new String[]{taskId});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateStatusInInquiry(String inquiryId, String columnName, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(columnName, status);
        try {
            db.update(TABLE_INQUIRY, values, SERVER_ID + " = '" + inquiryId +"'" , null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public boolean isCommentSynced(String commentId) {
        try {
            String query = "SELECT * FROM " + TABLE_COMMENT + " WHERE " + KEY_COMMENT_ID + " = " + commentId +" AND "+KEY_MYSQL_INSERT +" = 1";
            Cursor cursor = getReadableDatabase().rawQuery(query, null);
            return cursor != null && cursor.moveToNext();
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public void completeTheIdea(int taskId) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(Sqlite_Query.KEY_IDEA_COMPLETED, 1);
            db.update(TABLE_IDEA, values, KEY_IDEA_ID + " = " + taskId, null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void completeTheTask(int taskId) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(Sqlite_Query.KEY_TASK_COMPLETED, 1);
            db.update(TABLE_TASK, values, KEY_TASK_ID + " = " + taskId, null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void completeTheInquiry(int taskId) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(Sqlite_Query.KEY_INQUIRY_COMPLETED, 1);
            db.update(TABLE_INQUIRY, values, KEY_INQUIRY_ID + " = " + taskId, null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public Cursor getUnsyncedEditedIdea() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM " + Sqlite_Query.TABLE_IDEA + " WHERE " + KEY_MYSQL_INSERT + " = 1 AND "+ KEY_MYSQL_UPDATE + " = 0 AND " + KEY_FLAG + " = 'U'";
        Log.e("Edit Idea SIZE",db.rawQuery(sql, null).getCount()+"");
        return db.rawQuery(sql, null);
    }

    public Cursor getUnsyncedEditedInquiry() {
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM " + Sqlite_Query.TABLE_INQUIRY + " WHERE " + KEY_MYSQL_INSERT + " = 1 AND "+ KEY_MYSQL_UPDATE + " = 0 AND " + KEY_FLAG + " = 'U'";
        Log.e("Edit INQUIRY SIZE",db.rawQuery(sql, null).getCount()+"");
        return db.rawQuery(sql, null);
    }

    public void logOut() {
        getWritableDatabase().execSQL("delete from "+ Sqlite_Query.TABLE_PROJECT);
        getWritableDatabase().execSQL("delete from "+ Sqlite_Query.TABLE_CONTACT_COLABRATION);
        getWritableDatabase().execSQL("delete from "+ Sqlite_Query.TABLE_TASK);
        getWritableDatabase().execSQL("delete from "+ Sqlite_Query.TABLE_IDEA);
        getWritableDatabase().execSQL("delete from "+ Sqlite_Query.TABLE_INQUIRY);
        getWritableDatabase().execSQL("delete from "+ Sqlite_Query.TABLE_COMMENT);
    }

    public void saveSharedTask(String task_id, String user_id) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(Sqlite_Query.KEY_TASK_COLOBRATION, user_id);
            db.update(TABLE_TASK, values, Sqlite_Query.SERVER_ID + " = '" + task_id +"'", null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void updatePermissionContactCollabration(ColabrationContact contact) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(Sqlite_Query.KEY_ADD_TASK, contact.getAdd_task());
            values.put(Sqlite_Query.KEY_EDIT_TASK, contact.getEdit_task());
            values.put(Sqlite_Query.KEY_DELETE_TASK, contact.getDelete_task());
            values.put(Sqlite_Query.KEY_ASSIGN_TO_OTHER, contact.getAssign_to_other());
            db.update(TABLE_CONTACT_COLABRATION, values, Sqlite_Query.KEY_CONTACT_COLA_ID + " = " + contact.getId(), null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void saveSharedIdea(String task_id, String user_id) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(Sqlite_Query.KEY_IDEA_COLOBRATION, user_id);
            db.update(TABLE_IDEA, values, Sqlite_Query.SERVER_ID + " = '" + task_id +"'", null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void saveSharedInquiry(String task_id, String user_id) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(Sqlite_Query.KEY_INQUIRY_COLOBRATION, user_id);
            db.update(TABLE_INQUIRY, values, Sqlite_Query.SERVER_ID + " = '" + task_id +"'", null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public boolean isServerProjectExists(String serverId)
    {
        try {
            String query = "SELECT * FROM " + TABLE_PROJECT + " WHERE " + Sqlite_Query.SERVER_ID + " = '" + serverId +"'";
            Cursor cursor = getReadableDatabase().rawQuery(query, null);
            return cursor != null && cursor.moveToNext();
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public boolean isTaskExists(String serverId) {
        try {
            String query = "SELECT * FROM " + Sqlite_Query.TABLE_TASK + " WHERE " + Sqlite_Query.SERVER_ID + " = '" + serverId +"'";
            Cursor cursor = getReadableDatabase().rawQuery(query, null);
            return cursor != null && cursor.moveToNext();
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public boolean isIdeaExists(String serverId) {
        try {
            String query = "SELECT * FROM " + Sqlite_Query.TABLE_IDEA + " WHERE " + Sqlite_Query.SERVER_ID + " = '" + serverId +"'";
            Cursor cursor = getReadableDatabase().rawQuery(query, null);
            return cursor != null && cursor.moveToNext();
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public boolean isInquiryExists(String serverId) {
        try {
            String query = "SELECT * FROM " + Sqlite_Query.TABLE_INQUIRY + " WHERE " + Sqlite_Query.SERVER_ID + " = '" + serverId +"'";
            Cursor cursor = getReadableDatabase().rawQuery(query, null);
            return cursor != null && cursor.moveToNext();
        }
        catch (Exception e)
        {
            return false;
        }
    }
}