package com.arccus.accountability.activity;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.arccus.accountability.R;
import com.arccus.accountability.helper.DataBaseHelper;
import com.arccus.accountability.shared_preference.SessionManager;

import org.w3c.dom.Text;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by JD on 09-03-2018.
 */

/**
 * Created by Manisha on 20/4/2018.
 */


public class Setting extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private ImageView ivBack;
    private TextView tvTitle, tvSave, tvLogout, tvGeneral, tvReminders, tvNotifications, tvSupport;
    private SessionManager sessionManager;
    private GoogleApiClient mGoogleApiClient;
    private Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        sessionManager = new SessionManager(Setting.this);

        initView();
    }

    private void initView() {

        ivBack = findViewById(R.id.ivBack);
        ivBack.setOnClickListener(this);

        tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText("Settings");

        tvSave = findViewById(R.id.tvSave);
        tvSave.setVisibility(View.GONE);

        tvGeneral = findViewById(R.id.tvGeneral);
        tvGeneral.setOnClickListener(this);

        tvReminders = findViewById(R.id.tvReminders);
        tvReminders.setOnClickListener(this);

        tvNotifications = findViewById(R.id.tvNotifications);
        tvNotifications.setOnClickListener(this);

        tvSupport = findViewById(R.id.tvSupport);
        tvSupport.setOnClickListener(this);

        tvLogout = findViewById(R.id.tvLogout);
        tvLogout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBack:

                finish();
                break;
            case R.id.tvGeneral:
//                intent = new Intent(Setting.this, GeneralActivity.class);
//                startActivity(intent);
                break;
            case R.id.tvReminders:
//                intent = new Intent(Setting.this, ReminderActivity.class);
//                startActivity(intent);
                break;
            case R.id.tvNotifications:
//                intent = new Intent(Setting.this, NotificationActivity.class);
//                startActivity(intent);
                break;
            case R.id.tvSupport:
                break;
            case R.id.tvLogout:
                LogoutDialog();
                break;
        }
    }

    private void LogoutDialog() {
        try {
            final Dialog dialog = new Dialog(Setting.this, R.style.AppCompatAlertDialogStyle);
            dialog.requestWindowFeature(1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            View m_view = LayoutInflater.from(Setting.this).inflate(R.layout.dlg_alert, null);
            TextView tvYes = m_view.findViewById(R.id.tvYes);

            String msg = "Are you sure want to logout?";

            try {
                if (DataBaseHelper.getInstance().getUnsyncedInsertedProject().getCount() > 0 ||
                        DataBaseHelper.getInstance().getUnsyncedAddSharedProject().getCount() > 0 ||
                        DataBaseHelper.getInstance().getUnsyncedInsertedTask().getCount() > 0 ||
                        DataBaseHelper.getInstance().getUnsyncedInsertedIdea().getCount() > 0 ||
                        DataBaseHelper.getInstance().getUnsyncedInsertedInquiry().getCount() > 0 ||
                        DataBaseHelper.getInstance().getUnsyncedInsertedComment().getCount() > 0
                )
                {
                    msg = "There are some data not synced with server yet, Logout can cause data loss!\n Are you sure want to logout?";
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            TextView tvNo = m_view.findViewById(R.id.tvNo);
            ((TextView) m_view.findViewById(R.id.tvMessage)).setText(msg);

            tvYes.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dialog.cancel();
                    if (sessionManager.getLogintType().equalsIgnoreCase("0")) {
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        Log.e("Logout","logout");
                                        Log.e("status",""+status);
                                        Logout();
                                    }
                                });
                    } else if (sessionManager.getLogintType().equalsIgnoreCase("1")) {
                        Logout();
                    } else if (sessionManager.getLogintType().equalsIgnoreCase("2")) {
                        LoginManager.getInstance().logOut();
                        Logout();
                    }
                }
            });
            tvNo.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dialog.cancel();
                }
            });
            dialog.setContentView(m_view);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    private void Logout() {
        DataBaseHelper.getInstance().logOut();
        sessionManager.Logout(Setting.this);
        Intent intentLogin = new Intent(Setting.this, Splash.class);
        intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentLogin);
        finishAffinity();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
