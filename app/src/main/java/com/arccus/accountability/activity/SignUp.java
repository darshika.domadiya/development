package com.arccus.accountability.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arccus.accountability.R;
import com.arccus.accountability.shared_preference.SessionManager;

import com.arccus.accountability.utils.AppUtils;
import com.arccus.accountability.utils.CommanFunction;
import com.arccus.accountability.utils.CommonDialog;
import com.arccus.accountability.utils.LoaderView;
import com.arccus.accountability.utils.Validator;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

/**
 * Created by Admin on 21/12/2017.
 */

public class SignUp extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private static final int RC_SIGN_IN = 007;
    private String TAG = "SignUp";
    private EditText etvFullName, etvMobileNumber, etvEmail, etvPassword;
    private Button btnSignUp;
    private TextView tvLogIn;
    private ImageButton  llGoogle, llFacebook;
    private GoogleApiClient mGoogleApiClient;
    private CallbackManager mCallbackManager;
    private ProgressDialog pd;
    private SessionManager sessionManager;

    LoaderView loaderView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.new_activity_register);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        sessionManager = new SessionManager(SignUp.this);

        loaderView = new LoaderView(SignUp.this);

        initView();
    }

    private void initView() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        initFacebook();

        etvFullName = findViewById(R.id.etvFullName);
        etvMobileNumber = findViewById(R.id.etvMobileNumber);
        etvEmail = findViewById(R.id.etvEmail);
        etvPassword = findViewById(R.id.etvPassword);

        btnSignUp = findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(this);

        tvLogIn = findViewById(R.id.tvLogIn);
        tvLogIn.setOnClickListener(this);

        llGoogle = findViewById(R.id.llGoogle);
        llGoogle.setOnClickListener(this);

        llFacebook = findViewById(R.id.llFacebook);
        llFacebook.setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSignUp:
                if (isValidation()) {
                    if (CommanFunction.isNetworkAvaliable(SignUp.this)) {
                        CallSignUpAPI();
                    } else {
                        CommonDialog.DialogAlert(SignUp.this, "No Internet Connection.");
                    }
                }
                break;
            case R.id.tvLogIn:
                Intent intentLogin = new Intent(SignUp.this, Login.class);
                startActivity(intentLogin);
                finish();
                break;

            case R.id.llGoogle:
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            case R.id.llFacebook:
                if (AccessToken.getCurrentAccessToken() != null && AccessToken.getCurrentAccessToken().getPermissions().contains("public_profile") && AccessToken.getCurrentAccessToken().getPermissions().contains("email")) {
                    loginFBCall();
                } else {
                    LoginManager.getInstance().logInWithReadPermissions(SignUp.this, Arrays.asList("email", "public_profile"));
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("JD", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String profilePiclUrl = null;
            Log.e("JD", "display name: " + acct.getDisplayName());

            String personName = acct.getDisplayName();
            if (acct.getPhotoUrl() != null) {
                profilePiclUrl = acct.getPhotoUrl().toString();
                Log.e("JD", "personPhotoUrl: " + profilePiclUrl);

            }
            String email = acct.getEmail();
            String google_id = acct.getId();

            Log.e("google_id","" +google_id);
            sessionManager.setProfilePic(profilePiclUrl);
            sessionManager.setUserEmail(email);
            sessionManager.setUserName(personName);
            Google_FB_Login(email, personName, "google",google_id);
            Log.e("JD", "Name: " + personName + ", email: " + email);

        } else {
            // Signed out, show unauthenticated UI.
        }
    }

    private void initFacebook() {
        FacebookSdk.sdkInitialize(getApplicationContext());

        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        loginFBCall();
                    }

                    @Override
                    public void onCancel() {
                        AlertDialog.Builder builder = new AlertDialog.Builder(SignUp.this);
                        builder.setCancelable(false);
                        builder.setMessage("You have cancelled Facebook Dialog.")
                                .setPositiveButton("OK", null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(SignUp.this);
                        builder.setCancelable(false);
                        builder.setMessage(exception.getMessage())
                                .setPositiveButton("OK", null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        exception.printStackTrace();
                    }
                });
    }

    private void loginFBCall() {
        pd = new ProgressDialog(SignUp.this);
        pd.setMessage("Please Wait");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.show();

        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        // Application code
                        if (isFinishing())
                            return;
                        if (pd != null && pd.isShowing()) {
                            pd.dismiss();
                        }
                        if (response != null && response.getError() != null) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(SignUp.this);
                            builder.setMessage(response.getError().getErrorMessage() != null ? response.getError().getErrorMessage() : "Could not get user data").setPositiveButton("OK", null);
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                        String id = object.optString("id", "0");
                        String email = null, strProfile = null;
                        String first_name = object.optString("first_name", "Unknown");
                        String last_name = object.optString("last_name", "Unknown");
                        try {
                            if (object.has("email")) {
                                email = object.getString("email");
                            }
                            if (object.has("picture")) {
                                strProfile = object.getJSONObject("picture").getJSONObject("data").getString("url");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        StringBuilder sb = new StringBuilder("");

                        if (id != null) {
                            sb.append("Id: " + id);
                        }
                        if (first_name != null) {
                            if (sb.length() > 0)
                                sb.append("\n");
                            sb.append("first_name: " + first_name);
                        }
                        if (last_name != null) {
                            if (sb.length() > 0)
                                sb.append("\n");
                            sb.append("last_name: " + last_name);
                        }
                        if (strProfile != null) {

                        }
                        sessionManager.setProfilePic(strProfile);
                        sessionManager.setUserEmail(email);
                        sessionManager.setUserName(first_name + " " + last_name);
                        Google_FB_Login(email, first_name + " " + last_name, "facebook","");
                        Log.e("FaceBookResponse: ", id + " Profile: " + strProfile + " Email: " + email);

                    }
                }
        );

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,friends,first_name,last_name,gender,updated_time,link,email,cover,picture.width(500)");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void CallSignUpAPI() {
        loaderView.show();
        RequestQueue queue = Volley.newRequestQueue(SignUp.this);
        StringRequest request = new StringRequest(Request.Method.POST, AppUtils.SIGNUP_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
              loaderView.dismiss();
                Log.d(TAG, "Sign Up Response = " + response);
                Log.d(TAG, "fcm Token = " + sessionManager.getFcmToken());
                try {

//                    {"status":"1","message":"User Register Successfully.","Common":{"Title":"User Register Api","version":"1.0","Description":"User Register Api","Method":"POST"},"Response":{}}
                    JSONObject jsonObject = new JSONObject(response);
                    int success = jsonObject.optInt("status");

                    if (success == 0) {
                        if(jsonObject.has("message") && !TextUtils.isEmpty(jsonObject.getString("message")))
                            CommonDialog.DialogAlert(SignUp.this, jsonObject.getString("message"));
                        else
                            CommonDialog.DialogAlert(SignUp.this, "NetWork Connection Error, Try Again.");
                    } else if (success == 1) {
                        String user_id =  jsonObject.getJSONObject("Response").getString("userID");
                        Log.e("user_id",user_id);


                        ShowOtpConfirmDialog(user_id,"normal");

                        /*Intent intentOtp = new Intent(SignUp.this, Otp_Confirm.class);
                        startActivity(intentOtp);*/

                    } else if (success == 2) {
                        CommonDialog.DialogAlert(SignUp.this, "This email is already registered.");
                    } else {
                        CommonDialog.DialogAlert(SignUp.this, "Please try again later.");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderView.dismiss();
                Log.d(TAG, "Sign Up Api Eroor = " + error);
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                //param.put("user_id", etvEmail.getText().toString().trim());
                param.put("user_fullname", etvFullName.getText().toString().trim());
                param.put("user_email", etvEmail.getText().toString().trim());
                param.put("user_password", etvPassword.getText().toString().trim());
                param.put("user_login_type", "normal");
                param.put("user_facebookid", "");
                param.put("user_googleid", "");
                param.put("user_device_id", sessionManager.getFcmToken());
                param.put("user_mobile_no", etvMobileNumber.getText().toString().trim());
                param.put("user_device_type", "android");
                return param;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    private void Google_FB_Login(final String email, final String personName, final String LoginType,final String google_id) {
        loaderView.show();
        RequestQueue queue = Volley.newRequestQueue(SignUp.this);

        final StringRequest request = new StringRequest(Request.Method.POST, AppUtils.SIGNUP_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loaderView.dismiss();
                Log.d(TAG, "Google_FB_Login_Response = " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int success = jsonObject.getInt("status");

                    if (success == 0) {
                        CommonDialog.DialogAlert(SignUp.this, "Try Again Later");
                    } else if(success == 1) {
                        String user_id =  jsonObject.getJSONObject("Response").getString("userID");
                        Log.e("user_id",user_id);
                        ShowOtpConfirmDialog(user_id, "normal");


//                        sessionManager.setLoginType(LoginType);
//                        Intent intentHome = new Intent(SignUp.this, Home.class);
//                        startActivity(intentHome);
//                        finish();
//                        Splash.getInstance().finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderView.dismiss();
                Log.d(TAG, "Google_Fb_login Error = " + error);

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                Log.e("param_google_id",""+google_id);
                param.put("user_email", email);
                param.put("user_fullname", personName);
                param.put("user_password", "");
                param.put("user_login_type", LoginType);
                param.put("user_facebookid", "");
                param.put("user_googleid", google_id);
                param.put("user_device_id", sessionManager.getFcmToken());
                param.put("user_mobile_no", "");
                param.put("user_device_type", "android");
                return param;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);
    }

    public boolean isValidation() {
        if (!Validator.isEmptyField(this, etvFullName)) {
            etvFullName.requestFocus();
            return false;
        } else if (!Validator.isEmptyField(this, etvMobileNumber)) {
            etvMobileNumber.requestFocus();
            return false;
        } else if (!Validator.isEmptyField(this, etvEmail)) {
            etvEmail.requestFocus();
            return false;
        } else if (!Validator.isEmptyField(this, etvPassword)) {
            etvPassword.requestFocus();
            return false;
        } else if (!Validator.isValidEmail(this, etvEmail)) {
            etvEmail.requestFocus();
            return false;
        } else if (!Validator.isPasswordValid(this, etvPassword)) {
            etvPassword.requestFocus();
            return false;
        }

        return true;
    }

    private void ShowOtpConfirmDialog(final String user_id, final String normal) {
        try {
            final Dialog dialog = new Dialog(SignUp.this, R.style.AppCompatAlertDialogStyle);
            dialog.requestWindowFeature(1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            View m_view = LayoutInflater.from(SignUp.this).inflate(R.layout.dlg_confirm, null);

            final EditText etvOtp = m_view.findViewById(R.id.etvOtp);
            etvOtp.setHint("Enter the code received");
            TextView tvConfirm = m_view.findViewById(R.id.tvConfirm);

            tvConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!Validator.isEmptyField(SignUp.this, etvOtp)) {
                        etvOtp.requestFocus();
                    } else {
                        if (CommanFunction.isNetworkAvaliable(SignUp.this)) {
                            CallOtpConfirmAPI(user_id,etvOtp.getText().toString().trim(), normal,dialog);
                        } else {
                            CommonDialog.DialogAlert(SignUp.this, "No Internet Connection.");
                        }
                    }
                }
            });

            dialog.setContentView(m_view);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Dialog Alert Exception = " + e);
        }
    }

    private void CallOtpConfirmAPI(final String user_id, final String OTPCode, final String normal, final Dialog dialog) {
        loaderView.show();
        RequestQueue queue = Volley.newRequestQueue(SignUp.this);

        StringRequest request = new StringRequest(Request.Method.POST, AppUtils.VERIFY_USER_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loaderView.dismiss();
                Log.d(TAG, "Login Confirm Password = " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int success = jsonObject.optInt("statusz");
                    if (success == 0) {
                        Toast.makeText(SignUp.this, "Invalid Confirmation Code.", Toast.LENGTH_SHORT).show();
                        //CommonDialog.DialogAlert(SignUp.this, "Invalid Confirmation Code.");
                    } else {
                        sessionManager.setUserId(jsonObject.getJSONObject("Response").getString("userID"));
                        sessionManager.setUserName(jsonObject.getJSONObject("Response").getString("user_fullname"));
                        sessionManager.setMobileNumber(jsonObject.getJSONObject("Response").getString("user_mobile_no"));
                        sessionManager.setUserEmail(jsonObject.getJSONObject("Response").getString("user_email"));

                        if(normal.equals("normal")){
                            sessionManager.setLoginType("1");
                        }else if(normal.equals("google")){
                            sessionManager.setLoginType("0");
                        } else if(normal.equals("facebbok")){
                        sessionManager.setLoginType("2");
                    }

                        sessionManager.setLoginType("1");
                        dialog.dismiss();
                        Intent intentHome = new Intent(SignUp.this, Home.class);
                        startActivity(intentHome);
                        finish();
                        Splash.getInstance().finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "CallOtpConfirmAPI  Error = " + error);
                loaderView.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("user_id",user_id);
                param.put("user_verificationcode", OTPCode);
                Log.e("PARAMS",param.toString());
                return param;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
