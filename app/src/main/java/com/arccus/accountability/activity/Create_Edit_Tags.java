package com.arccus.accountability.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.arccus.accountability.R;
import com.arccus.accountability.Response.ColorResponse;
import com.arccus.accountability.adapter.ShowProjectColor;
import com.arccus.accountability.helper.DataBaseHelper;
import com.arccus.accountability.model.ProjectColor;
import com.arccus.accountability.shared_preference.SessionManager;
import com.arccus.accountability.utils.Constant;
import com.arccus.accountability.utils.LoaderView;
import com.arccus.accountability.utils.Validator;
import com.arccus.accountability.utils.VolleyHelper;
import com.arccus.accountability.webservice.StaticDataApi;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.arccus.accountability.utils.AppUtils.ADD_PROJECT;
import static com.arccus.accountability.utils.AppUtils.ADD_TAG;
import static com.arccus.accountability.utils.AppUtils.EDIT_TAG;

public class Create_Edit_Tags extends AppCompatActivity implements View.OnClickListener {
    private String TAG = "Create_Edit_Projects";

    private LinearLayout rlSelectColor;
    private int  projectPosition;
    private String colorPosition = "1";
    private String colorName = "";
    private ImageView ivDelete, ivColor, ivBack;

    private DataBaseHelper baseHelper;
    LoaderView loaderView;


    EditText etvTagName;

    String titleName;
//    String tag_name = "";
//    String tag_id = "";

    ImageButton ibtnSend;

    TextView tvTitle;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_create__edit__tags);
        loaderView = new LoaderView(Create_Edit_Tags.this);

        baseHelper = new DataBaseHelper(Create_Edit_Tags.this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);



        rlSelectColor = findViewById(R.id.rlSelectColor);
        tvTitle = findViewById(R.id.tvTitle);
        etvTagName = findViewById(R.id.etvTagName);
        ibtnSend = findViewById(R.id.ibtnSend);
        ivColor = findViewById(R.id.ivColor);
        ivBack = findViewById(R.id.ivBack);
        ivBack.setOnClickListener(this);
        rlSelectColor.setOnClickListener(this);
        ibtnSend.setOnClickListener(this);
    }

    private void ShowProjectColor() {
        try {
            final Dialog dialog = new Dialog(Create_Edit_Tags.this, R.style.AppCompatAlertDialogStyle);
            dialog.requestWindowFeature(1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            View m_view = LayoutInflater.from(Create_Edit_Tags.this).inflate(R.layout.dlg_show_project, null);

            ImageView ivCloseDialog = m_view.findViewById(R.id.ivCloseDialog);
            ivCloseDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });


            RecyclerView rvProjectList = m_view.findViewById(R.id.rvProjectList);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(Create_Edit_Tags.this);
            rvProjectList.setLayoutManager(layoutManager);

            ShowProjectColor showProjectColor = new ShowProjectColor(Create_Edit_Tags.this, Constant.getProjectColor());
            rvProjectList.setAdapter(showProjectColor);
            showProjectColor.setOnItemClickListener(new ShowProjectColor.OnItemClickListener() {
                @Override
                public void onItemClick(int position, ProjectColor projectColor) {
                    Log.d("JD", "getPosition = " + position);
                    colorPosition = String.valueOf(position+1);
                    colorName = Constant.getProjectColor().get(position).getColorName();
                    ivColor.setImageResource(projectColor.getColor());
                    //tvProjectColor.setText(projectColor.getColorName());
                    dialog.dismiss();
                }
            });

            dialog.setContentView(m_view);
            dialog.setCancelable(true);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Dialog Alert Exception = " + e);
        }
    }
    public boolean isValidation() {
        if (!Validator.isEmptyField(this, etvTagName)) {
            etvTagName.requestFocus();
            return false;
        }else {
           // tag_name = etvTagName.getText().toString();



        }

        return true;
    }



    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.rlSelectColor:
                ShowProjectColor();
                break;

            case R.id.ivBack:
               finish();
                break;

            case R.id.ibtnSend:
                if (isValidation()) {


                    CallApiInsertTag(etvTagName.getText().toString().trim(),colorPosition);




                }
                break;
        }

    }

    private void CallApiInsertTag(final String tagname, final String colorPosition) {


        loaderView.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ADD_TAG ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ADD PROJECT RESPONSE = ",""+response);
                        loaderView.dismiss();
                        try {
                            JSONObject obj = new JSONObject(response);
                            String message = obj.getString("message");
                            if (obj.has("status") && obj.getString("status").equals("1")) {


                               /* {"status":"1","message":"Project Added Successfully.","Common":{"Title":"Project Added Api","version":"1.0","Description":"Project Added Api","Method":"POST"},
                                    "Response":{"project_id":"56","user_id":"6","project_name":"Test",
                                        "color_label":"Blue","priority":"","project_cdate":"2020-06-23 08:58:28",
                                        "project_udate":"2020-06-23 08:58:28"}}*/

                                String tag_id = obj.getJSONObject("Response").getString("tag_id");
                                Log.e("tag_id",tag_id);

                                baseHelper.addTag(tag_id,etvTagName.getText().toString().trim(), colorPosition);



                                Toast.makeText(Create_Edit_Tags.this, message, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(Create_Edit_Tags.this, Home.class);
//                                intent.putExtra("projectId", project_id);
//                                intent.putExtra("project_name", etvProjectName.getText().toString().trim());
//                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(Create_Edit_Tags.this, message, Toast.LENGTH_SHORT).show();


                                //if there is some error
                                //  updateStatusInProject(projectId, NOT_SYNCED_WITH_SERVER,KEY_MYSQL_INSERT);
                            }
                        } catch (JSONException e) {
                           loaderView.dismiss();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loaderView.dismiss();
                        //on error
                        // updateStatusInProject(projectId, NOT_SYNCED_WITH_SERVER,KEY_MYSQL_INSERT);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("userID", SessionManager.getInstance().getUserId());
                params.put("tag_title",tagname);

                params.put("colorID",colorPosition);

                Log.e("Insert_tag_PARAMS",""+params.toString());
                return params;
            }
        };

        VolleyHelper.getInstance().addToRequestQueue(stringRequest);
    }
}
