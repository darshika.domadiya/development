package com.arccus.accountability.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arccus.accountability.R;
import com.arccus.accountability.shared_preference.SessionManager;
import com.arccus.accountability.utils.AppUtils;
import com.arccus.accountability.utils.CommanFunction;
import com.arccus.accountability.utils.CommonDialog;
import com.arccus.accountability.utils.LoaderView;
import com.arccus.accountability.utils.Validator;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

/**
 * Created by Admin on 21/12/2017.
 */

public class Login extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private static final int RC_SIGN_IN = 123;
    private String TAG = "Login";
    private EditText etvEmail, etvPassword;
    private GoogleApiClient mGoogleApiClient;
    private CallbackManager mCallbackManager;
    //  private ProgressDialog pd;
    private SessionManager sessionManager;
    private TextView tvForgotPassword;

    LoaderView loaderView;


    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.new_activity_login);

      /*  Commen_Data.Sync_All(getApplicationContext());
        Intent i = new Intent(Login.this, Sync_all.class);
        sendBroadcast(i);*/
        loaderView = new LoaderView(Login.this);

        initView();
    }

    private void initView() {
        sessionManager = new SessionManager(Login.this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        initFacebook();

        etvEmail = findViewById(R.id.etvEmail);
        etvPassword = findViewById(R.id.etvPassword);

        Button btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);

        TextView tvSignup = findViewById(R.id.tvSignup);
        tvSignup.setOnClickListener(this);

        ImageButton llGoogle = findViewById(R.id.llGoogle);
        llGoogle.setOnClickListener(this);

        ImageButton llFacebook = findViewById(R.id.llFacebook);
        llFacebook.setOnClickListener(this);

        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        tvForgotPassword.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                if (isValidation()) {
                    if (CommanFunction.isNetworkAvaliable(Login.this)) {
                        CallLoginAPI();
                    } else {
                        CommonDialog.DialogAlert(Login.this, "No Internet Connection.");
                    }
                }
                break;

            case R.id.tvSignup:
                Intent intentSignup = new Intent(Login.this, SignUp.class);
                startActivity(intentSignup);
                finish();
                break;

            case R.id.llGoogle:

                Log.e("Googleclick","Googleclick");
                if (CommanFunction.isNetworkAvaliable(Login.this)) {
                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, RC_SIGN_IN);
                } else {
                    CommonDialog.DialogAlert(Login.this, "No Internet Connection.");
                }
                break;

            case R.id.llFacebook:
                if (CommanFunction.isNetworkAvaliable(Login.this)) {
                    if (AccessToken.getCurrentAccessToken() != null && AccessToken.getCurrentAccessToken().getPermissions().contains("public_profile") && AccessToken.getCurrentAccessToken().getPermissions().contains("email")) {
                        loginFBCall();
                    } else {
                        LoginManager.getInstance().logInWithReadPermissions(Login.this, Arrays.asList("email", "public_profile"));
                    }
                } else {
                    CommonDialog.DialogAlert(Login.this, "No Internet Connection.");
                }
                break;

            case R.id.tvForgotPassword:
                Intent intentForgot = new Intent(Login.this, ForgotPassword.class);
                startActivity(intentForgot);
                finish();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);

        Log.e("requestcode",""+requestCode);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
            Log.e("auth","auth");
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("JD", "handleSignInResult:" + result.isSuccess());
        Log.e("result",""+result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String profilePiclUrl = null;
            Log.e("JD", "display name: " + acct.getDisplayName());

            String personName = acct.getDisplayName();
            if (acct.getPhotoUrl() != null) {
                profilePiclUrl = acct.getPhotoUrl().toString();
                Log.e("JD", "personPhotoUrl: " + profilePiclUrl);

            }
            String email = acct.getEmail();
            String google_id = acct.getId();
            Log.e("JD", "Name: " + personName + ", email: " + email);
            sessionManager.setProfilePic(profilePiclUrl);
            sessionManager.setUserEmail(email);
            sessionManager.setUserName(personName);
            Google_FB_Login(email, personName, "0",google_id);
            Log.e("google_fb_sucess","google_fb_sucess");

        } else {

            Log.e("fail",""+result.getStatus());
            // Signed out, show unauthenticated UI.
        }
    }

    private void initFacebook() {
        FacebookSdk.sdkInitialize(getApplicationContext());

        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        loginFBCall();
                    }

                    @Override
                    public void onCancel() {
                        AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                        builder.setCancelable(false);
                        builder.setMessage("You have cancelled Facebook Dialog.")
                                .setPositiveButton("OK", null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                        builder.setCancelable(false);
                        builder.setMessage(exception.getMessage())
                                .setPositiveButton("OK", null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        exception.printStackTrace();
                    }
                });
    }

    private void loginFBCall() {
      /*  pd = new ProgressDialog(Login.this);
        pd.setMessage("Please Wait");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.show();*/
        loaderView.show();

        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        // Application code
                        if (isFinishing())
                            return;
                       /* if (pd != null && pd.isShowing()) {
                            pd.dismiss();
                        }*/
                        loaderView.dismiss();
                        if (response != null && response.getError() != null) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                            builder.setMessage(response.getError().getErrorMessage() != null ? response.getError().getErrorMessage() : "Could not get user data").setPositiveButton("OK", null);
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                        String id = object.optString("id", "0");
                        String email = null, strProfile = null;
                        String first_name = object.optString("first_name", "Unknown");
                        String last_name = object.optString("last_name", "Unknown");
                        try {
                            if (object.has("email")) {
                                email = object.getString("email");
                            }
                            if (object.has("picture")) {
                                strProfile = object.getJSONObject("picture").getJSONObject("data").getString("url");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        StringBuilder sb = new StringBuilder("");

                        if (id != null) {
                            sb.append("Id: " + id);
                        }
                        if (first_name != null) {
                            if (sb.length() > 0)
                                sb.append("\n");
                            sb.append("first_name: " + first_name);
                        }
                        if (last_name != null) {
                            if (sb.length() > 0)
                                sb.append("\n");
                            sb.append("last_name: " + last_name);
                        }
                        if (strProfile != null) {

                        }
                        Log.e("FaceBookResponse: ", id + " Profile: " + strProfile + " Email: " + email);
                        sessionManager.setProfilePic(strProfile);
                        sessionManager.setUserEmail(email);
                        sessionManager.setUserName(first_name + " " + last_name);

                        Google_FB_Login(email, first_name + " " + last_name, "2", id);

                    }
                }
        );

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,friends,first_name,last_name,gender,updated_time,link,email,cover,picture.width(500)");
        request.setParameters(parameters);
        request.executeAsync();
    }

    public boolean isValidation() {
        if (!Validator.isEmptyField(this, etvEmail)) {
            etvEmail.requestFocus();
            return false;
        } else if (!Validator.isEmptyField(this, etvPassword)) {
            etvPassword.requestFocus();
            return false;
        } else if (!Validator.isValidEmail(this, etvEmail) && !Validator.isValidMobileNo(this,etvEmail)) {
            etvEmail.requestFocus();
            return false;
        } else if (!Validator.isPasswordValid(this, etvPassword)) {
            etvPassword.requestFocus();
            return false;
        }
        return true;
    }

    private void CallLoginAPI() {
        loaderView.show();
        RequestQueue queue = Volley.newRequestQueue(Login.this);
        StringRequest request = new StringRequest(Request.Method.POST, AppUtils.LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e(TAG, "Call Login API = " + response);
                loaderView.dismiss();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.optString("status");

                            if (status.equalsIgnoreCase("0")) {
                                CommonDialog.DialogAlert(Login.this, "Invalid Email or Password");
                            } else if (status.equalsIgnoreCase("1")) {

                                sessionManager.setUserId(jsonObject.getJSONObject("Response").getString("userID"));
                                sessionManager.setUserName(jsonObject.getJSONObject("Response").getString("user_fullname"));
                                sessionManager.setMobileNumber(jsonObject.getJSONObject("Response").getString("user_mobile_no"));
                                sessionManager.setUserEmail(jsonObject.getJSONObject("Response").getString("user_email"));


                                    sessionManager.setLoginType("1");

                                Intent intentHome = new Intent(Login.this, Home.class);
                                startActivity(intentHome);
                                finish();


                                //parseSuccessResponse(jsonObject);
                            } else if (status.equalsIgnoreCase("2")) {
                                ShowOtpConfirmDialog(jsonObject);
                            } else {
                                CommonDialog.DialogAlert(Login.this, "Something Wrong, Try Again Later ");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                Log.d(TAG, "CallLoginAPI = " + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderView.dismiss();
                CommonDialog.DialogAlert(Login.this, "Somthing Wrong, Try Again Later ");
                Log.d(TAG, "Login Respons Error = " + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> param = new HashMap<>();
                param.put("user_email", etvEmail.getText().toString().trim());
                param.put("user_password", etvPassword.getText().toString().trim());
                param.put("user_login_type", "normal");
                param.put("user_facebookid", "");
                param.put("user_googleid", "");
                param.put("user_device_type", "android");
                param.put("user_device_token", SessionManager.getInstance().getFcmToken());
                return param;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }


    private void Google_FB_Login(final String email, final String personName, final String LoginType, final String google_id) {
        loaderView.show();
        RequestQueue queue = Volley.newRequestQueue(Login.this);

        final StringRequest request = new StringRequest(Request.Method.POST, AppUtils.LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loaderView.dismiss();
                Log.d(TAG, "Google_FB_Login Response = " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int success = jsonObject.getInt("success");

                    if (success == 0) {
                        CommonDialog.DialogAlert(Login.this, "Try Again Later");
                    } else {
                        Splash.getInstance().finish();
                        sessionManager.setLoginType(LoginType);
                        Intent intentHome = new Intent(Login.this, Home.class);
                        startActivity(intentHome);
                        finish();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderView.dismiss();
                Log.d(TAG, "Google_Fb_login Error = " + error);

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("user_email", email);
                param.put("user_password", personName);
                param.put("user_login_type", "google");
                param.put("user_facebookid", "");
                param.put("user_googleid",google_id);
                param.put("user_device_type", "android");
                param.put("user_device_token", SessionManager.getInstance().getFcmToken());

                return param;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);
    }

    private void ShowOtpConfirmDialog(final JSONObject jsonObject) {
        try {
            final Dialog dialog = new Dialog(Login.this, R.style.AppCompatAlertDialogStyle);
            dialog.requestWindowFeature(1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            View m_view = LayoutInflater.from(Login.this).inflate(R.layout.dlg_confirm, null);

            final EditText etvOtp = m_view.findViewById(R.id.etvOtp);
            etvOtp.setHint("Enter the code received");
            TextView tvConfirm = m_view.findViewById(R.id.tvConfirm);

            tvConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!Validator.isEmptyField(Login.this, etvOtp)) {
                        etvOtp.requestFocus();
                    } else {
                        if (CommanFunction.isNetworkAvaliable(Login.this)) {
                            CallOtpConfirmAPI(etvOtp.getText().toString().trim(), dialog,jsonObject);
                        } else {
                            CommonDialog.DialogAlert(Login.this, "No Internet Connection.");
                        }
                    }
                }
            });

            dialog.setContentView(m_view);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Dialog Alert Exception = " + e);
        }
    }

    private void CallOtpConfirmAPI(final String OTPCode, final Dialog dialog,final JSONObject jsonObject) {
        loaderView.show();
        RequestQueue queue = Volley.newRequestQueue(Login.this);

        StringRequest request = new StringRequest(Request.Method.POST, AppUtils.VERIFY_USER_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loaderView.dismiss();
                Log.d(TAG, "Login Confirm Password = " + response);

                try {
                    JSONObject jsonObject1 = new JSONObject(response);
                    int success = jsonObject1.optInt("success");
                    if (success == 0) {
                        Toast.makeText(Login.this, "Invalid Confirmation Code.", Toast.LENGTH_SHORT).show();
                        //CommonDialog.DialogAlert(Login.this, "Invalid Confirmation Code.");
                    } else {
                        dialog.dismiss();
                        Splash.getInstance().finish();
//                        Intent intentHome = new Intent(Login.this, Home.class);
//                        startActivity(intentHome);
//                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "CallOtpConfirmAPI  Error = " + error);
                loaderView.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                String user_id="";
                try {
                    JSONObject user_data = jsonObject.getJSONObject("user_data");
                    user_id = user_data.getString("user_id");
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                param.put("user_id", user_id);
                param.put("user_verificationcode", OTPCode);
                Log.e("PARAMS ",""+param);
                return param;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("JD", "onConnectionFailed:" + connectionResult);
    }
}
