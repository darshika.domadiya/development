package com.arccus.accountability.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arccus.accountability.R;
import com.arccus.accountability.utils.AppUtils;
import com.arccus.accountability.utils.CommanFunction;
import com.arccus.accountability.utils.CommonDialog;
import com.arccus.accountability.utils.LoaderView;
import com.arccus.accountability.utils.Validator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

/**
 * Created by Admin on 23/12/2017.
 */

public class ForgotPassword extends AppCompatActivity {
    private EditText etvEmail;
    private String TAG = "ForgotPassword";

    LoaderView loaderView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_forgot_password);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        loaderView = new LoaderView(ForgotPassword.this);
        initView();
    }

    private void initView() {

        etvEmail = findViewById(R.id.etvEmail);

        Button btnForgotPassword = findViewById(R.id.btnForgotPassword);
        btnForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidation()) {
                    if (CommanFunction.isNetworkAvaliable(ForgotPassword.this)) {
                        CallForgotPasswrodAPI();
                    } else {
                        CommonDialog.DialogAlert(ForgotPassword.this, "No Internet Connection.");
                    }
                }

            }
        });
    }

    public boolean isValidation() {
        if (!Validator.isEmptyField(this, etvEmail)) {
            etvEmail.requestFocus();
            return false;
        }  else if (!Validator.isValidEmail(this, etvEmail) && !Validator.isValidMobileNo(this,etvEmail)) {
            etvEmail.requestFocus();
            return false;
        }

        return true;
    }

    private void CallForgotPasswrodAPI() {
     loaderView.show();

        RequestQueue queue = Volley.newRequestQueue(ForgotPassword.this);
        StringRequest request = new StringRequest(Request.Method.POST, AppUtils.FORGOT_PASSWORD_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
              loaderView.dismiss();
                Log.d(TAG, "Call Forgot Api Response = " + response);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int success = jsonObject.optInt("success");

                    if (success == 1) {
                        Dialog();
                    } else {
                        CommonDialog.DialogAlert(ForgotPassword.this, "Please Enter Correct Email Id. or Phone Number ");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              loaderView.dismiss();
                Log.d(TAG, "Forgot Password error = " + error);
                CommonDialog.DialogAlert(ForgotPassword.this, "Somthing Wrong. Please try again later.");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("user_email", etvEmail.getText().toString().trim());
                return param;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);
    }

    private void Dialog() {
        try {
            final Dialog dialog = new Dialog(ForgotPassword.this, R.style.AppCompatAlertDialogStyle);
            dialog.requestWindowFeature(1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            View m_view = LayoutInflater.from(ForgotPassword.this).inflate(R.layout.dlg_alert, null);
            TextView tvYes = m_view.findViewById(R.id.tvYes);

            tvYes.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            tvYes.setGravity(Gravity.CENTER);
            TextView tvNo = m_view.findViewById(R.id.tvNo);
            View vLine = m_view.findViewById(R.id.vLine);
            vLine.setVisibility(View.GONE);
            tvNo.setVisibility(View.GONE);
            ((TextView) m_view.findViewById(R.id.tvMessage)).setText("Password Send Successfully.");

            tvYes.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dialog.cancel();
                    Intent intent = new Intent(ForgotPassword.this, Login.class);
                    startActivity(intent);
                    finish();
                }
            });
            dialog.setContentView(m_view);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Dialog Alert Exception = " + e);
        }
    }
}
