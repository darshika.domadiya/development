package com.arccus.accountability.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.arccus.accountability.R;
import com.arccus.accountability.adapter.SelectProject;
import com.arccus.accountability.adapter.ShowProjectColor;
import com.arccus.accountability.helper.DataBaseHelper;
import com.arccus.accountability.helper.Sqlite_Query;
import com.arccus.accountability.model.ColabrationContact;
import com.arccus.accountability.model.CreateProject;
import com.arccus.accountability.model.ProjectColor;
import com.arccus.accountability.shared_preference.SessionManager;
import com.arccus.accountability.utils.Constant;
import com.arccus.accountability.utils.LoaderView;
import com.arccus.accountability.utils.Validator;
import com.arccus.accountability.utils.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.arccus.accountability.utils.AppUtils.ADD_PROJECT;
import static com.arccus.accountability.utils.AppUtils.DELETE_PROJECT;
import static com.arccus.accountability.utils.AppUtils.EDIT_PROJECT;

public class EditProjectActivity  extends AppCompatActivity implements View.OnClickListener {

    EditText etvProjectName;
    ImageView ivDelete;
    String Project_name = "";
    String Project_id = "";

    ImageButton ibtnSend;

    private String TAG = "Create_Edit_Projects";

    private LinearLayout llAddMember;
    private RecyclerView rvContactList;
    private Animation animationUp, animationDown;
    private TextView tvCount, tvTitle;
    private String tvName;
    private ImageView  ivColor, ivBack;
    private ToggleButton stNotification;

    private int  projectPosition;

    private int colorPosition;
   // private String projectId;
    private String projectName, projectSelected;
    private CreateProject createProject;
    private ArrayList<ColabrationContact> colabrationContacts = new ArrayList<>();
    private ArrayList<CreateProject> createProjects = new ArrayList<>();
    // private TextViewControl tvProjectColor;
    private LinearLayout rlSelectColor;
    private Spinner spinner1;
    private String colorName = "";



    LoaderView loaderView;
    //, rlSelectRealtedProject;tvRelatedProject
    private boolean contactsSaved;
    private DataBaseHelper db;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.edit_project);

        if(getIntent().getStringExtra("projectId") != null){

            Log.e("projectId",getIntent().getStringExtra("projectId"));
            Log.e("project_name",getIntent().getStringExtra("project_name"));
            Project_name = getIntent().getStringExtra("project_name");
            Project_id = getIntent().getStringExtra("projectId");



        }

        loaderView = new LoaderView(EditProjectActivity.this);
        db = new DataBaseHelper(EditProjectActivity.this);



        initview();


    }

    private void initview() {
        etvProjectName = findViewById(R.id.etvProjectName);
        etvProjectName.setText(Project_name);
        ivDelete = findViewById(R.id.ivDelete);
        ibtnSend = findViewById(R.id.ibtnSend);

        ibtnSend.setOnClickListener(this);
        ivDelete.setOnClickListener(this);

        spinner1 = findViewById(R.id.spinner1);
        tvCount = findViewById(R.id.tvCount);

        ivBack = findViewById(R.id.ivBack);
        ivBack.setOnClickListener(this);
        spinner1 = findViewById(R.id.spinner1);
        tvCount = findViewById(R.id.tvCount);

        rlSelectColor = findViewById(R.id.rlSelectColor);
        rlSelectColor.setOnClickListener(this);


        ivColor = findViewById(R.id.ivColor);

        ivDelete = findViewById(R.id.ivDelete);
        ivDelete.setOnClickListener(this);
        stNotification = findViewById(R.id.stNotification);

        llAddMember = findViewById(R.id.llAddMember);
        llAddMember.setOnClickListener(this);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) stNotification.getLayoutParams();
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        stNotification.setLayoutParams(params);
        ivDelete.setVisibility(View.VISIBLE);


        CreateProject createProject1 = new CreateProject();
        createProject1.setProject_name("No Project");
        createProjects.add(0, createProject1);

        SelectProject selectProject = new SelectProject(getApplicationContext(), createProjects);
        selectProject.notifyDataSetChanged();
        spinner1.setAdapter(selectProject);


        createProject = db.getProjectDetails(Project_id);

        tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText("Edit Project");
        etvProjectName.setText(Project_name);
        colorPosition = createProject.getColorPosition();
        colorName = createProject.getColorName();
        ivColor.setImageResource(Constant.getProjectColor().get(createProject.getColorPosition()).getColor());

            createProjects = db.getProjectListIs(createProject.getProject_name());

//        SelectProject selectProject = new SelectProject(getApplicationContext(), createProjects);

        selectProject.notifyDataSetChanged();
        spinner1.setAdapter(selectProject);

        spinner1.setSelection(createProject.getProjectPositon());

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               /* if (createProject != null) {
                    projectSelected = createProject.getRealtedProject();
                    projectPosition = position;
                } else {*/
                try {
                    projectSelected = createProjects.get(position).getProject_name();
                    projectPosition = position;
                    Log.e("position_spin", "" + position);
//                    ((TextView) view).setTextColor(Color.RED);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("spin_ex", e.toString());
//                    ((TextView) view).setTextColor(Color.RED);
                }
                // }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        animationUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        animationDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);

        rvContactList = findViewById(R.id.rvContactList);


    }

    public boolean isValidation() {
        if (!Validator.isEmptyField(this, etvProjectName)) {
            etvProjectName.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.ibtnSend:

                if (isValidation()) {
                    Log.e("send", "send");
                    EditProjectInServer(etvProjectName.getText().toString().trim(), colorName, colorPosition + "");

                }

                break;



            case R.id.ivBack:
                finish();
                break;
            case R.id.ivDelete:
                deleteDialog();
                break;

            case R.id.rlSelectColor:
                ShowProjectColor();
                break;

//            case R.id.ivEditProject:
//                ShowProjectColor();
//                break;




        }




    }

    private void EditProjectInServer(final String project_name, final String colorName, final String colorposition) {





            loaderView.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, EDIT_PROJECT ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("ADD PROJECT RESPONSE = ",""+response);
                            loaderView.dismiss();
                            try {
                                JSONObject obj = new JSONObject(response);
                                String message = obj.getString("message");
                                if (obj.has("status") && obj.getString("status").equals("1")) {


                               /* {"status":"1","message":"Project Added Successfully.","Common":{"Title":"Project Added Api","version":"1.0","Description":"Project Added Api","Method":"POST"},
                                    "Response":{"project_id":"56","user_id":"6","project_name":"Test",
                                        "color_label":"Blue","priority":"","project_cdate":"2020-06-23 08:58:28",
                                        "project_udate":"2020-06-23 08:58:28"}}*/

                                    String project_id = obj.getJSONObject("Response").getString("project_id");
                                    Log.e("project_id",project_id);


                                    db.updateProject(project_id,etvProjectName.getText().toString().trim(), colorName, colorPosition, projectName, projectSelected, projectPosition, "U");
                                    Toast.makeText(EditProjectActivity.this, message, Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(EditProjectActivity.this, CreateTaskActivity.class);
                                    intent.putExtra("projectId", project_id);
                                    intent.putExtra("project_name", etvProjectName.getText().toString().trim());
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(EditProjectActivity.this, message, Toast.LENGTH_SHORT).show();


                                    //if there is some error
                                    //  updateStatusInProject(projectId, NOT_SYNCED_WITH_SERVER,KEY_MYSQL_INSERT);
                                }
                            } catch (JSONException e) {
                                loaderView.dismiss();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            loaderView.dismiss();
                            //on error
                            // upda   teStatusInProject(projectId, NOT_SYNCED_WITH_SERVER,KEY_MYSQL_INSERT);
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();

                    params.put("userID", SessionManager.getInstance().getUserId());
                    params.put("project_id", Project_id);
                    params.put("title",project_name);
                    params.put("colorID",colorposition);
                    params.put("sharingUserIDs"," ");
                    params.put("relatedProjectID","");
                    params.put("isNotificationEnabled","1");
                    Log.e("Insert_PARAMS_INFO = ",""+params.toString());
                    return params;
                }
            };

            VolleyHelper.getInstance().addToRequestQueue(stringRequest);



    }

    private void ShowProjectColor() {
        try {
            final Dialog dialog = new Dialog(EditProjectActivity.this, R.style.AppCompatAlertDialogStyle);
            dialog.requestWindowFeature(1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            View m_view = LayoutInflater.from(EditProjectActivity.this).inflate(R.layout.dlg_show_project, null);

            ImageView ivCloseDialog = m_view.findViewById(R.id.ivCloseDialog);
            ivCloseDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });


            RecyclerView rvProjectList = m_view.findViewById(R.id.rvProjectList);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(EditProjectActivity.this);
            rvProjectList.setLayoutManager(layoutManager);

            ShowProjectColor showProjectColor = new ShowProjectColor(EditProjectActivity.this, Constant.getProjectColor());
            rvProjectList.setAdapter(showProjectColor);
            showProjectColor.setOnItemClickListener(new ShowProjectColor.OnItemClickListener() {
                @Override
                public void onItemClick(int position, ProjectColor projectColor) {
                    Log.d("JD", "getPosition = " + position);
                    colorPosition = position+1;
                    colorName = Constant.getProjectColor().get(position).getColorName();
                    ivColor.setImageResource(projectColor.getColor());
                    //tvProjectColor.setText(projectColor.getColorName());
                    dialog.dismiss();
                }
            });

            dialog.setContentView(m_view);
            dialog.setCancelable(true);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Dialog Alert Exception = " + e);
        }
    }



    private void deleteDialog() {
        try {
            final Dialog dialog = new Dialog(EditProjectActivity.this, R.style.AppCompatAlertDialogStyle);
            dialog.requestWindowFeature(1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            View m_view = LayoutInflater.from(EditProjectActivity.this).inflate(R.layout.dlg_alert, null);
            TextView tvYes = m_view.findViewById(R.id.tvYes);
            TextView tvNo = m_view.findViewById(R.id.tvNo);
            ((TextView) m_view.findViewById(R.id.tvMessage)).setText("Are you sure delete this project?");

            tvYes.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    Log.e("projectid123",Project_id);
                    db.deleteProject(Project_id);
                      DeleteProjectInServer();
                      dialog.cancel();
                    //ProjectFragment.getInstance().finish();
//                    Intent intent = new Intent(Create_Edit_Projects.this, Home.class);
//                    startActivity(intent);
//                    finish();
                }
            });
            tvNo.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dialog.cancel();
                }
            });
            dialog.setContentView(m_view);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Dialog Alert Exception = " + e);
        }
    }

    private void DeleteProjectInServer() {

        loaderView.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, DELETE_PROJECT ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ADD PROJECT RESPONSE = ",""+response);
                        loaderView.dismiss();
                        try {
                            JSONObject obj = new JSONObject(response);
                            String message = obj.getString("message");
                            if (obj.has("status") && obj.getString("status").equals("1")) {


                               /* {"status":"1","message":"Project Added Successfully.","Common":{"Title":"Project Added Api","version":"1.0","Description":"Project Added Api","Method":"POST"},
                                    "Response":{"project_id":"56","user_id":"6","project_name":"Test",
                                        "color_label":"Blue","priority":"","project_cdate":"2020-06-23 08:58:28",
                                        "project_udate":"2020-06-23 08:58:28"}}*/

//


//                                db.addProject(project_id, SessionManager.getInstance().getUserId(), etvProjectName.getText().toString().trim(),colorName, colorPosition, 0, 0, 0, 0, projectSelected, projectPosition,"I");
                                Toast.makeText(EditProjectActivity.this, message, Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(EditProjectActivity.this, Home.class);
//                                intent.putExtra("projectId", Project_id);
//                                intent.putExtra("project_name", etvProjectName.getText().toString().trim());
                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(EditProjectActivity.this, message, Toast.LENGTH_SHORT).show();


                                //if there is some error
                                //  updateStatusInProject(projectId, NOT_SYNCED_WITH_SERVER,KEY_MYSQL_INSERT);
                            }
                        } catch (JSONException e) {
                            loaderView.dismiss();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loaderView.dismiss();
                        //on error
                        // updateStatusInProject(projectId, NOT_SYNCED_WITH_SERVER,KEY_MYSQL_INSERT);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("userID", SessionManager.getInstance().getUserId());
                params.put("project_id", Project_id);
//                params.put("project_name",Project_name);
//                params.put("colorID",colorposition);
//                params.put("sharingUserIDs"," ");
//                params.put("relatedProjectID","");
//                params.put("isNotificationEnabled","1");
                Log.e("Delete_PARAMS_INFO = ",""+params.toString());
                return params;
            }
        };

        VolleyHelper.getInstance().addToRequestQueue(stringRequest);
    }

}
