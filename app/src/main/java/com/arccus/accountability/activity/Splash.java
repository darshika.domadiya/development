package com.arccus.accountability.activity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.arccus.accountability.R;
import com.arccus.accountability.shared_preference.SessionManager;
import com.google.firebase.iid.FirebaseInstanceId;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

public class Splash extends AppCompatActivity implements View.OnClickListener {
    public static Splash activity;
    SQLiteDatabase sqLiteDatabase;
    private SessionManager sessionManager;

    public static Splash getInstance() {
        return activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        activity = this;
        sessionManager = new SessionManager(Splash.this);


        if (sessionManager.getUserId().length() > 0) {
            Intent intentHome = new Intent(Splash.this, Home.class);
            startActivity(intentHome);
            finish();
        } else {

            setContentView(R.layout.activity_splash);
            initView();
        }
        //  sqLiteDatabase = Splash.this.openOrCreateDatabase(DataBaseHelper.DB_NAME, Context.MODE_PRIVATE, null);
        // sqLiteDatabase.execSQL("DROP TABLE IF EXISTS tbl_task" );
    }

    private void initView() {

        SessionManager sessionManager = new SessionManager(Splash.this);
        sessionManager.setFcmToken(FirebaseInstanceId.getInstance().getToken());
        Log.d("JD", "getFcm = " + sessionManager.getFcmToken());

        TextView tvLogin = findViewById(R.id.tvLogin);
        tvLogin.setOnClickListener(this);

        TextView tvSignup = findViewById(R.id.tvSignup);
        tvSignup.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvLogin:
                Intent intentLogin = new Intent(Splash.this, Login.class);
                startActivity(intentLogin);
                break;
            case R.id.tvSignup:
                Intent intentSignUp = new Intent(Splash.this, SignUp.class);
                startActivity(intentSignUp);
                break;
        }
    }

}
