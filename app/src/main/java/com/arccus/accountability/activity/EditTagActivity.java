package com.arccus.accountability.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.arccus.accountability.R;
import com.arccus.accountability.adapter.SelectProject;
import com.arccus.accountability.adapter.ShowProjectColor;
import com.arccus.accountability.helper.DataBaseHelper;
import com.arccus.accountability.model.CreateProject;
import com.arccus.accountability.model.ProjectColor;
import com.arccus.accountability.shared_preference.SessionManager;
import com.arccus.accountability.utils.Constant;
import com.arccus.accountability.utils.LoaderView;
import com.arccus.accountability.utils.Validator;
import com.arccus.accountability.utils.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.arccus.accountability.utils.AppUtils.ADD_PROJECT;
import static com.arccus.accountability.utils.AppUtils.DELETE_PROJECT;
import static com.arccus.accountability.utils.AppUtils.DELETE_TAG;
import static com.arccus.accountability.utils.AppUtils.EDIT_TAG;

public class EditTagActivity extends AppCompatActivity implements View.OnClickListener {
    private String TAG = "Create_Edit_Projects";


    private LinearLayout rlSelectColor;
    private int  projectPosition;
    private String colorPosition = "1";
    private String colorName = "";
    private ImageView ivDelete, ivColor, ivBack;

    private DataBaseHelper baseHelper;
    LoaderView loaderView;


    EditText etvTagName;

    String titleName;
    String tag_name = "";
    String tag_id = "";
    String tag_position = "";

    ImageButton ibtnSend;

    private CreateProject createProject;

    TextView tvTitle;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.edit_tag);



        if(getIntent().getStringExtra("tag_id") != null){

            Log.e("tag_id",getIntent().getStringExtra("tag_id"));
            Log.e("tag_name",getIntent().getStringExtra("tag_name"));
            tag_name = getIntent().getStringExtra("tag_name");
            tag_id = getIntent().getStringExtra("tag_id");
            tag_position = getIntent().getStringExtra("tag_position");




        }

        loaderView = new LoaderView(EditTagActivity.this);

        baseHelper = new DataBaseHelper(EditTagActivity.this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        rlSelectColor = findViewById(R.id.rlSelectColor);
        tvTitle = findViewById(R.id.tvTitle);
        ivDelete = findViewById(R.id.ivDelete);
        etvTagName = findViewById(R.id.etvTagName);
        ibtnSend = findViewById(R.id.ibtnSend);
        ivColor = findViewById(R.id.ivColor);
        ivBack = findViewById(R.id.ivBack);
        ivBack.setOnClickListener(this);
        rlSelectColor.setOnClickListener(this);
        ibtnSend.setOnClickListener(this);
        ivDelete.setOnClickListener(this);
        tvTitle.setText("Edit Tag");

        etvTagName.setText(tag_name);








        ivColor.setImageResource(Constant.getProjectColor().get(Integer.parseInt(colorPosition)).getColor());




//        ivColor.setImageResource(Constant.getTagColor().get(Integer.parseInt(colorPosition)).getColor());
       // tvTagColor.setText(Constant.getTagColor().get(colorPosition).getColorName());
    }



    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.rlSelectColor:
                ShowProjectColor();
                break;

            case R.id.ivBack:
                finish();
                break;

            case R.id.ibtnSend:
                if (isValidation()) {


                    CallApiUpdateTag(etvTagName.getText().toString().trim(),colorPosition);

                }
                break;

            case R.id.ivDelete:
                deleteDialog();
                break;
        }

    }

    private void CallApiUpdateTag(final String tagname, final String colorPosition) {



            loaderView.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, EDIT_TAG ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("ADD PROJECT RESPONSE = ",""+response);
                            loaderView.dismiss();
                            try {
                                JSONObject obj = new JSONObject(response);
                                String message = obj.getString("message");
                                if (obj.has("status") && obj.getString("status").equals("1")) {


                               /* {"status":"1","message":"Project Added Successfully.","Common":{"Title":"Project Added Api","version":"1.0","Description":"Project Added Api","Method":"POST"},
                                    "Response":{"project_id":"56","user_id":"6","project_name":"Test",
                                        "color_label":"Blue","priority":"","project_cdate":"2020-06-23 08:58:28",
                                        "project_udate":"2020-06-23 08:58:28"}}*/

                                String tag_id = obj.getJSONObject("Response").getString("tag_id");
                                Log.e("tag_id",tag_id);


                                    baseHelper.updateTag(etvTagName.getText().toString().trim(), colorPosition,tag_name);
                                    Toast.makeText(EditTagActivity.this, message, Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(EditTagActivity.this, CreateTaskActivity.class);
//                                intent.putExtra("projectId", project_id);
//                                intent.putExtra("project_name", etvProjectName.getText().toString().trim());
//                                startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(EditTagActivity.this, message, Toast.LENGTH_SHORT).show();


                                    //if there is some error
                                    //  updateStatusInProject(projectId, NOT_SYNCED_WITH_SERVER,KEY_MYSQL_INSERT);
                                }
                            } catch (JSONException e) {
                                loaderView.dismiss();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            loaderView.dismiss();
                            //on error
                            // updateStatusInProject(projectId, NOT_SYNCED_WITH_SERVER,KEY_MYSQL_INSERT);
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();

                    params.put("userID", SessionManager.getInstance().getUserId());
                    params.put("tag_id",tag_id);
                    params.put("tag_title",tagname);

                    params.put("colorID",colorPosition);

                    Log.e("Edit_tag_PARAMS",""+params.toString());
                    return params;
                }
            };

            VolleyHelper.getInstance().addToRequestQueue(stringRequest);
        }




    public boolean isValidation() {
        if (!Validator.isEmptyField(this, etvTagName)) {
            etvTagName.requestFocus();
            return false;
        }

        return true;
    }

    private void ShowProjectColor() {
        try {
            final Dialog dialog = new Dialog(EditTagActivity.this, R.style.AppCompatAlertDialogStyle);
            dialog.requestWindowFeature(1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            View m_view = LayoutInflater.from(EditTagActivity.this).inflate(R.layout.dlg_show_project, null);

            ImageView ivCloseDialog = m_view.findViewById(R.id.ivCloseDialog);
            ivCloseDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });


            RecyclerView rvProjectList = m_view.findViewById(R.id.rvProjectList);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(EditTagActivity.this);
            rvProjectList.setLayoutManager(layoutManager);

            ShowProjectColor showProjectColor = new ShowProjectColor(EditTagActivity.this, Constant.getProjectColor());
            rvProjectList.setAdapter(showProjectColor);
            showProjectColor.setOnItemClickListener(new ShowProjectColor.OnItemClickListener() {
                @Override
                public void onItemClick(int position, ProjectColor projectColor) {
                    Log.d("JD", "getPosition = " + position);
                    colorPosition = String.valueOf(position+1);
                    colorName = Constant.getProjectColor().get(position).getColorName();
                    ivColor.setImageResource(projectColor.getColor());
                    //tvProjectColor.setText(projectColor.getColorName());
                    dialog.dismiss();
                }
            });

            dialog.setContentView(m_view);
            dialog.setCancelable(true);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Dialog Alert Exception = " + e);
        }
    }


    private void deleteDialog() {
        try {
            final Dialog dialog = new Dialog(EditTagActivity.this, R.style.AppCompatAlertDialogStyle);
            dialog.requestWindowFeature(1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            View m_view = LayoutInflater.from(EditTagActivity.this).inflate(R.layout.dlg_alert, null);
            TextView tvYes = m_view.findViewById(R.id.tvYes);
            TextView tvNo = m_view.findViewById(R.id.tvNo);
            ((TextView) m_view.findViewById(R.id.tvMessage)).setText("Are you sure delete this tag?");

            tvYes.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    baseHelper.deleteTag(etvTagName.getText().toString().trim());
                    CallApiDeleteTag();
                    dialog.cancel();
                    finish();
                }
            });
            tvNo.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dialog.cancel();
                }
            });
            dialog.setContentView(m_view);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(Constant.TAG, "Dialog Alert Exception = " + e);
        }
    }

    private void CallApiDeleteTag() {

        loaderView.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, DELETE_TAG ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ADD PROJECT RESPONSE = ",""+response);
                        loaderView.dismiss();
                        try {
                            JSONObject obj = new JSONObject(response);
                            String message = obj.getString("message");
                            if (obj.has("status") && obj.getString("status").equals("1")) {


                               /* {"status":"1","message":"Project Added Successfully.","Common":{"Title":"Project Added Api","version":"1.0","Description":"Project Added Api","Method":"POST"},
                                    "Response":{"project_id":"56","user_id":"6","project_name":"Test",
                                        "color_label":"Blue","priority":"","project_cdate":"2020-06-23 08:58:28",
                                        "project_udate":"2020-06-23 08:58:28"}}*/

//


//                                db.addProject(project_id, SessionManager.getInstance().getUserId(), etvProjectName.getText().toString().trim(),colorName, colorPosition, 0, 0, 0, 0, projectSelected, projectPosition,"I");
                                Toast.makeText(EditTagActivity.this, message, Toast.LENGTH_SHORT).show();
//                                Intent intent = new Intent(EditTagActivity.this, CreateTaskActivity.class);
//                                intent.putExtra("projectId", Project_id);
//                                intent.putExtra("project_name", etvProjectName.getText().toString().trim());
//                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(EditTagActivity.this, message, Toast.LENGTH_SHORT).show();


                                //if there is some error
                                //  updateStatusInProject(projectId, NOT_SYNCED_WITH_SERVER,KEY_MYSQL_INSERT);
                            }
                        } catch (JSONException e) {
                            loaderView.dismiss();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loaderView.dismiss();
                        //on error
                        // updateStatusInProject(projectId, NOT_SYNCED_WITH_SERVER,KEY_MYSQL_INSERT);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("userID", SessionManager.getInstance().getUserId());
                params.put("tag_id", tag_id);
//                params.put("project_name",Project_name);
//                params.put("colorID",colorposition);
//                params.put("sharingUserIDs"," ");
//                params.put("relatedProjectID","");
//                params.put("isNotificationEnabled","1");
                Log.e("Delete_PARAMS_INFO = ",""+params.toString());
                return params;
            }
        };

        VolleyHelper.getInstance().addToRequestQueue(stringRequest);
    }


}
