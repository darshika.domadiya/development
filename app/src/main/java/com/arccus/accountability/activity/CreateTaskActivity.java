package com.arccus.accountability.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.arccus.accountability.R;
import com.arccus.accountability.fragment.CompletedTaskFragment;
import com.arccus.accountability.fragment.TaskFragment;
import com.arccus.accountability.fragment.TodayFragment;
import com.arccus.accountability.helper.DataBaseHelper;
import com.arccus.accountability.model.CreateProject;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import org.apmem.tools.layouts.FlowLayout;

import java.util.ArrayList;
import java.util.List;

public class CreateTaskActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvProjectName;
    String Project_name = "";
    String Project_id = "";


    private TabLayout tlToday;
    ViewPager vpToday;

    ImageView ivBack;

    private List<String> titles;

    ImageView   ivEditProject;;





    private int[] tabIcons = {
            R.drawable.ic_task,
            R.drawable.ic_task

    };
    private String[] titles1 = {
            "Task",
            "Completed"

    };





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_create_task);

        if(getIntent().getStringExtra("projectId") != null){

            Log.e("projectId",getIntent().getStringExtra("projectId"));
            Log.e("project_name",getIntent().getStringExtra("project_name"));
            Project_name = getIntent().getStringExtra("project_name");
            Project_id = getIntent().getStringExtra("projectId");



        }
        initView();

    }

    private void initView() {

        tvProjectName = findViewById(R.id.tvProjectName);
        ivEditProject = findViewById(R.id.ivEditProject);
        tvProjectName.setText(Project_name);
        ivEditProject.setOnClickListener(this);


        tlToday = findViewById(R.id.tlToday);
        vpToday = (ViewPager) findViewById(R.id.vpToday);
        ivBack = (ImageView) findViewById(R.id.ivBack);
        ivBack.setOnClickListener(this);




        titles = new ArrayList<>();
        titles.add("Task");
        titles.add("Completed");





        setupViewPager(vpToday);
        tlToday.setupWithViewPager(vpToday);
        setupTabIcons();






    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new TaskFragment(), "Task");
        adapter.addFragment(new CompletedTaskFragment(), "Completed");

        viewPager.setAdapter(adapter);
    }

    private void setupTabIcons() {
        tlToday.getTabAt(0).setCustomView(getTabView(0));
        tlToday.getTabAt(1).setCustomView(getTabView(1));
        //tlToday.getTabAt(2).setCustomView(getTabView(2));


        for(int i=0; i < tlToday.getTabCount(); i++) {
            View tab = ((ViewGroup) tlToday.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            p.setMargins(0, 0, 30, 0);
            tab.requestLayout();
        }
    }

    public View getTabView(int position) {
        View view = LayoutInflater.from(CreateTaskActivity.this).inflate(R.layout.custom_tab, null);
        TextView txt_title = view.findViewById(R.id.txt_title);
        txt_title.setText(titles.get(position));
        ImageView img_title = view.findViewById(R.id.img_title);
        img_title.setImageResource(tabIcons[position]);

      /*  if (position == 0) {
             txt_title.setText(titles1[position]);
            img_title.setImageResource(tabIcons[position]);
        } else if (position == 1) {
            txt_title.setText(titles1[position]);
            img_title.setImageResource(tabIcons[position]);
        }*/

      /*  else if (position == 2) {
            //txt_title.setTextColor(Color.WHITE);
            img_title.setImageResource(tabIcons[position]);
        }*/
        return view;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.ivBack:
                finish();
                break;

            case R.id.ivEditProject:


                Intent intent = new Intent(CreateTaskActivity.this, EditProjectActivity.class);
                intent.putExtra("projectId", Project_id);
                intent.putExtra("project_name", Project_name);
                startActivity(intent);
                break;

        }

    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
