package com.arccus.accountability.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.arccus.accountability.R;


import com.arccus.accountability.adapter.ProjectAdapter;
import com.arccus.accountability.fragment.ContactFragment;
import com.arccus.accountability.fragment.Next7DayFragment;
import com.arccus.accountability.fragment.PersonalFragment;
import com.arccus.accountability.fragment.TodayFragment;

import com.arccus.accountability.helper.DataBaseHelper;
import com.arccus.accountability.model.ProjectColor;
import com.arccus.accountability.model.Task;
import com.arccus.accountability.permission.PermissionResultCallback;
import com.arccus.accountability.permission.PermissionUtils;
import com.arccus.accountability.shared_preference.SessionManager;
import com.arccus.accountability.utils.AppUtils;

import com.arccus.accountability.utils.Constant;
import com.arccus.accountability.webservice.StaticDataApi;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


/**
 * Created by Admin on 22/12/2017.
 */

public class Home extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, ActivityCompat.OnRequestPermissionsResultCallback, PermissionResultCallback {

    ArrayList<String> permissions = new ArrayList<>();
    private String TAG = "Home";
    private SessionManager sessionManager;
    private GoogleApiClient mGoogleApiClient;
    private DrawerLayout drawer;
    private RelativeLayout rlInbox, rlToday, rlNext7Day, rlProjects, rlTags, rlLocations, rlFilters,
            rlDailyNotes, rlContactNote, rlMyContact, rlSettings, rlLogout,rlAllocateMe,rlAllocateOthers,rlHighPriority,
            rlMediumPriority,rlLowPriority,rlNoDueDate,rlAllData ;
    private TextView tvTitle, tvUserName, tvCreateProject, tvTag,tvEmailId;
    private RecyclerView rvProjects, rvTags;
    private ImageView ivPic, ivProjectsDrop, ivTagDrop,ivFilterDrop, ivSideMenu, ivInbox,
            ivToday, ivNext7Day, ivProjects, ivTagIcon, ivLocationIcon, ivFilters, ivDailyNotes, ivContactNoteIcon,
            ivMyContactIcon, ivLogouts, ivSetting;
    private Fragment fragment = null;
    private boolean isProject = true;
    private boolean isTag = true;
    private PermissionUtils permissionUtils;
    private ImageView[] image;
    private RelativeLayout[] relativeLayouts;
    private LinearLayout llProject, llTags,llFilters;
    private DataBaseHelper dataBaseHelper;
//    private ProjectAdapter projectAdapter, projectAdapter1;
    private String backStack = "Today";
    private TextView tvNoOfItemInbox, tvNoOfItemToday, tvNoOfItemNext;
    private RelativeLayout rlSearch;
    private EditText etvSearch;
    private ImageView ivSearchListClear, ivSearch, ivSorting;
    private Animation animationUp, animationDown;
    private boolean isCheckDate, isCheckPriority, isCheckSharing, isCheckProject;
    private int wheretoCome;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private LinearLayout menu;
    private String currentDate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_home);


//        StaticDataApi.CallGetColorApi();

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        sessionManager = new SessionManager(Home.this);
        permissionUtils = new PermissionUtils(this);
        permissions.add(Manifest.permission.READ_CONTACTS);
        permissions.add(Manifest.permission.WRITE_CONTACTS);
        permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissions.add(Manifest.permission.SEND_SMS);
        permissions.add(Manifest.permission.RECEIVE_SMS);
        permissionUtils.check_permission(permissions, "In this app all permission need.", 1);

        currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        dataBaseHelper = new DataBaseHelper(Home.this);

        animationUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        animationDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = pref.edit();
        wheretoCome = pref.getInt("wheretocome", 0);
        Log.e("wheretocome",""+pref.getInt("wheretocome", 0));
        initView();

//        registerReceiver(new My_Project_BroadcastReceiverSync_1(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                        /*All Data Syc From Here */
      /*  Commen_Data.Sync_All(getApplicationContext());
        Intent i = new Intent(Home.this, Sync_all.class);
        sendBroadcast(i);*/

        //getDashboard();
    }


    private void initView() {

        if (wheretoCome != 0) {
            backStack = pref.getString("startpage", "");
        }

        Log.e("backStack",""+backStack);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        menu = findViewById(R.id.menu);
        menu.setVisibility(View.VISIBLE);

        ivSearch = findViewById(R.id.ivSearch);
        ivSearch.setOnClickListener(this);

        ivSorting = findViewById(R.id.ivSorting);
        ivSorting.setOnClickListener(this);

        etvSearch = findViewById(R.id.etvSearch);

        rlSearch = findViewById(R.id.rlSearch);
        if (rlSearch.getVisibility() == View.VISIBLE) {
            rlSearch.setVisibility(View.GONE);
            etvSearch.setText("");
            ivSearch.setBackgroundResource(R.drawable.ic_search_new);
        }

        etvSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count != 0) {
                    ivSearchListClear.setVisibility(View.VISIBLE);
                    /*mOnItemClickListener.onItemClick(String.valueOf(s));
                    onITemIdeaFragment.onItemClick(String.valueOf(s));*/
                } else {
                    ivSearchListClear.setVisibility(View.GONE);
                }




            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ivSearchListClear = findViewById(R.id.ivSearchListClear);
        ivSearchListClear.setOnClickListener(this);

        //mOnItemClickListener.onItemClick(ivSearchListClear, etvSearch);



        /*Count task value */

       /* tvNoOfItemInbox = findViewById(R.id.tvNoOfItemInbox);
        int pernoalTaskList = dataBaseHelper.getPersonalTask().size() + dataBaseHelper.getPersonalInquiry().size() + dataBaseHelper.getPersonalIdea().size();
        if (pernoalTaskList != 0) {
            tvNoOfItemInbox.setText(pernoalTaskList + "");
        }

        tvNoOfItemToday = findViewById(R.id.tvNoOfItemToday);
            int todayListSize = dataBaseHelper.getTodayTask(currentDate).size() + dataBaseHelper.getTodayInquiry(currentDate).size();
        if (todayListSize != 0) {
            tvNoOfItemToday.setText(todayListSize + "");
        }

        tvNoOfItemNext = findViewById(R.id.tvNoOfItemNext);
        int next7dayTask = dataBaseHelper.Next7DayTask().size() + dataBaseHelper.Next7DayInquiry().size();

        if (next7dayTask != 0) {
            tvNoOfItemNext.setText(next7dayTask + " ");
        }*/

        llProject = findViewById(R.id.llProject);
        llTags = findViewById(R.id.llTags);
        llFilters = findViewById(R.id.llFilters);


        tvCreateProject = findViewById(R.id.tvCreateProject);
        tvCreateProject.setOnClickListener(this);

        tvTag = findViewById(R.id.tvTag);
        tvTag.setOnClickListener(this);

        tvUserName = findViewById(R.id.tvUserName);
        tvEmailId = findViewById(R.id.tvEmailId);

        tvUserName.setText(sessionManager.getUserName());
        tvEmailId.setText(sessionManager.getUserEmail());

//        if (!TextUtils.isEmpty(sessionManager.getUserName())) {
//            tvUserName.setText(sessionManager.getUserName());
//        } else {
//            tvUserName.setText(sessionManager.getUserId());
//        }

        tvTitle = findViewById(R.id.tvTitle);


        ivPic = findViewById(R.id.ivPic);

        /*Set Profile Pic code*/

  /*      if (!TextUtils.isEmpty(sessionManager.getProfilePic())) {
            Picasso.get().load(sessionManager.getProfilePic())
                    .transform(new CircleTransform())
                    .error(R.drawable.ic_img_gallary)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .memoryPolicy(MemoryPolicy.NO_CACHE).into(ivPic);
        } else {
            if (sessionManager.getProfileColor() != 0) {
                ColorGenerator generator = ColorGenerator.MATERIAL;
                TextDrawable.IBuilder builder = TextDrawable.builder()
                        .beginConfig()
                        .toUpperCase()
                        .endConfig()
                        .round();
                char first = sessionManager.getUserName().charAt(0);
                TextDrawable ic1 = builder.build(String.valueOf(first), sessionManager.getProfileColor());
                Log.d("123", "getColor = " + generator.getRandomColor());
                ivPic.setImageDrawable(ic1);
            } else {
                ColorGenerator generator = ColorGenerator.MATERIAL;
                int color = generator.getRandomColor();
                TextDrawable.IBuilder builder = TextDrawable.builder()
                        .beginConfig()
                        .toUpperCase()
                        .endConfig()
                        .round();
                try {
                    char first = sessionManager.getUserName().charAt(0);
                    TextDrawable ic1 = builder.build(String.valueOf(first), color);
                    sessionManager.setProfileColor(color);
                    Log.d("123", "getColor = " + generator.getRandomColor());
                    ivPic.setImageDrawable(ic1);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

        }*/

        drawer = findViewById(R.id.drawer_layout);

        ivSideMenu = findViewById(R.id.ivSideMenu);
        ivSideMenu.setOnClickListener(this);

        rlInbox = findViewById(R.id.rlInbox);
        rlInbox.setOnClickListener(this);

        rlToday = findViewById(R.id.rlToday);
        rlToday.setOnClickListener(this);


        rlNext7Day = findViewById(R.id.rlNext7Day);
        rlNext7Day.setOnClickListener(this);

        rlProjects = findViewById(R.id.rlProjects);
        rlProjects.setOnClickListener(this);

        rlTags = findViewById(R.id.rlTags);
        rlTags.setOnClickListener(this);

        rlLocations = findViewById(R.id.rlLocations);
        rlLocations.setOnClickListener(this);

        rlFilters = findViewById(R.id.rlFilters);
        rlFilters.setOnClickListener(this);

        rlDailyNotes = findViewById(R.id.rlDailyNotes);
        rlDailyNotes.setOnClickListener(this);

        rlContactNote = findViewById(R.id.rlContactNote);
        rlContactNote.setOnClickListener(this);

        rlMyContact = findViewById(R.id.rlMyContact);
        rlMyContact.setOnClickListener(this);

        rlSettings = findViewById(R.id.rlSettings);
        rlSettings.setOnClickListener(this);


        rlAllocateMe = findViewById(R.id.rlAllocateMe);
        rlAllocateMe.setOnClickListener(this);


        rlAllocateOthers = findViewById(R.id.rlAllocateOthers);
        rlAllocateOthers.setOnClickListener(this);


        rlHighPriority = findViewById(R.id.rlHighPriority);
        rlHighPriority.setOnClickListener(this);


        rlMediumPriority = findViewById(R.id.rlMediumPriority);
        rlMediumPriority.setOnClickListener(this);


        rlLowPriority = findViewById(R.id.rlLowPriority);
        rlLowPriority.setOnClickListener(this);


        rlNoDueDate = findViewById(R.id.rlNoDueDate);
        rlNoDueDate.setOnClickListener(this);


        rlAllData = findViewById(R.id.rlAllData);
        rlAllData.setOnClickListener(this);

        ivSetting = findViewById(R.id.ivSetting);
        ivSetting.setOnClickListener(this);

        rlLogout = findViewById(R.id.rlLogout);
        rlLogout.setOnClickListener(this);

        //ivLogouts = findViewById(R.id.ivLogouts);
        // ivLogouts.setOnClickListener(this);

        rvProjects = findViewById(R.id.rvProjects);
        rvTags = findViewById(R.id.rvTags);
        ivProjectsDrop = findViewById(R.id.ivProjectsDrop);

        ivTagDrop = findViewById(R.id.ivTagDrop);
        ivFilterDrop = findViewById(R.id.ivFilterDrop);
        ivInbox = findViewById(R.id.ivInbox);
        ivToday = findViewById(R.id.ivToday);
        ivNext7Day = findViewById(R.id.ivNext7Day);
        ivProjects = findViewById(R.id.ivProjects);
        ivTagIcon = findViewById(R.id.ivTagIcon);
        ivLocationIcon = findViewById(R.id.ivLocationIcon);
        ivFilters = findViewById(R.id.ivFilters);
        ivDailyNotes = findViewById(R.id.ivDailyNotes);
        ivContactNoteIcon = findViewById(R.id.ivContactNoteIcon);
        ivMyContactIcon = findViewById(R.id.ivMyContactIcon);
        // ivSettings = findViewById(R.id.ivSettings);
        // ivLogout = findViewById(R.id.ivLogout);

        relativeLayouts = new RelativeLayout[]{rlInbox, rlToday, rlNext7Day, rlProjects, rlTags, rlLocations, rlFilters, rlDailyNotes, rlContactNote, rlMyContact, rlSettings, rlLogout};
        image = new ImageView[]{ivInbox, ivToday, ivNext7Day, ivProjects, ivTagIcon, ivLocationIcon, ivFilters, ivDailyNotes, ivContactNoteIcon, ivMyContactIcon};


        /*Code for projectlist */

        Log.e("Project_list_size",""+dataBaseHelper.getProjectList().size());



        ProjectAdapter   projectAdapter = new ProjectAdapter(getApplicationContext(), dataBaseHelper.getProjectList(), 1);

        projectAdapter.notifyDataSetChanged();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvProjects.setLayoutManager(linearLayoutManager);
        rvProjects.setAdapter(projectAdapter);

        /*Code for taglist */

        ProjectAdapter projectAdapter1 = new ProjectAdapter(getApplicationContext(), dataBaseHelper.getTagList(), 2);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getApplicationContext());
        rvTags.setLayoutManager(linearLayoutManager1);
        rvTags.setAdapter(projectAdapter1);

        if (backStack.equalsIgnoreCase("Personal")) {
            fragment = new PersonalFragment();
            showFragment(fragment);
            tvTitle.setText("Personal");
            setupUI(ivInbox, rlInbox, 0);
        } else if (backStack.equalsIgnoreCase("Next 7 Day")) {
            fragment = new Next7DayFragment();
            showFragment(fragment);
            tvTitle.setText("Next 7 Day");
            setupUI(ivNext7Day, rlNext7Day, 2);
        } else if (backStack.equalsIgnoreCase("Today")) {
            fragment = new TodayFragment();
            showFragment(fragment);
            tvTitle.setText("Today");
            setupUI(ivToday, rlToday, 1);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initView();
    }

    private void showFragment(Fragment fragment) {
        try {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
        } catch (IllegalStateException ieEx) {
            ieEx.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            ExitDialog();
        }
    }

    private void ExitDialog() {
        try {
            final Dialog dialog = new Dialog(Home.this, R.style.AppCompatAlertDialogStyle);
            dialog.requestWindowFeature(1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            View m_view = LayoutInflater.from(Home.this).inflate(R.layout.dlg_alert, null);
            TextView tvYes = m_view.findViewById(R.id.tvYes);

            TextView tvNo = m_view.findViewById(R.id.tvNo);
            ((TextView) m_view.findViewById(R.id.tvMessage)).setText("Are you sure want to exit?");

            tvYes.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dialog.cancel();
                    finishAffinity();
                }
            });
            tvNo.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dialog.cancel();
                }
            });
            dialog.setContentView(m_view);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Dialog Alert Exception = " + e);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "Google Connection Faild = " + connectionResult);
    }

    private void LogoutDialog() {
        try {
            final Dialog dialog = new Dialog(Home.this, R.style.AppCompatAlertDialogStyle);
            dialog.requestWindowFeature(1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            View m_view = LayoutInflater.from(Home.this).inflate(R.layout.dlg_alert, null);
            TextView tvYes = m_view.findViewById(R.id.tvYes);

            TextView tvNo = m_view.findViewById(R.id.tvNo);
            ((TextView) m_view.findViewById(R.id.tvMessage)).setText("Are you sure want to logout?");

            tvYes.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dialog.cancel();
                    if (sessionManager.getLogintType().equalsIgnoreCase("0")) {
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(Status status) {
                                        Logout();
                                    }
                                });
                    } else if (sessionManager.getLogintType().equalsIgnoreCase("1")) {
                        Logout();
                    } else if (sessionManager.getLogintType().equalsIgnoreCase("2")) {
                        LoginManager.getInstance().logOut();
                        Logout();
                    }
                }
            });
            tvNo.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dialog.cancel();
                }
            });
            dialog.setContentView(m_view);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Dialog Alert Exception = " + e);
        }
    }

    private void Logout() {
        sessionManager.Logout(Home.this);
        Intent intentLogin = new Intent(Home.this, Splash.class);
        intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentLogin);
        finish();
    }


    private void sortingDialog() {
        try {
            final Dialog dialog = new Dialog(Home.this, R.style.AppCompatLoderDialogStyle);
            dialog.setCancelable(true);
            dialog.requestWindowFeature(1);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            LayoutInflater m_inflater = LayoutInflater.from(Home.this);
            View m_view = m_inflater.inflate(R.layout.dlg_sorting_ui, null);


            final CheckBox cbSortbyDate = m_view.findViewById(R.id.cbSortbyDate);
            final ImageView img_cancel = m_view.findViewById(R.id.ivCloseDialog);

            img_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });

            //Date
            if (isCheckDate) {
                cbSortbyDate.setChecked(true);
            } else {
                cbSortbyDate.setChecked(false);
            }

            cbSortbyDate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    try {
                        if (isChecked) {
                            isCheckDate = true;
                            sortData(1);
                            dialog.dismiss();
                        } else {
                            try {
                                isCheckDate = false;
                                unsortOriginalData();
                                dialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            });

            //Priority
            CheckBox cbSortbyPriority = m_view.findViewById(R.id.cbSortbyPriority);

            if (isCheckPriority) {
                cbSortbyPriority.setChecked(true);
            } else {
                cbSortbyPriority.setChecked(false);
            }
            cbSortbyPriority.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        isCheckPriority = true;
                        sortData(2);
                        dialog.dismiss();
                    } else {
                        try {
                            isCheckPriority = false;
                            unsortOriginalData();
                            dialog.dismiss();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }

                }
            });

            //Sharing
            CheckBox cbSortbySharing = m_view.findViewById(R.id.cbSortbySharing);

            if (isCheckSharing) {
                cbSortbySharing.setChecked(true);
            } else {
                cbSortbySharing.setChecked(false);
            }
            cbSortbySharing.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        isCheckSharing = true;
                        sortData(3);
                        dialog.dismiss();
                    } else {
                        try {
                            isCheckSharing = false;
                            unsortOriginalData();
                            dialog.dismiss();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }

                }
            });

            //Project
            CheckBox cbSortbyProject = m_view.findViewById(R.id.cbSortbyProject);

            if (isCheckProject) {
                cbSortbyProject.setChecked(true);
            } else {
                cbSortbyProject.setChecked(false);
            }
            cbSortbyProject.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        isCheckProject = true;
                        sortData(4);
                        dialog.dismiss();
                    } else {
                        try {
                            isCheckProject = false;
                            unsortOriginalData();
                            dialog.dismiss();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            });

            dialog.setContentView(m_view);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void sortData(int i) {
//        if (backStack.equalsIgnoreCase("Personal")) {
////            if (PersonalFragment.vpToday.getCurrentItem() == 0) {
////                switch (i) {
////                    case 1:
////                        TaskFragment.todayTaskAdapter.sortByDate();
////                        break;
////                    case 2:
////                        TaskFragment.todayTaskAdapter.sortByPriority();
////                        break;
////                    case 3:
////                        TaskFragment.todayTaskAdapter.sortBySharing();
////                        break;
////                   /* case 4:
////                        TaskFragment.todayTaskAdapter.sortByProject();
////                        break;*/
////                }
////            }
//        } else if (backStack.equalsIgnoreCase("Today")) {
//            if (TodayFragment.vpToday.getCurrentItem() == 0) {
//                switch (i) {
//                    case 1:
//                        TaskFragment.todayTaskAdapter.sortByDate();
//                        TaskFragment.next7dayTaskAdapter.sortByDate();
//                        TaskFragment.overDueTaskAdapter.sortByDate();
//                        break;
//                    case 2:
//                        TaskFragment.todayTaskAdapter.sortByPriority();
//                        TaskFragment.next7dayTaskAdapter.sortByPriority();
//                        TaskFragment.overDueTaskAdapter.sortByPriority();
//                        break;
//                    case 3:
//                        TaskFragment.todayTaskAdapter.sortBySharing();
//                        TaskFragment.next7dayTaskAdapter.sortBySharing();
//                        TaskFragment.overDueTaskAdapter.sortBySharing();
//                        break;
//                    case 4:
//                        TaskFragment.todayTaskAdapter.sortByProject();
//                        TaskFragment.next7dayTaskAdapter.sortByProject();
//                        TaskFragment.overDueTaskAdapter.sortByProject();
//                        break;
//                }
//            } else if (backStack.equalsIgnoreCase("Next 7 Day")) {
//                if (Next7DayFragment.vpToday.getCurrentItem() == 0) {
//                    switch (i) {
//                        case 1:
//                          //  TaskFragment.next7dayTaskAdapter.sortByDate();
//                            break;
//                        case 2:
//                        //    TaskFragment.next7dayTaskAdapter.sortByPriority();
//                            break;
//                        case 3:
//                          //  TaskFragment.next7dayTaskAdapter.sortBySharing();
//                            break;
//                        case 4:
//                          //  TaskFragment.next7dayTaskAdapter.sortByProject();
//                            break;
//                    }
//                } else if (backStack.equalsIgnoreCase(Constant.FILTER)) {
//                    switch (i) {
//                        case 1:
//                           // TaskFragment.todayTaskAdapter.sortByDate();
//                            break;
//                        case 2:
//                           /// TaskFragment.todayTaskAdapter.sortByPriority();
//                            break;
//                        case 3:
//                           // TaskFragment.todayTaskAdapter.sortBySharing();
//                            break;
//                /*case 4:
//                    TaskFragment.todayTaskAdapter.sortByProject();
//                    break;*/
//                    }
//                }
//
//            }
//        }
        }

    private void unsortOriginalData() {
/*
        try {
            if(isCheckDate)
                sortData(1);
            else if(isCheckPriority)
                sortData(2);
            else if(isCheckSharing)
                sortData(3);
            else if(isCheckProject)
                sortData(4);
            else {
                if (backStack.equalsIgnoreCase("Personal")) {
                    if (PersonalFragment.vpToday.getCurrentItem() == 0) {
                        ArrayList<Task> personaArrayList = dataBaseHelper.getPersonalTask();
                        TaskFragment.todayTaskAdapter.refreshData(personaArrayList);
                    } else if (PersonalFragment.vpToday.getCurrentItem() == 1) {
                        ArrayList<Task> personaArrayList = dataBaseHelper.getPersonalIdea();
                        IdeaFragment.todayTaskAdapter.refreshData(personaArrayList);
                    } else if (PersonalFragment.vpToday.getCurrentItem() == 2) {
                        ArrayList<Task> personaArrayList = dataBaseHelper.getPersonalInquiry();
                        InquiryFragment.todayTaskAdapter.refreshData(personaArrayList);
                    }
                } else if ((backStack.equalsIgnoreCase("Today"))) {
                    if (TodayFragment.vpToday.getCurrentItem() == 0) {
                        TaskFragment.todayTaskAdapter.refreshData(dataBaseHelper.getTodayTask(currentDate));
                        TaskFragment.overDueTaskAdapter.refreshData(dataBaseHelper.getOverdueTask1());
                        TaskFragment.next7dayTaskAdapter.refreshData(dataBaseHelper.Next7DayTask());
                    } else if (TodayFragment.vpToday.getCurrentItem() == 1) {
                        InquiryFragment.todayTaskAdapter.refreshData(dataBaseHelper.getTodayInquiry(currentDate));
                        InquiryFragment.overDueTaskAdapter.refreshData(dataBaseHelper.getOverdueInquiry());
                        InquiryFragment.next7dayTaskAdapter.refreshData(dataBaseHelper.Next7DayInquiry());
                    }
                } else if (backStack.equalsIgnoreCase("Next 7 Day")) {
                    if (Next7DayFragment.vpToday.getCurrentItem() == 0) {
                        TaskFragment.next7dayTaskAdapter.refreshData(dataBaseHelper.Next7DayTask());
                    } else if (Next7DayFragment.vpToday.getCurrentItem() == 1) {
                        InquiryFragment.next7dayTaskAdapter.refreshData(dataBaseHelper.Next7DayInquiry());
                    }
                } else if (backStack.equalsIgnoreCase(Constant.FILTER)) {
                    String currentFilter = pref.getString("FILTER_SELECTED", "");
                    switch (currentFilter) {
                        case Constant.ALLOCATE_ME:
                            TaskFragment.todayTaskAdapter.refreshData(dataBaseHelper.getAllocateMe());
                            break;
                        case Constant.ALLOCATE_OTHERS:
                            TaskFragment.todayTaskAdapter.refreshData(dataBaseHelper.getAllocateOthers());
                            break;
                        case Constant.HIGH_PRIORITY:
                            TaskFragment.todayTaskAdapter.refreshData(dataBaseHelper.getHighPriorityTask());
                            break;
                        case Constant.MEDIUM_PRIORITY:
                            TaskFragment.todayTaskAdapter.refreshData(dataBaseHelper.getMediumPriorityTask());
                            break;
                        case Constant.LOW_PRIORITY:
                            TaskFragment.todayTaskAdapter.refreshData(dataBaseHelper.getLowPriorityTask());
                            break;
                        case Constant.VIEW_ALL:
                            TaskFragment.todayTaskAdapter.refreshData(dataBaseHelper.getAllDataTask());
                            break;
                        case Constant.NO_DUE_DATE:
                            TaskFragment.todayTaskAdapter.refreshData(dataBaseHelper.getPersonalTask());
                            break;
                    }
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivSorting:
                sortingDialog();
                break;
            case R.id.ivSearchListClear:
                etvSearch.setText("");
                break;
            case R.id.ivSearch:
                if (rlSearch.getVisibility() == View.GONE) {
                    rlSearch.startAnimation(animationDown);
                    rlSearch.setVisibility(View.VISIBLE);
                    ivSearch.setBackgroundResource(R.drawable.ic_close_black_24dp);
                    ///mOnItemClickListener.onItemClick(ivSearchListClear, etvSearch);
                    // onITemIdeaFragment.onItemClick(ivSearchListClear, etvSearch);
                } else {
                    rlSearch.startAnimation(animationUp);
                    CountDownTimer countDownTimerStatic = new CountDownTimer(500, 16) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                        }

                        @Override
                        public void onFinish() {
                            rlSearch.setVisibility(View.GONE);
                        }
                    };
                    countDownTimerStatic.start();

                    ivSearch.setBackgroundResource(R.drawable.ic_search_new);
                }
                break;
            case R.id.ivSideMenu:
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
                break;

            case R.id.rlInbox:
                backStack = "Personal";

                drawer.closeDrawer(GravityCompat.START);

                fragment = new PersonalFragment();
                showFragment(fragment);
                tvTitle.setText("Personal");
                /*try {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                } catch (IllegalStateException ieEx) {
                    ieEx.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }*/
                setupUI(ivInbox, rlInbox, 0);

                break;

            case R.id.rlToday:
                backStack = "Today";
                tvTitle.setText("Today");
                drawer.closeDrawer(GravityCompat.START);
                setupUI(ivToday, rlToday, 1);
                fragment = new TodayFragment();
                showFragment(fragment);
                break;

            case R.id.rlNext7Day:
                backStack = "Next 7 Day";
                tvTitle.setText("Next 7 Day");
                drawer.closeDrawer(GravityCompat.START);
                fragment = new Next7DayFragment();
                setupUI(ivNext7Day, rlNext7Day, 2);
                showFragment(fragment);
                /*try {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                } catch (IllegalStateException ieEx) {
                    ieEx.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }*/
                break;

            case R.id.rlProjects:
                setupUI(ivProjects, rlProjects, 3);
                if (isProject) {
                    isProject = false;
                    isVisible(llProject, ivProjectsDrop);
                } else {
                    isProject = true;
                    isGone(llProject, ivProjectsDrop);
                }
                break;

            case R.id.rlTags:
                setupUI(ivTagIcon, rlTags, 4);
                if (isTag) {
                    isTag = false;
                    isVisible(llTags, ivTagDrop);
                } else {
                    isTag = true;
                    isGone(llTags, ivTagDrop);
                }
                //drawer.closeDrawer(GravityCompat.START);
                break;

//            case R.id.rlLocations:
//                backStack = "Location";
//                setupUI(ivLocationIcon, rlLocations, 5);
//                drawer.closeDrawer(GravityCompat.START);
//                tvTitle.setText("Add Location");
//                fragment = new LocationFragment();
//                showFragment(fragment);
                /*try {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                } catch (IllegalStateException ieEx) {
                    ieEx.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }*/
                // tvTitle.setTextSize(getResources().getDimension(R.dimen._6sdp));

               /* Intent intent = new Intent(Home.this, LocationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);*/
//                break;

            case R.id.rlFilters:
                setupUI(ivFilters, rlFilters, 6);
                if (isTag) {
                    isTag = false;
                    isVisible(llFilters, ivFilterDrop);
                } else {
                    isTag = true;
                    isGone(llFilters, ivFilterDrop);
                }
                break;

            case R.id.rlDailyNotes:
                setupUI(ivDailyNotes, rlDailyNotes, 7);
                drawer.closeDrawer(GravityCompat.START);
                break;

            case R.id.rlContactNote:
                setupUI(ivContactNoteIcon, rlContactNote, 8);
                drawer.closeDrawer(GravityCompat.START);
                break;

            case R.id.rlMyContact:
                backStack = "My Contact";
                tvTitle.setText("My Contact");
                drawer.closeDrawer(GravityCompat.START);
               // fragment = new ContactFragment();
                fragment = new ContactFragment();
                setupUI(ivMyContactIcon, rlMyContact, 9);
                showFragment(fragment);

                // drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.rlAllocateMe:
                backStack = Constant.FILTER;

                tvTitle.setText("Assigned To Me");
                editor.putString("FILTER_SELECTED",Constant.ALLOCATE_ME);
                editor.commit();
                drawer.closeDrawer(GravityCompat.START);
             //   fragment = new FilterFragment();
                fragment = new TodayFragment();
                setupUI(ivMyContactIcon, rlMyContact, 10);
                showFragment(fragment);

                break;


            case R.id.rlAllocateOthers:
                backStack = Constant.FILTER;

                tvTitle.setText("Assigned To Others");
                editor.putString("FILTER_SELECTED",Constant.ALLOCATE_OTHERS);
                editor.commit();
                drawer.closeDrawer(GravityCompat.START);
               // fragment = new FilterFragment();
                fragment = new TodayFragment();
                setupUI(ivMyContactIcon, rlMyContact, 10);
                showFragment(fragment);

                break;
            case R.id.rlHighPriority:
                backStack = Constant.FILTER;

                tvTitle.setText("High Priority");
                editor.putString("FILTER_SELECTED",Constant.HIGH_PRIORITY);
                editor.commit();
                drawer.closeDrawer(GravityCompat.START);
                //fragment = new FilterFragment();
                fragment = new TodayFragment();
                setupUI(ivMyContactIcon, rlMyContact, 10);
                showFragment(fragment);

                break;
            case R.id.rlMediumPriority:
                backStack = Constant.FILTER;

                tvTitle.setText("Medium Priority");
                editor.putString("FILTER_SELECTED",Constant.MEDIUM_PRIORITY);
                editor.commit();
                drawer.closeDrawer(GravityCompat.START);
              //  fragment = new FilterFragment();
                fragment = new TodayFragment();
                setupUI(ivMyContactIcon, rlMyContact, 10);
                showFragment(fragment);

                break;
            case R.id.rlLowPriority:
                backStack = Constant.FILTER;

                tvTitle.setText("Low Priority");
                editor.putString("FILTER_SELECTED",Constant.LOW_PRIORITY);
                editor.commit();
                drawer.closeDrawer(GravityCompat.START);
                fragment = new TodayFragment();
              //  fragment = new FilterFragment();
                setupUI(ivMyContactIcon, rlMyContact, 10);
                showFragment(fragment);

                break;
            case R.id.rlAllData:
                backStack = Constant.FILTER;

                tvTitle.setText("All");
                editor.putString("FILTER_SELECTED",Constant.VIEW_ALL);
                editor.commit();
                drawer.closeDrawer(GravityCompat.START);
                fragment = new TodayFragment();
             //   fragment = new FilterFragment();
                setupUI(ivMyContactIcon, rlMyContact, 10);
                showFragment(fragment);

                break;
            case R.id.rlNoDueDate:
                backStack = Constant.FILTER;

                tvTitle.setText("No Due Date");
                editor.putString("FILTER_SELECTED",Constant.NO_DUE_DATE);
                editor.commit();
                drawer.closeDrawer(GravityCompat.START);
             //   fragment = new FilterFragment();
                fragment = new TodayFragment();
                setupUI(ivMyContactIcon, rlMyContact, 10);
                showFragment(fragment);

                break;

            /*case R.id.rlSettings:
                setupUI(ivSettings, rlSettings, 10);
                drawer.closeDrawer(GravityCompat.START);
                break;*/

           /* case R.id.ivLogouts:
                //setupUI(ivLogout, rlLogout, 11);
                //drawer.closeDrawer(GravityCompat.START);
                LogoutDialog();
                break;*/

            case R.id.tvCreateProject:
                Intent intent = new Intent(Home.this, Create_Edit_Projects.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constant.CREATE_PROJECTS, "Create Project");
                startActivity(intent);
                break;

            case R.id.tvTag:
                Intent intentTag = new Intent(Home.this, Create_Edit_Tags.class);
                intentTag.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intentTag.putExtra(Constant.CREATE_TAG, "Add Tag");
                startActivity(intentTag);
                break;

            case R.id.ivSetting:
                Intent intentSettings = new Intent(Home.this, Setting.class);
                startActivity(intentSettings);
                break;
        }
    }

    private void isVisible(LinearLayout recyclerView, ImageView dropdown) {
        //recyclerView.startAnimation(animationDown);
        dropdown.setBackgroundResource(R.drawable.ic_up);
        recyclerView.setVisibility(View.VISIBLE);
    }

    private void isGone(final LinearLayout recyclerView, final ImageView dropdown) {
        //recyclerView.startAnimation(animationUp);
        dropdown.setBackgroundResource(R.drawable.ic_down);
        recyclerView.setVisibility(View.GONE);
        /*CountDownTimer countDownTimerStatic = new CountDownTimer(COUNTDOWN_RUNNING_TIME, 16) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                dropdown.setBackgroundResource(R.drawable.ic_dropdown);
                recyclerView.setVisibility(View.GONE);
            }
        };
        countDownTimerStatic.start();*/
    }

    private void setupUI(ImageView imageView, RelativeLayout relativeLayout, int position) {
//        for (int j = 0; j < selectedImage.length; j++) {
//            if (j == position) {
//                imageView.setImageResource(selectedImage[j]);
//                sessionManager.setPosition(position);
//                relativeLayout.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.divider_create));
//            } else {
//                image[j].setImageResource(normalImage[j]);
//                relativeLayouts[j].setBackgroundColor(Color.WHITE);
//            }
//        }
    }


    /* private void ShowFragment(final Fragment fragment) {


     *//* new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {*//*
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (fragment != null) {
                    try {
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                    } catch (IllegalStateException ieEx) {
                        ieEx.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {
                    Log.e("DashBoardSHIPPERS", "Error in creating fragment");
                }

            }
        });
    }*/
    //}, 300);

    //}

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        // redirects to utils

        permissionUtils.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @Override
    public void PermissionGranted(int request_code) {
        //initView();
        Log.d(TAG, "PermissionGranted");
    }

    @Override
    public void PartialPermissionGranted(int request_code, ArrayList<
            String> granted_permissions) {
        Log.d(TAG, "PartialPermissionGranted");

    }


    @Override
    public void PermissionDenied(int request_code) {
        Log.d(TAG, "PermissionDenied");

    }

    @Override
    public void NeverAskAgain(int request_code) {
        Log.d(TAG, "NeverAskAgain");
    }

 /*   private void getDashboard() {
        DialogLoder.showLoderDialog(Home.this, false);
        RequestQueue queue = Volley.newRequestQueue(Home.this);
        StringRequest request = new StringRequest(Request.Method.POST, AppUtils.DASHBOARD, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.d(TAG, "Call Login API = " + response);
                DialogLoder.dismisLoderDialog();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.optString("success");
                            if (status.equalsIgnoreCase("1")) {
                                parseSuccessResponse(jsonObject);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                Log.d(TAG, "Dashboard API RES = " + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogLoder.dismisLoderDialog();
                Log.d(TAG, "Dashboard Respons Error = " + error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> param = new HashMap<>();
                param.put("user_id", SessionManager.getInstance().getUserId());
                return param;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }*/

}
