package com.arccus.accountability.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.arccus.accountability.R;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.skydoves.balloon.ArrowConstraints;
import com.skydoves.balloon.ArrowOrientation;
import com.skydoves.balloon.Balloon;
import com.skydoves.balloon.BalloonAnimation;

import java.util.Random;

/**
 * A simple {@link Fragment} subclass.
 */
public class TaskFragment extends Fragment {

    View v ;
    ExtendedFloatingActionButton extended_fab;

    public TaskFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v =  inflater.inflate(R.layout.fragment_task, container, false);

        initview(v);
        return v;
    }

    private void initview(final View v) {

        extended_fab = v.findViewById(R.id.extended_fab);

        extended_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("fabclick","fabclick");

//                OpenAddTaskDialog(v);
            }
        });
    }

    private void OpenAddTaskDialog(final View view) {




            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),R.style.CustomAlertDialog);
            ViewGroup viewGroup = view.findViewById(android.R.id.content);
            View dialogView = LayoutInflater.from(view.getContext()).inflate(R.layout.add_task_dialog, viewGroup, false);

            final LinearLayout ln_calander = dialogView.findViewById(R.id.ln_calander);
            final ImageView img_cal = dialogView.findViewById(R.id.img_cal);

            builder.setView(dialogView);
            final AlertDialog alertDialog = builder.create();
            alertDialog.setCanceledOnTouchOutside(false);


        final BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(getActivity()).inflate(R.layout.ln_cal, null);
        final PopupWindow popupWindow = BubblePopupHelper.create(getActivity(), bubbleLayout);

//        TextView txt = bubbleLayout.findViewById(R.id.);
        final Random random = new Random();

        ln_calander.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("Range")
            @Override
            public void onClick(View view1) {

                Log.e("open_ballon","open_ballon");



                Balloon balloon = new Balloon.Builder(getActivity())

                 .setLayout(R.layout.ballon_layout)
                .setArrowOrientation(ArrowOrientation.TOP)
                .setArrowPosition(0.5f)
                .setWidthRatio(0.55f)

                .setCornerRadius(4f)
                .setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.color_blue))
               . setBalloonAnimation(BalloonAnimation.CIRCULAR)
//                .setLifecycleOwner(lifecycle)
                        .build();

                balloon.showAlignBottom(ln_calander);


//                int[] location = new int[3];
//                ln_calander.getLocationInWindow(location);
//                popupWindow.showAtLocation(ln_calander, Gravity.CENTER_VERTICAL, location[1], v.getHeight() + location[0]);




            }
        });



            alertDialog.show();
        }







}
