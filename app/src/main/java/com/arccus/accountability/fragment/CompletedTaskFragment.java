package com.arccus.accountability.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arccus.accountability.R;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import androidx.fragment.app.Fragment;

public class CompletedTaskFragment extends Fragment {

    View v;
    ExtendedFloatingActionButton extended_fab;



    public CompletedTaskFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        v = inflater.inflate(R.layout.fragment_completed_task, container, false);

        extended_fab = v.findViewById(R.id.extended_fab);
        extended_fab.setVisibility(View.GONE);
        return  v;
    }
}

