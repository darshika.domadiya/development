package com.arccus.accountability.shared_preference;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.arccus.accountability.MyApplicationClass;


/**
 * Created by Admin on 09/10/2017.
 */

public class SessionManager {

    private static String PREF_NAME = "Login Session";
    private String KEY_FCM_TOKEN = "fcm_Token";
    private String KEY_USERPROFLEPIC = "profile_pic";
    private static String KEY_USERID = "user_id";
    private static String KEY_USERNAME = "userName";
    private static String KEY_USEREMAIL = "userEmail";
    private String KEY_MOBILE_NUMBER = "mobile_number";
    private String KEY_LOGIN_TYPE = "login_type";

    private String KEY_POSITION = "position";

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    private static SessionManager instance;


    @SuppressLint("CommitPrefEdits")
    public SessionManager(Context context) {
        sharedPreferences = context.getApplicationContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public SessionManager()
    {
        sharedPreferences = MyApplicationClass.getAppContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public static SessionManager getInstance()
    {
        if(instance != null)
            return instance;
        else
        {
            instance = new SessionManager();
            return instance;
        }
    }
    public void Logout(Context context) {
        try {
            SharedPreferences.Editor editor = context.getSharedPreferences(PREF_NAME, 0).edit();
            editor.clear();
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getFcmToken() {
        return sharedPreferences.getString(KEY_FCM_TOKEN, "");
    }

    public void setFcmToken(String fcmToken) {
        editor.putString(KEY_FCM_TOKEN, fcmToken);
        editor.apply();
    }

    public String getProfilePic() {
        return sharedPreferences.getString(KEY_USERPROFLEPIC, "");
    }

    public void setProfilePic(String profilePic) {
        editor.putString(KEY_USERPROFLEPIC, profilePic);
        editor.apply();
    }

    public void setProfileColor(int color) {
        editor.putInt("profile_color", color);
        editor.apply();
    }

    public int getProfileColor() {
        return sharedPreferences.getInt("profile_color", 0);
    }

    public String getUserId() {
        return sharedPreferences.getString(KEY_USERID, "");
    }

    public void setUserId(String user_id) {
        editor.putString(KEY_USERID, user_id);
        editor.apply();
    }

    public String getUserName() {
        return sharedPreferences.getString(KEY_USERNAME, "");
    }

    public void setUserName(String userName) {
        editor.putString(KEY_USERNAME, userName);
        editor.apply();
    }

    public String getUserEmail() {
        return sharedPreferences.getString(KEY_USEREMAIL, "");
    }

    public void setUserEmail(String useremail) {
        editor.putString(KEY_USEREMAIL, useremail);
        editor.apply();
    }


    public void setMobileNumber(String mobileNumber) {
        editor.putString(KEY_MOBILE_NUMBER, mobileNumber);
        editor.apply();
    }

    public String getMobileNumber() {
        return sharedPreferences.getString(KEY_MOBILE_NUMBER, "");
    }

    public void setLoginType(String type) {
        editor.putString(KEY_LOGIN_TYPE, type);
        editor.apply();
    }

    public String getLogintType() {
        return sharedPreferences.getString(KEY_LOGIN_TYPE, "");
    }


    public void setPosition(int position) {
        editor.putInt(KEY_POSITION, position);
        editor.apply();
    }

    public String getCurrentLAT() {
        return sharedPreferences.getString("current_lat", "");
    }

    public void setCurrentLAT(String foCurrentLAT) {
        sharedPreferences.edit().putString("current_lat", foCurrentLAT).apply();
    }

    public String getCurrentLONG() {
        return sharedPreferences.getString("current_long", "");
    }

    public void setCurrentLONG(String foCurrentLONG) {
        sharedPreferences.edit().putString("current_long", foCurrentLONG).apply();
    }

}
