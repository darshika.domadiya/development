package com.arccus.accountability.webservice;

import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.arccus.accountability.Response.ColorResponse;
import com.arccus.accountability.activity.CreateTaskActivity;
import com.arccus.accountability.activity.Create_Edit_Projects;
import com.arccus.accountability.shared_preference.SessionManager;
import com.arccus.accountability.utils.Constant;
import com.arccus.accountability.utils.VolleyHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.arccus.accountability.utils.AppUtils.ADD_PROJECT;
import static com.arccus.accountability.utils.AppUtils.GET_COLORS;

public class StaticDataApi {

    public static ArrayList<ColorResponse> colorResponseArrayList = new ArrayList<>();


    public static void CallGetColorApi() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GET_COLORS ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Get_COlor_response = ",""+response);

                        try {
                            JSONObject obj = new JSONObject(response);
                            String message = obj.getString("message");
                            if (obj.has("status") && obj.getString("status").equals("1")) {


                               Gson gson = new Gson();
                               //ColorResponse colorresponse = gson.fromJson(obj.getJSONArray("Response").toString(), ColorResponse.class);

                               colorResponseArrayList = gson.fromJson(obj.getJSONArray("Response").toString(), new TypeToken<ArrayList<ColorResponse>>() {
                                }.getType());

                               // Constant.getProjectColor().addAll(list);








                             /*   String project_id = obj.getJSONObject("Response").getString("project_id");
                                Log.e("project_id",project_id);*/



                            } else {
//                                Toast.makeText(Create_Edit_Projects.this, message, Toast.LENGTH_SHORT).show();


                                //if there is some error
                                //  updateStatusInProject(projectId, NOT_SYNCED_WITH_SERVER,KEY_MYSQL_INSERT);
                            }
                        } catch (JSONException e) {
//                            loaderView.dismiss();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        loaderView.dismiss();
                        //on error
                        // updateStatusInProject(projectId, NOT_SYNCED_WITH_SERVER,KEY_MYSQL_INSERT);
                    }
                }) {
           /* @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("user_id", SessionManager.getInstance().getUserId());
                params.put("project_name",project_name);
                params.put("color_label",colorName);
                params.put("color_position",colorposition);
                Log.e("Insert_PARAMS_INFO = ",""+params.toString());
                return params;
            }*/
        };

        VolleyHelper.getInstance().addToRequestQueue(stringRequest);





    }
}
